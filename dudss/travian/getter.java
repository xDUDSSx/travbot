package dudss.travian;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.SwingUtilities;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;

public class Getter {
	
	static ArrayList<String> XYlist = new ArrayList<String>(); 
	static List<String> Villagelist = new ArrayList<String>();
	static List<String> Namelist = new ArrayList<String>();
	static List<String> Populationlist = new ArrayList<String>();
	static List<String> Alliancelist = new ArrayList<String>();
	
	public static Boolean get_gettercoords(String x,String y, String range, Boolean natars, Boolean antinoob, String maxdiff, String URL) throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {
	
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF); 
		
		System.out.println("Gettercoords start - clearing lists");
		XYlist.clear();
		Villagelist.clear();
		Namelist.clear();
		Populationlist.clear();
		Alliancelist.clear();
		
		System.out.println("URL: " + URL);	
		System.out.println("get_gettercoords method Thread: " + Thread.currentThread());
		
		WebClient client = new WebClient();
		HtmlPage getterpage = null;
		try {
		    getterpage = client.getPage(URL);
		} catch (Exception e) {
			e.printStackTrace();
		    System.err.println("Get page error - FATAL gettercoords error");
		    client.close();
		    return false;
		}
		
		Thread.sleep(600);
		
		try {	
			client.getOptions().setThrowExceptionOnScriptError(false);
			client.getOptions().setThrowExceptionOnFailingStatusCode(false);
		
			HtmlInput xx_input = (HtmlInput) getterpage.getElementById("xyX");
				xx_input.setValueAttribute(x);
		
			HtmlInput yy_input = (HtmlInput) getterpage.getElementById("xyY");
				yy_input.setValueAttribute(y);
		
			HtmlInput range_input = (HtmlInput) getterpage.getElementById("range");
				range_input.setValueAttribute(range);
			HtmlInput checkbox = (HtmlInput) getterpage.getElementById("nataren");
				checkbox.setChecked(natars);
			HtmlInput antiNoob = (HtmlInput) getterpage.getElementById("antiNoob");
				antiNoob.setChecked(antinoob);
			HtmlInput maxDiff = (HtmlInput) getterpage.getElementByName("maxDiff");
				maxDiff.setValueAttribute(maxdiff);
		HtmlButton search = (HtmlButton) getterpage.getFirstByXPath
				("//*[@id=\"contentInner\"]/div/div[1]/section/div/form/table/tbody/tr[14]/td[2]/button");
			
		getterpage = (HtmlPage) search.click();
	    
		List<?> koordspans = (List<?>)getterpage.getByXPath("//span[@class='koordk1']");
		
		int total = koordspans.size();
		int i = 1;
		
		setIndeter(false);
		System.out.println("Getting coordinates ...");
		
		try {
			while (true) {	
					
				HtmlSpan koord1 = (HtmlSpan) getterpage.getByXPath("//span[@class='koordk1']").get(i-1);
				HtmlSpan koord2 = (HtmlSpan) getterpage.getByXPath("//span[@class='koordk2']").get(i-1);
				
				HtmlTableDataCell village = (HtmlTableDataCell) getterpage.getByXPath("*//td[@class=\"tscity\"]").get(i);
				Villagelist.add(village.getTextContent());
				
				HtmlTableDataCell name = (HtmlTableDataCell)  getterpage.getByXPath("*//td[@class=\"rb\"]").get(i);
				Namelist.add(name.getTextContent());
				
				HtmlTableDataCell pop = (HtmlTableDataCell) getterpage.getByXPath("*//td[@class=\"tdews \"]").get(i);
				Populationlist.add(pop.getTextContent());

				HtmlTableDataCell ally = (HtmlTableDataCell) getterpage.getByXPath("*//td[@class=\"tally\"]").get(i);
				Alliancelist.add(ally.getTextContent());
				
				String k1 = koord1.getTextContent().replaceAll("[()]","").replaceAll("\\s+", "");
				XYlist.add(k1);
				String k2 = koord2.getTextContent().replaceAll("[()]","").replaceAll("\\s+", "");
				XYlist.add(k2);
				
				System.out.println("x" + i + ": " + koord1.getTextContent().replaceAll("[()]","").replaceAll("\\s+", "") + 
						 " | y" + i + ": " + koord2.getTextContent().replaceAll("[()]","").replaceAll("\\s+", "") +
						 "   --   village: " + village.getTextContent() + "  username: " + name.getTextContent() + "  pop: " + pop.getTextContent());
				
				int tempi = i;
				double percentage = ((double) tempi/ (double) total)*100;
				
				SwingUtilities.invokeLater(new Runnable() {
				    public void run() {
				    	progressText("~" + (int)percentage + "% ("+k1+"|"+k2+") - V: " + village.getTextContent() + " P: " + name.getTextContent());
				        BotFrame.dpb.setValue((int)percentage);
				    }
				});
				
				BotFrame.dpb.setValue((i/total)*100);
				i++;
			}
			
		}	catch (IndexOutOfBoundsException ex ) {
				System.out.println("\nGT Reading finished! - Out of bounds!");
		} 	catch (Exception ex1) {
				System.out.println("\nGT Reading Exception");	
				ex1.printStackTrace();
		}
		
		System.out.println("Coords: " + XYlist.toString());
		System.out.println("Villages: " + Villagelist.toString());
		System.out.println("Names: " + Namelist.toString());
		System.out.println("Population: " + Populationlist.toString());
		System.out.println("Aliances: " + Alliancelist.toString());
		
		if (XYlist.size() == 0) {
			System.out.println("Gettertools did not retrieve any data - No farms found!");
			client.close();
			return false;
		}
		
		client.close();
		
		} catch(Exception e){
			e.printStackTrace();
			System.err.println("%%%% getter datareader ERROR %%%%");
			return false;
		}
		return true;
	}
	
	void progressValue(int percentage) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {		   
		        BotFrame.dpb.setValue(percentage);
		    }
		});
	}
	static void progressText(String s) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {		   
		        BotFrame.dlabel.setText(s);
		    }
		});
	}
	public static void setIndeter(Boolean b) {
			BotFrame.dpb.setIndeterminate(b);
	}
}

/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */