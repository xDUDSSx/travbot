package dudss.travian;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class SliderGraph extends JPanel{
	
	List<Slider> sliders;
	
	int WIDTH = 480;
	int HEIGHT = 200;
	
	boolean showValues = false;
	
	Color backgroundLineColor = new Color(0, 60, 0);
	Color foregroundLineColor = new Color(0, 255, 0);
	Color dotColor = new Color(0, 191, 0);
	
	AlphaComposite alcomF = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
	AlphaComposite alcomB = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f);
	
	final static float dash1[] = {5.0f};
	BasicStroke dashed = new BasicStroke(1.3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, dash1, 0.0f);
	BasicStroke basic = new BasicStroke(1.3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
		      
	
	private static final long serialVersionUID = 1L;

	SliderGraph(int width, int height) {
		this.WIDTH = width;
		this.HEIGHT = height;
		this.setSize(width, height);
		
		sliders = new ArrayList<Slider>();
		
		for(int i = 0; i < 24; i++) {
			Slider s = new Slider(i, 1.0);
			sliders.add(s);
		}
		
		SliderMouseListener sliderMouseListener = new SliderMouseListener();
		addMouseListener(sliderMouseListener);
		addMouseMotionListener(sliderMouseListener);
	}
	public void showValues(Boolean bool) {
		this.showValues = bool;
		repaint();
	}
	public void setPanelSize(int width, int height) {
		this.WIDTH = width;
		this.HEIGHT = height;
		this.setSize(WIDTH, HEIGHT);
	}
	public ArrayList<Double> getValues() {
		ArrayList<Double> values = new ArrayList<>();
		for(Slider s : sliders) {
			values.add(s.getPosFac());
		}
		return values;
	}
	
	public void setValues(ArrayList<Double> values) {
		for(int i = 0; i < values.size(); i++) {
			sliders.get(i).setPosFac(values.get(i));
			repaint();
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;	
		
		g2d.setColor(Color.BLACK);
		g2d.setComposite(alcomF);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,RenderingHints.VALUE_STROKE_PURE);  
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g2d.setComposite(alcomB);
		g2d.setColor(backgroundLineColor);
		
		//HORIZONTAL TOP + BOTTOM
		g2d.drawLine(0, 0, 0, WIDTH);
		g2d.drawLine(0, HEIGHT, WIDTH, HEIGHT);
		
		//QUARTER AND 3/4
		g2d.drawLine(0, HEIGHT/4, WIDTH, HEIGHT/4);
		g2d.drawLine(0, (HEIGHT/4)*3, WIDTH, (HEIGHT/4)*3);
		
		for (Slider s : sliders) {
			s.drawBar(g2d);
		}
		
		//1/2
		g2d.setColor(Color.yellow);	
		g2d.drawLine(0, HEIGHT/2, WIDTH, HEIGHT/2);
		
		//10% line
		g2d.setStroke(dashed);
		g2d.setColor(Color.yellow);	
		g2d.drawLine(0, HEIGHT - ((HEIGHT/2)/100) * 10, WIDTH,  HEIGHT - ((HEIGHT/2)/100) * 10);
		
		g2d.setStroke(basic);
		g2d.setComposite(alcomF);
		g2d.setColor(foregroundLineColor);	
		
		Path2D.Double prettyPoly = new Path2D.Double();	
	        boolean isFirst = true;
	        for (int i = 0; i < 24; i++) {
	            double x = sliders.get(i).calculateX() + 4;
	            double y = sliders.get(i).calculateY() + 4;
	            
	            if (isFirst) {
	                prettyPoly.moveTo(x, y);
	                isFirst = false;
	            } else {
	                prettyPoly.lineTo(x, y);
	            }
	            
				g2d.setColor(foregroundLineColor);	
	            g2d.draw(prettyPoly);            
	            g2d.setColor(dotColor);	     
	            Slider s = sliders.get(i);
				s.draw(g2d);
	        }
	        
	       
	}
	
	class SliderMouseListener extends MouseAdapter {
		Point2D.Double lastPos;
		Point2D.Double newPos;
		Boolean dragging = false;
		Slider dragged;
		@Override
		public void mouseDragged(MouseEvent e) {
			Point point = new Point(e.getX(), e.getY());
			Rectangle2D.Double rect = new Rectangle2D.Double();
			rect.setRect(point.x-5, point.y-5, 10, 10);

			if (dragging) {		
				double dY = e.getY();
				dragged.setPosFac(((HEIGHT - dY)/HEIGHT)*2);	
				repaint();
			} else {
				for(Slider s : sliders) {
					if (s.bar.intersects(rect)) {
						double dY = e.getY();
						s.setPosFac(((HEIGHT - dY)/HEIGHT)*2);	
						repaint();
					}
				}				
			}
		}
		
		@Override
		public void mousePressed(MouseEvent ev) 
		{
			Point point = new Point(ev.getX(), ev.getY());
			for(Slider s : sliders) {
				if (s.contains(point)) {
					dragged = s;
				}
			}
			if (dragged != null) {
				lastPos = new Point2D.Double(ev.getX(), ev.getY());
			    dragging = true;
			} else {
				dragging = false;
			}
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			dragging = false;
			dragged = null;
		}
	}
	
	class Slider extends Ellipse2D.Double {
		
		int index;
		double currentPositionFactor;
		double currentPositionPercentage;
		double width = 8;
		double height = 8;
		double x;
		double y;
		
		Line2D.Double bar;
		
		Slider(int index, double startingPositionFactor) {
			this.index = index;
			this.currentPositionFactor = startingPositionFactor;
			this.currentPositionPercentage = currentPositionFactor / 2;
			this.x = calculateX();
			this.y = calculateY();
			
			setFrame(x, y, width, height);			
		}
		public void draw(Graphics2D g2d) {
			this.x = calculateX();
			this.y = calculateY();

			g2d.setColor(dotColor);
			g2d.setComposite(alcomF);
			g2d.fill(this);
			
			if(showValues == true) {
				g2d.drawString(String.valueOf(round(this.currentPositionFactor, 2)), (int) x + 10, (int) y);
			}
		} 
		public double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();
		    BigDecimal bd = new BigDecimal(value);
		    bd = bd.setScale(places, RoundingMode.HALF_UP);
		    return bd.doubleValue();
		}
		public void drawBar(Graphics2D g2d) {
			bar = new Line2D.Double();
			bar.setLine(x + width/2, 0, x + width/2, HEIGHT);
			setFrame(x, y, width, height);
			
			g2d.setComposite(alcomB);
			g2d.setColor(backgroundLineColor);
			g2d.draw(bar);
		}
		public void setPosFac(double position) {
			if(position > 2.0) {
				position = 2.0;
			} else if (position < 0.1) {
				position = 0.1;
			}
			this.currentPositionFactor = position;
			this.currentPositionPercentage = position / 2;
		}
		public double getPosFac() {
			return currentPositionFactor;
		}
		public double calculateX() {
			return ((WIDTH/24) * index) + width/2;
		}
		public double calculateY() {
			return (HEIGHT*(1-currentPositionPercentage)) - height/2;
		}
		public double getX() {
			return this.x;
		}
		public double getY() {
			return this.y;
		}
	}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */