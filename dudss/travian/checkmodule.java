package dudss.travian;

import java.util.Timer;
import java.util.TimerTask;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CheckModule extends Travian {
	public static Boolean checkrunning = false;
	public static Boolean checkmodulescanning = false;
	static Timer timer;
	public static Boolean attackcheckerenabled = true;
	public static Boolean FBlogged = false;
	static String metaentry = "none";

	public static void startscan() {		
		class TaskCROP extends TimerTask {
			  public void run() {
				  try {
						  
						while(!Travian.driverInUse.compareAndSet(false, true)) {
							Thread.sleep(2000);
							System.out.println("checkmodule waiting (2sec)");
						}
					
						checkmodulescanning = true;
						System.out.println("\n---CROPMODULE---");
						try {  
							   Alert alt = driver.switchTo().alert();
							   alt.accept();
							} catch(NoAlertPresentException noe) {
							}
						try {
							driver.get(travian_url + "dorf1.php?newdid=6821&");
							} catch (UnhandledAlertException e){
								e.printStackTrace();
								System.out.println("AlertExp");
							}
						
						Double crop = Double.parseDouble((driver.findElement(By.xpath("//*[@id='l4']")).getText()));
						
						System.out.println("crop: " + crop);	
						
						if (crop * 1000 <= 10000) {
							System.out.println("Crop under 10k!");
							
							driver.get(travian_url + "build.php?t=0&id=30"); 
							
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='build']/div[5]/button")));
							driver.findElement(By.xpath("//*[@id=\"build\"]/div[5]/button")).click();
							
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='m2[3]']")));
							driver.findElement(By.xpath("//*[@id='m2[3]']")).sendKeys("9999999");	
							
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"submitText\"]/button")));
							driver.findElement(By.xpath("//*[@id=\"submitText\"]/button")).click();;
							
							try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							
																		
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ButtonText\"]/button")));
							driver.findElement(By.xpath("//*[@id=\"ButtonText\"]/button")).click();
							
							System.out.println("Crop traded!");
							
							
						} else {System.out.println("Crop OK");}
						
/*						if (attackcheckerenabled == true) {
							
							System.out.println("---AttackChecker---");
							
							driver.get(travian_url + "dorf1.php?newdid=6821&");
							
							WebElement movementtable = driver.findElement(By.xpath("//*[@id=\"movements\"]"));  
							List<WebElement> movements = movementtable.findElements(By.xpath("//div[@class='mov']/span")); 
							ArrayList<String> entries = new ArrayList<String>();
							
							String info = movementtable.findElement(By.xpath("//div[@class='mov']/span")).getText();
							
							for (int i = 0; i < movements.size(); i++) {               
				 				entries.add(movements.get(i).getText());
				 			}
														
							System.out.println(entries);
							
							boolean attack = entries.get(0).toLowerCase().contains("�tok");
							System.out.println("under attack: " + attack);										
							
							if (attack == true && !entries.get(0).equals(metaentry) ) {
								//try {
									//sendfbmessage("Attack(s) inbound! (" + entries.get(0) + ") at " + travian.ts());
									System.out.println("FB message fired");
							//	} catch (InterruptedException e) {
								//	e.printStackTrace();
								//}		
								System.out.println("Attack Incoming! (" + info + ")  at: " + travian.ts() );
							}
							
							sendfbmessage(".");
							
							metaentry = entries.get(0);
							System.out.println(metaentry);
						}
		*/
						driver.get(travian_url + "build.php?t=0&id=30");
						
					} catch (Exception e) {
						e.printStackTrace();
					System.err.println("\n%%%% Checkmodule ERROR %%%%");
					}			  
				  
					 driverInUse.set(false);
					 checkmodulescanning = false;
				  
			  }		  
		}
		
		timer = new Timer();
		timer.scheduleAtFixedRate(new TaskCROP(), 0, 10*60*1000);
		checkrunning = true;
	}
	public static void stopscan() {
		timer.cancel();
		checkrunning = false;
	}
	public static void sendfbmessage(String message) throws InterruptedException {
		try {
		
		System.out.println("\n---Facebook Module---");
		
		if (FBlogged == false) { 
			driver.get("https://www.facebook.com/");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"email\"]")));
			WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
			email.sendKeys("");

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"pass\"]")));
			WebElement pass = driver.findElement(By.xpath("//*[@id=\"pass\"]"));
			pass.sendKeys("");

			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@data-testid='royal_login_button']")));
			WebElement login = driver.findElement(By.xpath("//input[@data-testid='royal_login_button']"));
			login.click();
			
			FBlogged = true;
			System.out.println("Facebook logged in!");
		}
		
		System.out.println("Sending message ...");
		
		try {  
			   Alert alt = driver.switchTo().alert();
			   alt.accept();
			} catch(NoAlertPresentException noe) {
				System.out.println("no alert");
			}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_1mf _1mj']")));
		WebElement textarea = driver.findElement(By.xpath("//div[@class='_1mf _1mj']"));
		
		Actions actions = new Actions(driver);
		actions.moveToElement(textarea);
		actions.click();
		actions.sendKeys(message);
		actions.build().perform();
		Thread.sleep(100);
		actions.sendKeys(Keys.ENTER);
		actions.build().perform();
		
		System.out.println("Message sent!");
		
		driver.manage().deleteAllCookies();
		Thread.sleep(4000);
		try {  
			   Alert alt = driver.switchTo().alert();
			   alt.accept();
			} catch(NoAlertPresentException noe) {
				System.out.println("no alert");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n%%%% FBmessage ERROR %%%%");
		}
	}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */