package dudss.travian;

import java.awt.BorderLayout;

//import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;
import javax.swing.JScrollPane;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.awt.Font;
import java.awt.Color;

public class Console extends JFrame {

	private final JPanel contentPanel = new JPanel();
	public static JTextArea textArea;	
	
	public Console() {	
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setTitle("Output console");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Console.class.getResource("/res/travicon.png")));
		setBounds(100, 100, 730, 477);
		setLocationByPlatform(true);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		textArea = new JTextArea();
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(Color.WHITE);
		textArea.setFont(new Font("Courier New", Font.PLAIN, 15));
		textArea.setEditable(false);
		
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		{
			JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane);
			{
				scrollPane.setViewportView(textArea);
			}
		}
		
		PrintStream con = new PrintStream(new TextAreaOutputStream(textArea));
		System.setOut(con);
		System.setErr(con);
	}

}