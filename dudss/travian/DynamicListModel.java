package dudss.travian;

import javax.swing.AbstractListModel;

public class DynamicListModel<T> extends AbstractListModel<T> {
	private static final long serialVersionUID = 1L;
	
	T[] values;
	
	DynamicListModel(T[] vals) {
		values = vals;
	}
	
	//String[] values = new String[] {"General", "Farmlist sender", "Farm adder", "Crop checker", "Proxy", "Dev"};
	public int getSize() {
		return values.length;
	}
	public T getElementAt(int index) {
		return values[index];
	}
	public void setElementAt(int index, T s) {
		values[index] = s;
	}
}
