package dudss.travian;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.UnknownHostException;
import java.security.Key;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

import javax.crypto.Cipher;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

@SuppressWarnings("deprecation")
public class Travian extends Thread {
	
	static public WebDriver driver; 
	static public WebDriverWait wait;
	
	static ArrayList<FarmListSender> fllist;
	static int n_of_flsenders = -1;

	static Boolean addWindowToBeOpened = false;
	public static Boolean usehtmlunitrequest = true;
	
    static final SimpleDateFormat logdateforamt = new SimpleDateFormat("HH:mm:ss"); 
    
    static final byte[] keyValue = "BJG6ADSJ7S3ASEH5".getBytes();
	static String travian_url;
	static String travian_url_farm;
		
	public static List<WebElement> farmlists2;
	public static ArrayList<String> farmlists_names2;
	public static ArrayList<String> farmlists_id2;
	public static ArrayList<String> villageids;
	public static ArrayList<String> villagenames;
	
	public static ArrayList<String> villageids2;
	public static ArrayList<String> villagenames2;
	
	static String loginStatus = "";
	static String passwordStatus = "";
	
	static Boolean LoginCorrect = false;
	static Boolean PasswordCorrect = false;
	
	static Boolean LoginSuccessful = false;
	
	static Boolean rememberdataexists;
	static Boolean driverrunning = false;
	static Boolean driverinitiated = false;
	
	public static final AtomicBoolean driverInUse = new AtomicBoolean(false);
	
	static int tick = 0;
	
	public void run() {
		try {
			try {
				travianlogin(BotFrame.frameUsername, BotFrame.framePassword);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			if (Travian.LoginSuccessful == true) {
				BotFrame.getterbtnclick();
			}
		} catch (FailingHttpStatusCodeException | IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void travianlogin (String name_gui, String password_gui) throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException, InvocationTargetException {
		
		//TODO: Login "failed" detection a bit wonky. Make it more reliable. Driver init cant repeat itself (hopefully fixed)
		
		//INITIAL SERVER VERIFICATION
		System.out.println("\nServer verification");
		writetodatabase();
		System.out.println("Done!");
				
		String path = BotFrame.workingDirectoryPath;
		
		String ending = "";
		if (SystemUtils.IS_OS_WINDOWS) {
			ending = "/drivers/win/chromedriver.exe";
		} else if (SystemUtils.IS_OS_MAC) {
			ending = "/drivers/mac/chromedriver";
		} else if (SystemUtils.IS_OS_LINUX) { 
			ending = "/drivers/linux/chromedriver";
		}
		String driverpath = path + ending;		
		System.out.println("\nChromedriver path: " + driverpath + "\n");

		//USER DETAILS WRITER
		if (BotFrame.remembercheck == true) { 
			
			//File file = new File(BotFrame.workingDirectoryPath + "/pass192.trav");
	
			/*if (!file.exists()) {
				file.createNewFile();
				rememberdataexists = false;
			} else { rememberdataexists = true; }
			*/	
			
			try {	
				Key aesKey = BotFrame.generateKey();
				Cipher desCipher;
				desCipher = Cipher.getInstance("AES");
				desCipher.init(Cipher.ENCRYPT_MODE, aesKey);
				
				byte[] rawdata = password_gui.getBytes("UTF8");			
				byte[] encrypteddata = desCipher.doFinal(rawdata);
				byte[] encodeddata = new Base64().encode(encrypteddata);
			    String data = new String(encodeddata);			  
			    System.out.println("Password encryption finished");
					
			    try (
		            BufferedWriter writer = Files.newBufferedWriter(Paths.get("metadata/metadata.csv"));
		            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
		        ) {
		            csvPrinter.printRecord(data, name_gui, BotFrame.frameServer, BotFrame.frameUserAgent, String.valueOf(Settings.chckbxUseUseragent.isSelected()),
		            String.valueOf(Settings.logincheck.isSelected()), String.valueOf(Settings.httpcheckbox.isSelected()),
					Settings.FARMADDER_INIT_DELAY_FIELD.getText(),  Settings.FARMADDER_EXPAND_DELAY_FIELD.getText(),  Settings.FARMADDER_ADD_DELAY_FIELD.getText(),
					Settings.FARMLISTSENDER_INIT_DELAY_FIELD.getText(),  Settings.FARMLISTSENDER_EXPAND_DELAY_FIELD.getText(),  Settings.FARMLISTSENDER_CHECK_DELAY_FIELD.getText(),
					Settings.FARMLISTSENDER_SEND_DELAY_FIELD.getText(),  Settings.attack_lost_box.isSelected(), Settings.attack_win_loss_box.isSelected(),
					Settings.proxyfield.getText() , Settings.proxycheckbox.isSelected(), Settings.timeoutfield.getText(),
					BotFrame.checknatars.isSelected(), BotFrame.antinoob.isSelected(), BotFrame.maxdiff.getText(),
					Settings.chckbxOverrideDir.isSelected(), Settings.dirTextField.getText());
		            csvPrinter.flush();            
		        }
				    
			} catch(Exception e)
			{
				System.out.println("Encode exception!");
			}
		}
		
		if (driverinitiated == false) {
			//System.setProperty("webdriver.chrome.driver", driverpath);
			
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("disable-infobars");
			chromeOptions.addArguments("--no-sandbox");
			chromeOptions.addArguments("--disable-notifications");
			
			if (BotFrame.headless == true) {
				System.out.println("!! driver headless mode !!");
				chromeOptions.addArguments("--headless");
			} else {
				System.out.println("!! driver GUI mode !!");
			}
			
			if(BotFrame.useragentcheck == true) {			
				chromeOptions.addArguments("--user-agent=" + BotFrame.frameUserAgent);
				System.out.println("Custom user agent: " + BotFrame.frameUserAgent);
			}
			
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			capability.setBrowserName("chrome");
			
			if(BotFrame.enablelogging == true) {
				//Enabling chromedriver network/performance logging
				LoggingPreferences logPrefs = new LoggingPreferences();	
			    logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
				capability.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				
				Map<String, Object> perfLogPrefs = new HashMap<String, Object>();
				perfLogPrefs.put("traceCategories", "browser,devtools.timeline,devtools"); // comma-separated trace categories
				chromeOptions.setExperimentalOption("perfLoggingPrefs", perfLogPrefs);
				}
			if(BotFrame.useproxy == true) {		
				Proxy proxy = new Proxy();
				proxy.setHttpProxy(BotFrame.proxy); 
				proxy.setSslProxy(BotFrame.proxy); 
				System.out.println("-- Using proxy: " + proxy + " --");
				chromeOptions.setProxy(proxy);
			}			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			int w = (int) screenSize.getWidth();
			int h = (int) screenSize.getHeight();
			
			System.out.println("window-size=" + w + "," + h);
			chromeOptions.addArguments("window-size=" + w + "," + h);
			
			try {
				//A non deprecated way of starting a chromedriver, have to use this because chromedriver performance logging wont get enabled if I passed DesiredCap straight to ChromeOptions
				ChromeDriverService service = new ChromeDriverService.Builder()
	                    .usingDriverExecutable(new File(driverpath))
	                    .usingAnyFreePort()
	                    .build();
				
				//Merging capability into chromeOptions (Instead of chromeOptions.setCapability(capability))
				chromeOptions.merge(capability);    
				
				driver = new ChromeDriver(service, chromeOptions);
			} catch (IllegalStateException ex) {
				System.err.println("Driver initalisation error! The bot chromedriver might not be executable (if you're using Mac/Linux)");
				System.out.println("You can try running the \"chmod -x pathtochromedriver\". Chromedriver is located in /drivers/*yourOS*/chromedriver.");
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JOptionPane.showMessageDialog(BotFrame.MainGUI, "Driver initalisation error! The bot chromedriver might not be executabel (if you're using Mac/Linux)\n"
								+ "You can try running the \"chmod -x pathtochromedriver\". Chromedriver is located in /drivers/*yourOS*/chromedriver.\n"
								+ "Or the bot couldn't find the chromedriver executable itself. See the error stack trace in the console."
								, "Driver init error.", JOptionPane.ERROR_MESSAGE);
					}
				});
				ex.printStackTrace();
				
				Travian.driverInUse.set(false);
	 			BotFrame.loginbtn.setIcon(null);
	 			BotFrame.loggingin = false;
	 			LoginSuccessful = false;
	 			
				return;
			}
			if(BotFrame.enablelogging == true) {
			    System.out.println("Available Log Types: ");
				System.out.println(driver.manage().logs().getAvailableLogTypes());
			}
			
			driverinitiated = true;
			driverrunning = true;
			
			wait = new WebDriverWait(driver, BotFrame.timeout);
		}
		
		if(LoginSuccessful == false || driverrunning == true) {
			
			travian_url = "https://" + BotFrame.frameServer + "/";
			travian_url_farm =  travian_url + "build.php?tt=99&id=39";	
			
			try {
				travian_url = "https://" + BotFrame.frameServer + "/";
				
				while(Travian.driverInUse.compareAndSet(false, true)) {
					System.out.println("login waiting");
					Thread.sleep(1000);
				}
				
				driver.get(travian_url);	
				
			} catch (WebDriverException e) {
				System.err.println("Invalid server url");
				Travian.driverInUse.set(false);
	 			BotFrame.loginbtn.setIcon(null);
	 			BotFrame.loggingin = false;
	 			LoginSuccessful = false;
			}
			//Initital login wait
			System.out.println("2 sec login wait ...");
			Thread.sleep(2000);
			
			try {
				wait.until(ExpectedConditions.elementToBeClickable(By.id("s1")));
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("name")));
				
				if(detectCaptcha(driver) == true) {
					System.out.println("Travian login captcha detected!");
					Travian.driverInUse.set(false);
		 			BotFrame.loginbtn.setIcon(null);
		 			BotFrame.loggingin = false;
		 			LoginSuccessful = false;
		 			SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							JOptionPane.showMessageDialog(BotFrame.MainGUI, "Login captcha detected!", "Login error", JOptionPane.ERROR_MESSAGE);
						}
					});
		 			return;
				}
			
				System.out.println(BotFrame.frameUsername);
				driver.findElement(By.name("name")).sendKeys(BotFrame.frameUsername.toString());
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
				System.out.println("password");	
				driver.findElement(By.name("password")).sendKeys(BotFrame.framePassword.toString());
				
				driver.findElement(By.id("s1")).click();
				System.out.println("Logging in ...");
				
				System.out.println(driver.getCurrentUrl());
						
			//TODO: LOGINFAILED - figure out a way to know if login failed or not thus not executing following part and denying access immediately
			
				// If page still contains the password field, login did not succeed
				// Maybe make different messages for invalid username or password
				try {
					driver.findElement(By.name("password"));
					LoginSuccessful = false;
					System.err.println("-Login not successful-\n");
					
					Travian.driverInUse.set(false);
		 			BotFrame.loginbtn.setIcon(null);
		 			BotFrame.loggingin = false;
		 			LoginSuccessful = false;
		 			return;
				} catch (NoSuchElementException b) {
					LoginSuccessful = true;
					System.out.println("*Login successful*\n");
				}
			} catch (TimeoutException | NoSuchElementException e) {
				System.out.println("Login page error - Page timeout / No elements found");
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JOptionPane.showMessageDialog(BotFrame.MainGUI, "Login page error - Page timeout / No elements found", "Login error", JOptionPane.ERROR_MESSAGE);
					}
				});
				Travian.driverInUse.set(false);
	 			BotFrame.loginbtn.setIcon(null);
	 			BotFrame.loggingin = false;
	 			LoginSuccessful = false;
	 			return;
			}
		}	
			if(LoginSuccessful == true) {
				try {
					System.out.println("\nInitial farmlist init ...");
					FarmListInit.farmlistinit();
					BotFrame.initbtn.setEnabled(true);
		 			BotFrame.loginbtn.setText(BotFrame.langUtils.getPhraseFor("Logged in as") + " " + BotFrame.frameUsername);
				} catch (InterruptedException | IOException e) {
					e.printStackTrace();
				}
			}
			
			Travian.driverInUse.set(false);
 			BotFrame.cropbtn.setEnabled(true);
 			BotFrame.loginbtn.setIcon(null);
 			BotFrame.loggingin = false;
 			
	}
	
	public static String ts() {
	    return "Timestamp: " + new Timestamp(new java.util.Date().getTime());
	}
	public static String timeUTC() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		//cal.get(Calendar.MONTH) + 1,  Months are indexed from 0-11 instead of 1-12 so I need to add 1 to represent the current month
		String currentUtcDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
		return currentUtcDate;
	}
	
	public static Boolean detectCaptcha(WebDriver driver) {
		Boolean captchaDetected = false;
		String pagesource = driver.getPageSource();
		
		if (pagesource.contains("class=\"g-recaptcha\"") || pagesource.contains("class='g-recaptcha'")) {
			captchaDetected = true;
		}
		
		return captchaDetected;
	}
	
	public static void writetodatabase() throws FailingHttpStatusCodeException, MalformedURLException, IOException, InvocationTargetException, InterruptedException {
		
		String response = null;
		
		if(usehtmlunitrequest == true) {
			response =  htmlunitRequest();
		} else {
			response =  executeVarHTTPrequest();
		}
		
		String responses[] = response.split("<br>");
		
		String errormessage;
		
		if(responses[0].contains("blacklisted")) {
			System.err.println("\n-- blacklisted --");
			if (responses.length > 1 && responses[1] != null && !responses[1].isEmpty()) {
				errormessage = responses[1];
				System.out.println(responses[1]);		
			} else {
				errormessage = "";
			}
			EventQueue.invokeAndWait(new Runnable() {
	    	    @Override
	    	    public void run() {
	    	    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
	    				    "This ip is not verified by the server, contact the developer\n"
	    				    + errormessage,
	    				    "Verify error",
	    				    JOptionPane.ERROR_MESSAGE);
	    	    }
	    	  });
			quitmethod();
			System.exit(0);
		} else {
			if(responses[0].contains("success")) {		
				if(responses[1].contains(BotFrame.ver)) {
					System.out.println("Version up to date!");
				} else {
					EventQueue.invokeLater(new Runnable() {
			    	    @Override
			    	    public void run() {
			    	    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
								    "This version is outdated\nCurrent version: " + BotFrame.ver + "\nLatest versions: " + responses[1].toString() + "\nYou can download the latest version at travbot.xf.cz",
								    "Outdated version",
								    JOptionPane.WARNING_MESSAGE);
			    	    }
			    	  });
					
				}
			} else {	
				System.err.println("Could not verify bot session by server / no server connection");
				EventQueue.invokeAndWait(new Runnable() {
		    	    @Override
		    	    public void run() {
		    	    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
							    "TravBot tracking server down.",
							    "Warning",
							    JOptionPane.WARNING_MESSAGE);
		    	    }
		    	});
			}
		}
	}
	
	public static void runscript(String script) {
		JavascriptExecutor js;
		if (driver instanceof JavascriptExecutor) {
		    js = (JavascriptExecutor)driver;
		} else {
		    throw new IllegalStateException("No JavaScript support!");
		}
		
		js.executeScript(script);
		
		System.out.println("JS executed!");
	}   
	
	public static void getVillageIds() {
		
		villageids = new ArrayList<String>();
		villagenames = new ArrayList<String>();
		//villageids.clear();
		
		driver.get(travian_url_farm);
		
		Travian.runscript("Travian.Game.RaidList.showCreateNewList()");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"did\"]")));
		List<WebElement>options = driver.findElements(By.xpath("//*[@id=\"did\"]/option"));
		
		for(int i = 0; i < options.size(); i++) {	
			String tempid = options.get(i).getAttribute("value").toString();	
			String tempname = options.get(i).getText();
			
			villageids.add(tempid);
			villagenames.add(tempname);
			
			System.out.println("Village"+i+"ID " + villagenames.get(i)+ " = " + villageids.get(i));
		}
		
		System.out.println(villageids.toString());
		System.out.println(villagenames.toString());
		
		villageids2 = villageids;
		villagenames2 = villagenames;
	}
	
	private static String htmlunitRequest() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		String phpurl = "http://travbot.xf.cz/scripts/verify.php?user=" + BotFrame.frameUsername + "&ip=" + BotFrame.ip + "&time=" + timeUTC() + "&server=" + BotFrame.frameServer + "&ver=" + BotFrame.ver;
		String responsestring = null;
		
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF); 

		WebClient dataclient = new WebClient();
		
		dataclient.getOptions().setThrowExceptionOnScriptError(false);
		dataclient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		
		try {
			HtmlPage datapage = dataclient.getPage(phpurl);	
			WebResponse response = datapage.getWebResponse();
			responsestring = response.getContentAsString();
		} catch(UnknownHostException | HttpHostConnectException e) {
			System.err.println("Could not connect to the verification server");
			Travian.quitmethod();
			System.exit(10);
		}
		System.out.println("\n\nWebResponse:");
		System.out.println(responsestring);
		
		dataclient.close();
		return responsestring;
	}
	
	private static String executeVarHTTPrequest() throws IOException {
	int statusCodetemp = 0;
	String response = null;
	try {
		HttpPost httppost = new HttpPost("http://travbot.xf.cz/scripts/verification.php");
		
		List<BasicNameValuePair> parameters = new ArrayList<BasicNameValuePair>();
		parameters.add(new BasicNameValuePair("user", BotFrame.frameUsername));
		parameters.add(new BasicNameValuePair("ip", BotFrame.ip));
		parameters.add(new BasicNameValuePair("time", timeUTC()));
		parameters.add(new BasicNameValuePair("server", BotFrame.frameServer));
		parameters.add(new BasicNameValuePair("ver", BotFrame.ver));
		
		httppost.setEntity(new UrlEncodedFormEntity(parameters));
		
		@SuppressWarnings("resource")
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse httpResponse = httpclient.execute(httppost);
		HttpEntity resEntity = httpResponse.getEntity();

		int statusCode = httpResponse.getStatusLine().getStatusCode();
		statusCodetemp = statusCode;

		InputStream in = resEntity.getContent();

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		    StringBuilder out = new StringBuilder();
		    String line;
		    while ((line = reader.readLine()) != null) {
		        out.append(line);
		    }
		    reader.close();
		    if(statusCode == 200) {
		    	 System.out.println("Status Code: OK (" + statusCode + ")");
		    } else {
		    	System.out.println("Status Code: " + statusCode);
		    }
		    
		    response = out.toString().replaceAll("\\<!--WZ-REKLAMA-1.0IZ-->.*?\\<!--WZ-REKLAMA-1.0IK-->", "");
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("database verification error, response status code: " + statusCodetemp);
	}

	return response;
	
	}
	
	public static void switchVillage(String villageName) {
		String currentVillage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='sidebarBoxVillagelist']//li[@class=' active']/a/div[@class='name']"))).getText();
			
		//Checking current village and switching to the correct one
		if (!currentVillage.equals(villageName)) {
			System.out.println("Switching village! " + currentVillage + " --> " + villageName);
			Travian.driver.get("https://" + BotFrame.frameServer + "/dorf1.php?newdid="
			+ Travian.villageids2.get(Travian.villagenames2.indexOf(villageName)) + "&");
		}
	}
	
	public static void quitmethod() {
		try {
			if(Travian.driverinitiated == true || driverrunning == true) {
				System.out.println("Closing windows!");
				driver.close();
				driverrunning = false;
				
				System.out.println("Quitting!");
				driver.quit();
			}
			
			} catch (NullPointerException e) {
				System.err.println("NullPointerException - no drivers to close!");
			}
		}
}
/**
 *  © 2018 Dan Rakušan. All rights reserved.
 */