package dudss.travian.crop;

import java.util.Timer;

import dudss.travian.Settings;

/**A static class that manages a timer that schedules {@link CropListener}s.*/
public class CropChecker {
	static Timer t;

	public static boolean running = false;

	public static void start() {
		if (!running) {
			System.out.println("Starting CropChecker! Delay: " + Long.parseLong(Settings.cropRate.getText()) * 1000);
			t = new Timer();
			t.schedule(new CropListener(t), Long.parseLong(Settings.cropRate.getText()) * 1000);
			running = true;
		}
	}
	
	public static void stop() {
		if (running) {
			System.out.println("Stopping CropChecker!");
			t.cancel();
			t.purge();
			running = false;
		}
	}
}
