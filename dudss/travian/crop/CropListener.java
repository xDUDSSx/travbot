package dudss.travian.crop;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dudss.travian.BotFrame;
import dudss.travian.Settings;
import dudss.travian.Travian;

public class CropListener extends TimerTask {
	
	Timer t; 
	
	CropListener(Timer t) {
		super();
		this.t = t;
	}
	
	@Override
	public void run() {
		synchronized (Travian.driverInUse) {      		
        	Travian.driverInUse.set(true);
        	System.out.println("\n[CropListener (delay: " + Long.parseLong(Settings.cropRate.getText()) * 1000 + ") " + Thread.currentThread()+ "] Scanning ... at " + Travian.ts());
        	System.out.println("Current driver page: " + Travian.driver.getCurrentUrl());       
        	WebDriverWait wait = new WebDriverWait(Travian.driver, BotFrame.timeout);	
        	
    		if (Settings.villageComboBox.getSelectedItem() != null) {
    			String currentVillage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='sidebarBoxVillagelist']//li[@class=' active']/a/div[@class='name']"))).getText();
    			
       			System.out.println("Selected village: " + Settings.villageComboBox.getSelectedItem());
    			System.out.println("Current village: " + currentVillage);
    			
    			//Checking current village and switching to the correct one
    			if (!currentVillage.equals(Settings.villageComboBox.getSelectedItem().toString())) {
    				System.out.println("Switching village! " + currentVillage + " --> " + Settings.villageComboBox.getSelectedItem());
    				Travian.driver.get("https://" + BotFrame.frameServer + "/dorf1.php?newdid="
    				+ Travian.villageids2.get(Travian.villagenames2.indexOf(Settings.villageComboBox.getSelectedItem())) + "&");
    				currentVillage = (String) Settings.villageComboBox.getSelectedItem();
    			}
    			
    			double crop = Double.MAX_VALUE;
            	try {
            		String cropString = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='l4']"))).getText();
            		
            		crop = Double.parseDouble(cropString.replace(" " , "").replaceAll("\\D+",""));
            	} catch (Exception e) {
            		System.err.println("ERROR: Couldn't parse crop data!");
            		System.err.println(e.getLocalizedMessage());
            	}
            		
            	if (crop != Double.MAX_VALUE) {
                	int maxCropStorage = 0;
            		String cropStorage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='stockBarGranary']"))).getText();
            		maxCropStorage = Integer.parseInt((cropStorage.replace(" " , "").replaceAll("\\D+","")));
            		double cropPercentage = (crop / maxCropStorage)*100d;
            		
            		System.out.print("Crop level in " + currentVillage + ": " + crop + " / " + maxCropStorage + " (=" + cropPercentage + "%)");
           
            		if (Settings.rdbtnUnderflow.isSelected()) {	            	
	            		if (cropPercentage < Double.parseDouble(Settings.cropBelow.getText())) {
	            			System.out.println("\nLow crop level detected! Crop below " + Settings.cropBelow.getText() + "%!");
	            			String message = NPCTrade();
	            			if (!message.equals("")) {
	            				try {
									EventQueue.invokeAndWait(new Runnable() {
									    @Override
									    public void run() {
									    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
												    "NPC trade could not be executed!\n"
												    + message,
												    "CropChecker error",
												    JOptionPane.ERROR_MESSAGE);
									    }
									});
								} catch (InvocationTargetException | InterruptedException e) {
									e.printStackTrace();
								}
	            			};
	            		} else {
	            			System.out.println(" -- OK\n");
	            		}
            		} else {
            			if (cropPercentage > Double.parseDouble(Settings.cropAbove.getText())) {
	            			System.out.println("\nHigh crop level detected! Crop above " + Settings.cropAbove.getText() + "%!");
	            			String message = NPCTrade();
	            			if (!message.equals("")) {
	            				try {
									EventQueue.invokeAndWait(new Runnable() {
									    @Override
									    public void run() {
									    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
												    "NPC trade could not be executed!\n"
												    + message,
												    "CropChecker error",
												    JOptionPane.ERROR_MESSAGE);
									    }
									});
								} catch (InvocationTargetException | InterruptedException e) {
									e.printStackTrace();
								}
	            			};
	            		} else {
	            			System.out.println(" -- OK\n");
	            		}
            		}
            	}
    		} else {
    			System.err.println("ERROR: CropChecker target village NOT SET or NULL! Can be found under Settings > Crop checker!");
    		}	
    	}
    	
    	t.schedule(new CropListener(t), Long.parseLong(Settings.cropRate.getText()) * 1000);
    	Travian.driverInUse.set(false);
	}
	
	private String NPCTrade() {
		int[] targetResourcePercentage = new int[4];
		targetResourcePercentage[0] = Integer.parseInt(Settings.fieldWood.getText());
		targetResourcePercentage[1] = Integer.parseInt(Settings.fieldClay.getText());
		targetResourcePercentage[2] = Integer.parseInt(Settings.fieldIron.getText());
		targetResourcePercentage[3] = Integer.parseInt(Settings.fieldCrop.getText());
			
		for (int i = 0; i < targetResourcePercentage.length; i++) {
			if (targetResourcePercentage[i] > 100) {
				targetResourcePercentage[i] = 100;
			} else
			if (targetResourcePercentage[i] < 0) {
				targetResourcePercentage[i] = 0;
			}
		}
				
		System.out.println("Initiating NPC Trade (Wood: " + targetResourcePercentage[0] + "% Clay: " + targetResourcePercentage[1] + "% Iron: " + targetResourcePercentage[2] + "% Crop: " + targetResourcePercentage[3] + "%) at " + Travian.ts());
		int gold = Integer.parseInt(Travian.driver.findElement(By.xpath("//*[@class='ajaxReplaceableGoldAmount']")).getText());
		System.out.println("Checking amount of gold ...");
		if (gold > 3) {
			System.out.println("Gold: " + gold);
			System.out.println("Bringing up NPC trader");
			//Switching to NPC trader
        	try {					
        		Travian.driver.get("https://" + BotFrame.frameServer + "/dorf2.php");
        		System.out.println("Searching for marketplace");
            	WebDriverWait wait = new WebDriverWait(Travian.driver, BotFrame.timeout);	
            	
            	boolean marketplaceFound = false;
            	int slotID = 0;
            	int villageID = Integer.valueOf(Travian.villageids2.get(Settings.villageComboBox.getSelectedIndex()));
            	
            	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='village_map']")));
            	for (WebElement e : Travian.driver.findElements(By.xpath("//*[@id='village_map']/div"))) {
            		if (e.getAttribute("class").contains("g17")) {
            			marketplaceFound = true;
            			slotID = Integer.valueOf(e.getAttribute("class").substring(14, 16));
            			System.out.println("Marketplace found at slot " + slotID);
            			break;
            		}
            	}
            	if(!marketplaceFound) {
            		System.err.println("Marketplace not found! Cannot NPC trade!");
            		return "Marketplace not found! Cannot NPC trade!";
            	}
            	
            	/*
            	System.out.println("Marketplace URL: " + "https://" + BotFrame.frameServer + "/build.php?id=" + slotID);
            	Travian.driver.get("https://" + BotFrame.frameServer + "/build.php?id=" + slotID);
            	
            	WebElement npcButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='npcMerchant']/button[@class='gold ']")));  	
            	npcButton.click();
        		
        		Thread.sleep(500);
        		*/
        		
            	JavascriptExecutor executor = (JavascriptExecutor) Travian.driver;
            	System.out.println("Opening marketplace dialog (did: " + villageID + ")");
            	executor.executeScript("jQuery(window).trigger('buttonClicked', [this, {\r\n" + 
            			"            \"dialog\": {\r\n" + 
            			"                \"cssClass\": \"white\",\r\n" + 
            			"                \"draggable\": false,\r\n" + 
            			"                \"overlayCancel\": true,\r\n" + 
            			"                \"buttonOk\": false,\r\n" + 
            			"                \"saveOnUnload\": false,\r\n" + 
            			"                \"data\": {\r\n" + 
            			"                    \"cmd\": \"exchangeResources\",\r\n" + 
            			"                    \"defaultValues\": [],\r\n" + 
            			"                    \"did\": \"" + villageID + "\"\r\n" + 
            			"                },\r\n" + 
            			"                \"preventFormSubmit\": true\r\n" + 
            			"            }\r\n" + 
            			"        }]);");
            	
            	Thread.sleep(500);
            	
            	System.out.println("Fetching resource data");
            	int[] currentResourceCount = new int[4];
            	int resourceSum = 0;
        		for (int i = 0; i < currentResourceCount.length; i++) {
        			WebElement field = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='npc']/thead/tr/td[" + (i + 1) + "]/span")));     
                	currentResourceCount[i] = Integer.parseInt(field.getText());
                	resourceSum += currentResourceCount[i];
        		}    		
        		
            	System.out.println("Calculating resource values");
            	int[] finalResourceValues = new int[4];
            	
            	for (int i = 0; i < currentResourceCount.length; i++) { 
            		finalResourceValues[i] = (int) (resourceSum * (targetResourcePercentage[i] / 100f));
            	}
            	
            	System.out.println("Calculated values prior to NPC balancing:\nWood: " + finalResourceValues[0] + " Clay: " + finalResourceValues[1] + " Iron: " + finalResourceValues[2] + " Crop: " + finalResourceValues[3]);            	
        		System.out.println("Entering resource values");
            	WebElement woodField = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='npc']/tbody/tr[1]/td[1]/input")));           	
            	woodField.clear();
            	woodField.sendKeys(String.valueOf(finalResourceValues[0]));
            	
            	WebElement brickField = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='npc']/tbody/tr[1]/td[2]/input")));           	
            	brickField.clear();
            	brickField.sendKeys(String.valueOf(finalResourceValues[1]));
            	
            	WebElement rockField = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='npc']/tbody/tr[1]/td[3]/input")));    	
            	rockField.clear();
            	rockField.sendKeys(String.valueOf(finalResourceValues[2]));
            	
            	WebElement cropField = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='npc']/tbody/tr[1]/td[4]/input")));        	
            	cropField.clear();
            	cropField.sendKeys(String.valueOf(finalResourceValues[3]));           	
            	Thread.sleep(500);           	
            	System.out.println("Balancing");
            	executor.executeScript("exchangeResources.distribute(" + villageID + ");");
            	           	
            	/*System.out.println("Balancing");
            	WebElement balanceButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitText']/button")));           	
            	balanceButton.click();
            	*/

            	Thread.sleep(500);
            	
            	System.out.println("Exchanging!");
            	executor.executeScript("jQuery(window).trigger('buttonClicked', [this, {\r\n" + 
            			"                    \"wayOfPayment\": {\r\n" + 
            			"                        \"featureKey\": \"marketplace\",\r\n" + 
            			"                        \"context\": \"\",\r\n" + 
            			"                        \"dataCallback\": \"returnInputValues\"\r\n" + 
            			"                    }\r\n" + 
            			"                }]);");
            	
            	/*WebElement exchangeButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitButton']/button")));           	
            	exchangeButton.click();          
            	*/
            	
            	Thread.sleep(500);
            	Travian.driver.navigate().refresh();
            	return "";
            } catch (InterruptedException e) {
				e.printStackTrace();
				return "NPCtrade InterruptedException";
			}
		} else {
			System.out.println("You don't have enough gold on your account! (>3)");
			return "You don't have enough gold on your account! (>3)";
		}
	}
}
