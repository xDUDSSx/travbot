package dudss.travian.lang;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class LanguageUtils {
	HashMap<String, Integer> template;
	
	String[] langSheet;
	int langCount = 5;
	int langSize = 72;
	int sheetSize = langCount * langSize;
	
	String[] currentSheet;
	
	public int selectedLanguage = 0;
	
	public LanguageUtils() {
		template = new HashMap<String, Integer>();
		int n = 0;
		template.put("English", n++);
		template.put("Settings", n++);
		template.put("Username", n++);
		template.put("Password", n++);
		template.put("Login", n++);
		template.put("Minimize to tray", n++);
		template.put("Refresh farmlists", n++);
		template.put("Farmlist name", n++);
		template.put("Interval", n++);
		template.put("MIN", n++);
		template.put("MAX", n++);
		template.put("Last sent", n++);
		template.put("Profile", n++);
		template.put("How to use?", n++);
		template.put("Donate", n++);
		template.put("Quit", n++);
		template.put("TravBot settings", n++);
		template.put("General", n++);
		template.put("Farmlist sender", n++);
		template.put("Farm adder", n++);
		template.put("Crop checker", n++);
		template.put("Proxy", n++);
		template.put("Set server:", n++);
		template.put("Use useragent", n++);
		template.put("Set useragent: (you can get it here for example: https://www.whatsmyua.info)", n++);
		template.put("Global WebElement wait timeout", n++);
		template.put("Override current working directory", n++);
		template.put("Generate console log", n++);
		template.put("Reopen console", n++);	
		template.put("Language select", n++);
		template.put("Open", n++);
		template.put("Exit", n++);
		template.put("Advanced", n++);
		template.put("Include natars", n++);
		template.put("Max pop difference:", n++);
		template.put("Enable antiNoob module (3 days minimum playtime)", n++);
		template.put("Create new FL (100+)", n++);
		template.put("<-- Use existing FL or  -->", n++);
		template.put("Range:", n++);
		template.put("In village: ", n++);
		template.put("number of troops:", n++);
		template.put("Start", n++);
		template.put("Username not set!", n++);
		template.put("Server not set!", n++);
		template.put("Password not set!", n++);
		template.put("UserAgent not set!", n++);
		template.put("Use proxy", n++);
		template.put("Proxy server ( ip : port)", n++);
		template.put("Proxies that require authentication will probably not work.", n++);
		template.put("Farm adder delays - if farm adder reports -element not found- errors you can edit delays here", n++);
		template.put("Stop farm sending when:", n++);
		template.put("Attacker lost", n++);
		template.put("Attacker won with a loss", n++);
		template.put("Delays will directly affect farmlist sending speed", n++);
		template.put("(All data is saved on login)", n++);
		template.put("Save data", n++);
		template.put("Delays will directly affect FarmAdder speed", n++);
		template.put("Target village:", n++);
		template.put("Underflow", n++);
		template.put("Exchange when crop is below:", n++);
		template.put("Overflow", n++);
		template.put("Exchange when crop is above:", n++);
		template.put("Wood:", n++);
		template.put("Clay:", n++);
		template.put("Iron:", n++);
		template.put("Crop:", n++);
		template.put("Resources will get balanced by the NPC trader", n++);
		template.put("You can set a resource to 9999999 to trade everything else into it", n++);
		template.put("Crop checker polling rate (sec)", n++);
		template.put("These settings are applied during runtime and should be set AFTER logging in!", n++);
		template.put("Farmlist sender custom delays:", n++);
		template.put("Logged in as", n++);
		
		langSheet = new String[sheetSize];
		currentSheet = new String[langSize];
		
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get("metadata/languageSheet.csv"));
			CSVParser csvParser = CSVParser.parse(reader, CSVFormat.DEFAULT);
			java.util.List<CSVRecord> records = csvParser.getRecords();
			records.remove(0);
			records.remove(0);
			
			for (int i = 0; i < records.size(); i++) {
				CSVRecord r = records.get(i);			
				for (int y = 0; y < r.size(); y++) {
					langSheet[y*langSize + i] = r.get(y);
				}	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setLanguage(selectedLanguage);
	}
	
	public String getPhraseFor(String s) {
		if (s.equals("")) return "";
		
		int index = -1;
		for (int i = 0; i < langSheet.length; i++) {
			if (langSheet[i].equals(s)) {
				index = i;
			}
		}
		if (index == -1) return s;
		index %= this.langSize;
		if (template.get(langSheet[index]) == null) return s;
		
		String t = langSheet[selectedLanguage*langSize + template.get(langSheet[index])];		
		if (t == null || t.equals("")) {
			if (!langSheet[index].equals("") || langSheet[index] == null) {
				return langSheet[index];
			} else {
				return s;
			}
		} else {
			return t;
		}
	}
	
	public String getTemplatePhraseFor(String s) {
		if (s.equals("")) return "";
		
		int index = -1;
		for (int i = 0; i < langSheet.length; i++) {
			if (langSheet[i].equals(s)) {
				index = i;
			}
		}
		if (index == -1) return s;
		index %= this.langSize;
		if (template.get(langSheet[index]) == null) return s;
		
		String t = langSheet[0*langSize + template.get(langSheet[index])];		
		if (t == null || t.equals("")) {
			if (!langSheet[index].equals("") || langSheet[index] == null) {
				return langSheet[index];
			} else {
				return s;
			}
		} else {
			return t;
		}
	}
	
	public void setLanguage(int i) {
		this.selectedLanguage = i;	
		for (int x = 0; x < langSize; x++) {
			currentSheet[x] = langSheet[selectedLanguage*langSize + x];
		}
	}
}
