package dudss.travian.autoevade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.SwingUtilities;

import dudss.travian.Settings;
import dudss.travian.Travian;

public class AutoEvader {
	static Timer t;

	static Map<String, ScheduledEvasion> evasions = new HashMap<String, ScheduledEvasion>();
	
	public static boolean running = false;
	
	public static List<EvadeVillageEntry> evadeEntries = new ArrayList<EvadeVillageEntry>();
	
	public static void refreshSettings() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (int i = 0; i < Travian.villagenames2.size(); i++) {
					AutoEvader.evadeEntries.add(new EvadeVillageEntry(Travian.villagenames2.get(i)));
				}
				
				Settings.autoEvadePanel.removeAll();
				
				for (EvadeVillageEntry e : evadeEntries) {
					Settings.autoEvadePanel.add(e, "wrap");	
				}
								
				Settings.autoEvadePanel.revalidate();
				Settings.autoEvadePanel.repaint();
				Settings.autoEvadeScrollPane.setViewportView(Settings.autoEvadePanel);			
				Settings.autoEvadeScrollPane.revalidate();
				Settings.autoEvadeScrollPane.repaint();
			}	
		});
	}
	
	public static void start() {
		if (!running) {
			System.out.println("Starting AutoEvade! Delay: " + Long.parseLong(Settings.autoEvadeScanInterval.getText()) * 1000);
			t = new Timer();
			t.schedule(new EvadeListener(t), Long.parseLong(Settings.autoEvadeScanInterval.getText()) * 1000);
			running = true;
		}
	}
	
	public static void stop() {
		if (running) {
			System.out.println("Stopping AutoEvade!");
			t.cancel();
			t.purge();
			running = false;
		}
	}
}
