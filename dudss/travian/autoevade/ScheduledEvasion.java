package dudss.travian.autoevade;

import java.util.Timer;
import java.util.TimerTask;

import dudss.travian.Travian;
import dudss.travian.autoevade.TroopSender.AttackType;

public class ScheduledEvasion {
	
	int startTime;
	int attackTime;
	int evasionTime;
	
	int delay;
	int evadeTime;
	String village;
	Timer timer;
	
	public ScheduledEvasion(int inSeconds, int evadeAtSecondsRemaining, String village) {
		delay = inSeconds;
		evadeTime = evadeAtSecondsRemaining;
		this.village = village;
	
		timer = new Timer();
		
		if (evadeAtSecondsRemaining > inSeconds) {
			evadeAtSecondsRemaining = inSeconds;
		}
		
		System.out.println("(" + Travian.ts() + ") Scheduling evasion at " + village + " in " + (((inSeconds*1000) - (evadeAtSecondsRemaining*1000))/1000) + " seconds!");
		
		startTime = (int) (System.currentTimeMillis() / 1000);
		attackTime = startTime + delay;
		evasionTime = attackTime - evadeAtSecondsRemaining;
		
		timer.schedule(new TimerTask() {
		@Override
			public void run() {
				synchronized (Travian.driverInUse) {      		
		        	Travian.driverInUse.set(true);
					if (AutoEvader.running) {
						System.out.println("\n\n------- !! EVADING ATTACK !! -------");
						System.out.println("Evading in " + village + " at " + Travian.ts());
						
						EvadeVillageEntry entry = AutoEvader.evadeEntries.get(Travian.villagenames2.indexOf(village));
						
						if (entry.xField.getText() == "" || entry.xField.getText() == null || entry.yField.getText() == "" || entry.yField.getText() == null) {
							System.err.println("CANNOT send troops away! Target village evade coordinates NOT set!");
						} else {					
							int x = Integer.parseInt(entry.xField.getText());
							int y = Integer.parseInt(entry.yField.getText());
							
							AttackType attackType = null;
							
							switch(entry.attackType.getSelectedIndex()) {
								case 0: attackType = AttackType.NORMAL; 
								break;
								case 1: attackType = AttackType.RAID;
								break;
								case 2: attackType = AttackType.SUPPORT; 
								break;
							}
							
							System.out.println("Starting TroopSender!");
							TroopSender.sendAllTroopsTo(x, y, attackType, village);
						}
					} else {
						System.out.println("Evasion in " + village + " canceled because AutoEvader is OFF!");
					}
					AutoEvader.evasions.remove(village);   		
			        Travian.driverInUse.set(false);
				}
			}
		}, (inSeconds*1000) - (evadeAtSecondsRemaining*1000));
	}
	
	public void stop() {
		timer.cancel();
		timer.purge();
	}
}
