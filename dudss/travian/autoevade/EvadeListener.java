package dudss.travian.autoevade;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dudss.travian.BotFrame;
import dudss.travian.Settings;
import dudss.travian.Travian;

public class EvadeListener extends TimerTask {
	
	Timer t; 

	EvadeListener(Timer t) {
		super();
		this.t = t;
	}
	
	@Override
	public void run() {
		synchronized (Travian.driverInUse) {      		
        	Travian.driverInUse.set(true);
        	System.out.println("\n[AutoEvade (delay: " + Long.parseLong(Settings.autoEvadeScanInterval.getText()) * 1000 + ") " + Thread.currentThread()+ "] Scanning ... at " + Travian.ts());
        	System.out.println("Current driver page: " + Travian.driver.getCurrentUrl());       
        	WebDriverWait wait = new WebDriverWait(Travian.driver, BotFrame.timeout);	
        	
        	Travian.driver.get("https://" + BotFrame.frameServer + "/dorf1.php");
        	for (String village : Travian.villagenames2) {
        		Travian.switchVillage(village);
        		
        		//Waiting for the movements table	
        		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='map_details']/div[1]")));
        		List<WebElement> attackElement = Travian.driver.findElements(By.xpath("//*[@class='a1']"));
        		if (attackElement.size() > 0) {
        			System.out.println("\n!-- -- An attack detected in: " + village + " -- --!");
        			WebElement attackCountdown = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@class='a1']/../../div[@class='dur_r']/span"))));
        			int countdownSeconds = Integer.parseInt(attackCountdown.getAttribute("value"));
        			System.out.println("Attack arriving in " + countdownSeconds + " seconds!");
        			
        			if (!AutoEvader.evasions.containsKey(village)) {
	        			int evadeTime = Integer.parseInt((Settings.autoEvadeDelay.getText()));
	        			System.out.println("SCHEDULING NEW EVASION");
	        			AutoEvader.evasions.put(village, new ScheduledEvasion(countdownSeconds, evadeTime, village));
        			} else {
        				System.out.println("Evasion already scheduled ... checking for new attacks");
        				int evadeTime = Integer.parseInt((Settings.autoEvadeDelay.getText()));
        				if (evadeTime > countdownSeconds) {
        					evadeTime = countdownSeconds;
        				}
        				int currentTime = (int) (System.currentTimeMillis() / 1000);      		
        			
        				int timeToScheduledEvasion = AutoEvader.evasions.get(village).evasionTime - currentTime;
        				int timeToNewEvasion = countdownSeconds - evadeTime;
        				
        				if (timeToNewEvasion < timeToScheduledEvasion) {
        					System.out.println("Countdown change detected! (" + timeToNewEvasion + " x " + timeToScheduledEvasion + ")");
        					System.out.println("RESCHEDULING EVASION");
        					AutoEvader.evasions.get(village).stop();
        					AutoEvader.evasions.put(village, new ScheduledEvasion(countdownSeconds, evadeTime, village));
        				}
        			}
        		} else {
        			System.out.println("No attacks detected in: " + village);
        		}
        	}
        	
        	//scan all villages for incoming attacks
        	//Send all troops away if an attack is detected
        	//The destination coordinates can be found in the evadeEntries of AutoEvader
        	
	    	t.schedule(new EvadeListener(t), Long.parseLong(Settings.autoEvadeScanInterval.getText()) * 1000);
	    	Travian.driverInUse.set(false);
		}	
	}
}
