package dudss.travian.autoevade;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dudss.travian.BotFrame;
import dudss.travian.IconListRenderer;
import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;

import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class EvadeVillageEntry extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public JTextField xField;
	public JTextField yField;
	public JLabel lblX;
	public JLabel lblY;
	public JComboBox<Object> attackType;
	
	public EvadeVillageEntry(String name) {
		setLayout(new MigLayout("", "[][][][grow]", "[]"));
		
		JLabel villageName = new JLabel(name);
		add(villageName, "cell 0 0,alignx left,aligny center");
		
		lblX = new JLabel("X:");
		add(lblX, "flowx,cell 1 0,alignx leading,aligny center");
		
		xField = new JTextField();
		add(xField, "cell 1 0,alignx left,aligny center");
		xField.setColumns(10);
		
		lblY = new JLabel("Y:");
		add(lblY, "flowx,cell 2 0,alignx left");
		
		yField = new JTextField();
		add(yField, "cell 2 0,alignx left,aligny center");
		yField.setColumns(10);
		
		Map<Object, Icon> attackIcons = new HashMap<Object, Icon>();
		attackIcons.put("Attack: Normal", new ImageIcon(BotFrame.class.getResource("/res/attackIcon.png")));
		attackIcons.put("Attack: Raid", new ImageIcon(BotFrame.class.getResource("/res/raidIcon.png")));
		attackIcons.put("Support", new ImageIcon(BotFrame.class.getResource("/res/def1.gif")));
		
		attackType = new JComboBox<Object>(new Object[] { "Attack: Normal", "Attack: Raid", "Support"});
		attackType.setRenderer(new IconListRenderer(attackIcons));		    	
		add(attackType, "cell 3 0,alignx trailing,aligny center");
	}

}
