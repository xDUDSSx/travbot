package dudss.travian.autoevade;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import dudss.travian.BotFrame;
import dudss.travian.Travian;

public class TroopSender {
	public static enum AttackType {
		NORMAL, RAID, SUPPORT
	}
	
	public static void sendAllTroopsTo(int x, int y, AttackType attackType, String village) {
		System.out.println("Sending ALL troops to (" + x + ":" + y + ") AttackType: " + attackType.toString() + " Village: " + village);
		Travian.switchVillage(village);
		Travian.driver.get("https://" + BotFrame.frameServer + "/build.php?id=39");		
		try {
			try {
				Travian.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='build.php?tt=2&id=39']")));
			} catch (TimeoutException e) {
				System.err.println("The village " + village  + " from which the troops are supposed to get sent does NOT have a rally point built!");
			}
			Travian.driver.get("https://" + BotFrame.frameServer + "/build.php?tt=2&id=39");
			
			System.out.println("Entering troop values");
			JavascriptExecutor executor = (JavascriptExecutor) Travian.driver;
			for (int i = 1; i < 11; i++) {
				executor.executeScript("document.snd.t" + i + ".value = 99999999;");
			}
			System.out.println("Setting coordinates");
			executor.executeScript("document.snd.x.value = " + x + ";");
			executor.executeScript("document.snd.y.value = " + y + ";");
			
			System.out.println("Setting attack type");
			WebElement support = Travian.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='option']/label[1]/input")));
			WebElement attack = Travian.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='option']/label[2]/input")));
			WebElement raid = Travian.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='option']/label[3]/input")));
			
			switch(attackType) {
				case NORMAL: attack.click(); break;
				case RAID: raid.click(); break;
				case SUPPORT: support.click(); break;
			}
			System.out.println("Confirming selection");
			Travian.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btn_ok']"))).click();;
			Thread.sleep(500);
			System.out.println("Sending!");
			Travian.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btn_ok']"))).click();;
		} catch (Exception e) {
			System.err.println("Could not send troops from " + village + "!");
			e.printStackTrace();
		}
	}
}
