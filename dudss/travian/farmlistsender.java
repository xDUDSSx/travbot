package dudss.travian;

import java.awt.EventQueue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FarmListSender  {
	
    private String id;
    private int interval;
    String name;
    private int index;
    private String deviationMIN;
    private String deviationMAX;
	Boolean running = false;
	private WebDriverWait wait;
	Actions actions;
	
	ArrayList<String> disabledfarms;
	
	public ArrayList<Double> profile;
	
	FarmListSender() {
		//Default profile load
		profile = new ArrayList<Double>();
		for(int i = 0; i < 24; i++) {
			profile.add(1.0);
		}
	}
	
	public void setvalues(String id, int interval, String name, int index, String deviatonMIN, String deviationMAX) {
		this.id = id;
		this.interval = interval;
		this.name = name;
		this.index = index;
		this.deviationMIN = deviatonMIN;
		this.deviationMAX = deviationMAX;
		
		disabledfarms = new ArrayList<String>();
		if(disabledfarms.size() != 0) {
			disabledfarms.clear();
		}
		
		actions = new Actions(Travian.driver);
	}
	
	public void setProfile(ArrayList<Double> values) {
		for (int i = 0; i < 24; i++) {
			profile.set(i, values.get(i));
		}
	}
	
	private Timer timer;
	
	private int t_tick = 0;
	
	class TaskFLS extends TimerTask {
        public void run() {    
        	synchronized (Travian.driverInUse) {      		
	        	Travian.driverInUse.set(true);
	        	
        		//Delay
	        	int delay = 10000;
	        	while(delay < 30000) {
	        		delay = (ThreadLocalRandom.current().nextInt((interval*60*1000) - (Integer.valueOf(deviationMIN)*60*1000 + 1), (interval*60*1000) + (Integer.valueOf(deviationMAX)*60*1000 + 1)));
	        	}
	        	
	        	System.out.println("\n*" + id + " START\n* \n* \n*_____SendTimer(Thread: " + Thread.currentThread() + ") - at id: " + id + " ~interval: " + interval + " min _____*");
	        	
	        	//Profiler edits
	        	Date date = new Date();
	        	Calendar calendar = GregorianCalendar.getInstance();
	        	calendar.setTime(date);   
	        	
	        	int currentHourOfDay = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
	
	        	double currentProfilerFactor = 1.0;
	        	if (currentHourOfDay == 0) {
	        		currentProfilerFactor = profile.get(23);
	        	} else {
	        		currentProfilerFactor = profile.get(currentHourOfDay - 1);
	        	}
	        	
	        	System.out.println("Profiler factor: " + currentProfilerFactor + " Hour of day: " + currentHourOfDay);
	        	
	        	//Dividing delay with profiler factor
	        	System.out.println("Original delay: " + delay);
	        	delay = (int) Math.round(delay / currentProfilerFactor);
	        	System.out.println("New delay: " + delay);
	        	
	        	System.out.println("Time range of: " + (Integer.valueOf(interval) - Integer.valueOf(deviationMIN)) + " min to " + (Integer.valueOf(interval) + Integer.valueOf(deviationMAX)) + " min");
	        	double tempmin = (double)delay/60/1000;
	        	double tempsec = (double)delay/1000;
	        	System.out.println("interval of " + delay + " ms (" + round(tempmin,2) + " min, " + round(tempsec,2) + " sec)");
	  
	            System.out.println(Travian.ts() + "\n --- Sending troops now ...");
	            
	            BotFrame.model.setValueAt(new Timestamp(new java.util.Date().getTime()),index, 5);  	
	            /*while(!Travian.driverInUse.compareAndSet(false, true)) {
	    			try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}		
	    			
	    			System.out.println("FLSender thread: " + Thread.currentThread() + " (" + id + ") waiting for driver access (5 sec) ...");
	    		}
	    		*/
	            
	            //Variable currently used for captcha detection (Unless captcha is detected, sendtroops will always return true)
	            Boolean sendingSuccess = false;
				try {
					sendingSuccess = sendtroops();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(sendingSuccess == true) {				
					timer.schedule(new TaskFLS(), delay);
			        t_tick++;
			        System.out.println("Timer" + id + " tick n." + t_tick);
				} else {
					System.out.println("-- Shutting down FarmListSender " + id +" - " + name + " --");
				
					//Uncheck the checkbox for this farmlist row (BotFrame table listener will pause this FL accordingly)
					try {
						EventQueue.invokeAndWait(new Runnable() {
						    @Override
						    public void run() {
								BotFrame.table.getModel().setValueAt(false, index, 0);
						    }
						  });
					} catch (InvocationTargetException | InterruptedException e) {
						e.printStackTrace();
					}	
				}
				System.out.println("*\n*" + id + " END");
				Travian.driverInUse.set(false);
        	}  
        }
    }
	
	TaskFLS fls = new TaskFLS();
	
    void send() {
    	timer = new Timer();
    
    	timer.schedule(new TaskFLS(), 0);
		running = true;
	}
	
    void pause() throws InterruptedException {
    	timer.cancel();
    	
    	running = false;
    }
    
    void restart() {
    	System.out.println("restarting ... at " + id);
    	timer.cancel();
    	running = false;
    	timer = new Timer();
    	timer.schedule(new TaskFLS(), 2000);
    	running = true;
    }
    
	Boolean sendtroops() throws InterruptedException {	
		Boolean sendingSuccess = false;
		try {			
			Travian.driver.get(Travian.travian_url_farm);
			
			if(Travian.detectCaptcha(Travian.driver) == true) {
				System.out.println("-- Travian captcha detected! --");
				sendingSuccess = false;
				return sendingSuccess;
			}
			
			System.out.println(Travian.driver.getCurrentUrl());
			wait = new WebDriverWait(Travian.driver, BotFrame.timeout);	
			
			Thread.sleep(BotFrame.FARMLISTSENDER_INIT_DELAY);
		
			try {
					Boolean listcontentIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[2]", 10, 500);
					if (listcontentIsPresent == false) {
						System.err.println("Could not find farmlist id: " + id + " - could not find element");
					} 
					WebElement listcontent = Travian.driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[2]"));
					
					if (listcontent.getAttribute("class").equals("listContent hide")) {
						System.out.println("Expanding fl ...");
						Boolean expanderIsPresent = FarmListEdit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[1]/div[1]/div/div", 10, 500, 1);
						if (expanderIsPresent == false) {
							System.err.println("Farmlist expansion failed - could not find element");	
						}					
						WebElement expand = Travian.driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[1]/div[1]/div/div"));	
						Boolean loadingimgIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[1]/div[2]/img", 10, 500);
						if (loadingimgIsPresent == false) {
							System.err.println("Could not find loading img");
						} 
						WebElement loadingimg = Travian.driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[1]/div[2]/img"));
						actions.moveToElement(expand).click().build().perform();
						Thread.sleep(BotFrame.FARMADDER_EXPAND_DELAY);
						if(loadingimg.getAttribute("class").equals("loading")) {
							while(loadingimg.getAttribute("class").equals("loading")) {
								System.out.println("FL loading, polling rate: 100");
								Thread.sleep(100);
							}
						}
						if(loadingimg.getAttribute("class").equals("loading hide")) {
							System.out.println("Loading finished");
						}
					}
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("raidListMarkAll" + id)));
					assertAndVerifyElement(By.id("raidListMarkAll" + id));
					WebElement check = Travian.driver.findElement(By.id("raidListMarkAll" + id));
					
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list" + id + "']/form/div[2]/button"));
					assertAndVerifyElement(By.xpath("//*[@id='list" + id + "']/form/div[2]/button"));
					WebElement send = Travian.driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[2]/button"));
			
					System.out.println(name + " found!");
								
					Thread.sleep(BotFrame.FARMLISTSENDER_CHECK_DELAY);
					wait.until(ExpectedConditions.elementToBeClickable(check));	
					
					JavascriptExecutor executor = (JavascriptExecutor) Travian.driver;
					executor.executeScript("arguments[0].click();", check);
					//actions.moveToElement(check).click().build().perform();
					
					//LOOP THROUGH TABLE AND DISABLE RED/ORANGE ATTACKS ---
					if (Settings.attack_lost_box.isSelected() == true || Settings.attack_win_loss_box.isSelected() == true) {
						uncheckFarms(id);
					}
					
					Thread.sleep(BotFrame.FARMLISTSENDER_SEND_DELAY);
					wait.until(ExpectedConditions.elementToBeClickable(send));
					executor.executeScript("arguments[0].click();", send);
					
					//actions.moveToElement(send).click().build().perform();
					
					File logfile = null;
					
					try {						
						logfile = new File(BotFrame.workingDirectoryPath + "/log.txt");						
					} catch (Exception e) {System.out.println("Exception at sender def");}				
						
						try {		
							FileWriter logfw = new FileWriter(logfile, true);		
							BufferedWriter logbw = new BufferedWriter(logfw);		
							PrintWriter out = new PrintWriter(logbw);
							
							out.println(id + "  " + Travian.logdateforamt.format(new Timestamp(new java.util.Date().getTime())));
							
							out.close();
							logbw.close();
							logfw.close();
							
						} catch (IOException e) {
							System.out.println("LogFile exception");
							e.printStackTrace();
						}			
							
						System.out.println(name + " sent!");		
						
					} catch (NoSuchElementException e)
					{
						System.err.println("%%" + name + " not found (NoSuchElementException)%%");
						restart();
					}
		} catch (StaleElementReferenceException e) {
			System.err.println("%%%%%%%%%%%%%%%%%%% StaleElementReferenceException %%%%%%%%%%%%%%%%%%%"
					+ "\nRestarting TimerTask at Timer" + id + " FL: " + name + "Interval: " + interval + " deviations(MIN/MAX): " + deviationMIN + "/" + deviationMAX);
			restart();
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	System.out.print("-10 sec wait-");
	    	Thread.sleep(10000);
	    	restart();
	    }
		sendingSuccess = true;
		return sendingSuccess;		 
	}
	
	// iReport iReport1 == WIN
	// iReport iReport2 == WIN with LOSS
	// iReport iReport3 == LOSS
	 
	void uncheckFarms(String id) {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list" + id + "']/form/div[2]/div[1]/table/tbody/tr")));
		List<WebElement> rows = Travian.driver.findElements(By.xpath("//*[@id='list" + id + "']/form/div[2]/div[1]/table/tbody/tr"));
		
		for(WebElement row : rows) {
			String rowid = row.getAttribute("id");  // slot-row-110561 ->
			String idonly = rowid.replace("-row-", ""); // -> slot110561
			
			//Check if last attack icon is present
			boolean exists = Travian.driver.findElements(By.xpath("//*[@id='" + rowid + "']/td[6]/img")).size() != 0;

			//execute accordingly
			if(exists == true) {
				WebElement reportimage = Travian.driver.findElement(By.xpath("//*[@id='" + rowid + "']/td[6]/img"));
				
				String imgclass = reportimage.getAttribute("class");
				
				if (imgclass.equals("iReport iReport1")) {
				} else
				if (imgclass.equals("iReport iReport2") && Settings.attack_win_loss_box.isSelected() == true) {
					WebElement e = Travian.driver.findElement(By.xpath("//*[@id='"+ idonly + "']"));
					actions.moveToElement(e).click().build().perform();
				} else
				if (imgclass.equals("iReport iReport3") && Settings.attack_lost_box.isSelected() == true) {
					WebElement e = Travian.driver.findElement(By.xpath("//*[@id='"+ idonly + "']"));
					actions.moveToElement(e).click().build().perform();
				}
			}
		}
	}
	 
	 
	public void assertAndVerifyElement(By element) throws InterruptedException {
	        boolean isPresent = false;

	        for (int i = 0; i < 20; i++) {
	            try {
	                if (Travian.driver.findElement(element) != null) {
	                    isPresent = true;
	                    break;
	                }
	            } catch (Exception e) {
	                System.out.println(e.getLocalizedMessage());
	            	System.out.println("assert failed");
	                Thread.sleep(1000);
	            }
	        }
	        if(isPresent == false) {
	        	System.out.println("\"" + element + "\" is not present.");
	        	System.out.println("%%%% AssertError - restarting timer at: " + id + " %%%%");
	        	restart();
	        }
	    } 
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}

/**
 *  � 2017 Dan Raku�an. All rights reserved.
 */