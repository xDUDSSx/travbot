package dudss.travian;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.io.FileUtils;

import java.awt.Color;
import java.awt.Desktop;
import javax.swing.border.EtchedBorder;
import javax.swing.JCheckBox;

public class Profiler extends JFrame {

	private JPanel contentPane;
	
	public int index;
	
	public Profiler(int index) {
		this.index = index;
	
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Profiler.class.getResource("/res/travicon.png")));
		setTitle("TravBot - Profiler");
		setBounds(100, 100, 645, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][]", "[][][grow]"));
		
		JLabel lblFarmlist = new JLabel("Farmlist:");
		contentPane.add(lblFarmlist, "cell 0 0");
		
		JComboBox flComboBox = new JComboBox();
		contentPane.add(flComboBox, "cell 1 0,growx");
		
		JSliderGraph graph = new JSliderGraph(600,270);
		graph.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(graph, "cell 0 1 2 1,growx");
		
		JPanel panel = new JPanel();
		contentPane.add(panel, "cell 0 2 2 1,grow");
		panel.setLayout(new MigLayout("insets 0", "[40%][20%][40%]", "[][][]"));
		
		JButton btnDefaultProfile = new JButton("Default profile");
		btnDefaultProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Double> values = new ArrayList<Double>();
				for(int i = 0; i < 23; i++) {
					values.add(1.0);
				}
				graph.sliderGraph.setValues(values);
			}
		});
		panel.add(btnDefaultProfile, "cell 0 0,growx");
		
		JCheckBox chckbxShowValues = new JCheckBox("Show values");
		ActionListener actionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {		
				AbstractButton abstractButton = (AbstractButton) e.getSource();
				boolean selected = abstractButton.getModel().isSelected();
		    	graph.sliderGraph.showValues(!graph.sliderGraph.showValues);	
			}
		};
		
		chckbxShowValues.addActionListener(actionListener);
		
		panel.add(chckbxShowValues, "cell 1 0");
		
		JButton btnSave = new JButton("Save profile to file");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File directory = new File(System.getProperty("user.dir") + "/profiles");
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setDialogTitle("Save profile to file");
				jfc.setAcceptAllFileFilterUsed(false);
				
				File workingDirectory = new File(System.getProperty("user.dir") + "/profiles");
				jfc.setCurrentDirectory(workingDirectory);
				
				FileNameExtensionFilter filter = new FileNameExtensionFilter("TravBot profile file", "profile");
				jfc.addChoosableFileFilter(filter);

				int returnValue = jfc.showSaveDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File savedProfile = new File(jfc.getSelectedFile().getPath() + ".profile");
						
					try {
						ArrayList<Double> values = new ArrayList<Double>();
						values = graph.sliderGraph.getValues();
						
						StringBuilder strBuilder = new StringBuilder();
						for(int i = 0; i < values.size(); i++) {
							strBuilder.append(String.valueOf(values.get(i)) + "*");			
						}
						
						FileUtils.writeStringToFile(savedProfile, strBuilder.toString());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
			}
		});
		panel.add(btnSave, "cell 2 0,growx");
		
		JButton btnDaynightProfile = new JButton("Day/Night profile");
		btnDaynightProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File(System.getProperty("user.dir") + "/profiles" + "/daynightprofile.profile");
				String data = "";
				try {
					data = FileUtils.readFileToString(file);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.out.println("Data: " + data);					
				String[] chunks = data.split("\\*");
				ArrayList<Double> values = new ArrayList<Double>();
				
				try {					
					for(int i = 0; i < 23; i++) {
						values.add(Double.parseDouble(chunks[i]));
					}
					graph.sliderGraph.setValues(values);
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
					System.err.println("\nCannot load profile - Invalid PROFILE format!");
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							JOptionPane.showMessageDialog(null ,"Cannot load profile - Invalid PROFILE format!", "Profile load error", JOptionPane.ERROR_MESSAGE);
						}
					});
				}
			}
		});
		panel.add(btnDaynightProfile, "cell 0 1,growx");
		
		JButton btnLoad = new JButton("Load profile from file");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File directory = new File(System.getProperty("user.dir") + "/profiles");
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setDialogTitle("Load profile from file");
				jfc.setAcceptAllFileFilterUsed(false);
				
				File workingDirectory = new File(System.getProperty("user.dir") + "/profiles");
				jfc.setCurrentDirectory(workingDirectory);
				
				FileNameExtensionFilter filter = new FileNameExtensionFilter("TravBot profile files", "profile");
				jfc.addChoosableFileFilter(filter);

				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File file = new File(jfc.getSelectedFile().getPath());
					String data = "";
					try {
						data = FileUtils.readFileToString(file);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					System.out.println("Data: " + data);					
					String[] chunks = data.split("\\*");
					ArrayList<Double> values = new ArrayList<Double>();
					
					try {					
						for(int i = 0; i < 23; i++) {
							values.add(Double.parseDouble(chunks[i]));
						}
						graph.sliderGraph.setValues(values);
					} catch (NumberFormatException ex) {
						ex.printStackTrace();
						System.err.println("\nCannot load profile - Invalid PROFILE format!");
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								JOptionPane.showMessageDialog(null ,"Cannot load profile - Invalid PROFILE format!", "Profile load error", JOptionPane.ERROR_MESSAGE);
							}
						});
					}
					
				}
			}
		});
		
		JButton btnOpenProfileDir = new JButton("Open profile DIR");
		btnOpenProfileDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(new File(System.getProperty("user.dir") + "/profiles"));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnOpenProfileDir.setForeground(new Color(255, 140, 0));
		btnOpenProfileDir.setBorderPainted(false); 
		btnOpenProfileDir.setContentAreaFilled(false); 
		btnOpenProfileDir.setFocusPainted(false); 
		btnOpenProfileDir.setOpaque(false);
		panel.add(btnOpenProfileDir, "cell 1 1");
		
		panel.add(btnLoad, "cell 2 1,growx");
		
		JLabel lblCopyFrom = new JLabel("Copy profile from:");
		panel.add(lblCopyFrom, "flowx,cell 0 2");
		
		JButton btnCopy = new JButton("Copy");
		panel.add(btnCopy, "cell 1 2");
		
		JButton btnSetProfile = new JButton("Set profile to farmlist!");
		btnSetProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Travian.fllist.get(flComboBox.getSelectedIndex()).setProfile(graph.sliderGraph.getValues());
				System.out.println("Profile for " + flComboBox.getSelectedItem().toString() + " set!");
			}
		});
		panel.add(btnSetProfile, "cell 2 2,growx");
		
		JComboBox copyComboBox = new JComboBox();
		panel.add(copyComboBox, "cell 0 2,growx");
		
		for(int i = 0; i < Travian.farmlists_names2.size(); i++) {	
			flComboBox.addItem(Travian.farmlists_names2.get(i));
			copyComboBox.addItem(Travian.farmlists_names2.get(i));
		}
		flComboBox.setSelectedIndex(index);
		copyComboBox.setSelectedIndex(index);
		
		flComboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        JComboBox combo = (JComboBox) e.getSource();		       
		        int innerIndex = combo.getSelectedIndex();
		        
		        graph.sliderGraph.setValues(Travian.fllist.get(innerIndex).profile);        
		    }
		});
		
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int copyIndex = copyComboBox.getSelectedIndex();
				graph.sliderGraph.setValues(Travian.fllist.get(copyIndex).profile);
			}
		});
		
		//Loading values of initial FL
		graph.sliderGraph.setValues(Travian.fllist.get(index).profile);
	}
		void parseFile() {
			
		}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */