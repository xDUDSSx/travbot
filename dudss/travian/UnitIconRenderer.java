package dudss.travian;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.UIManager;

public class UnitIconRenderer extends DefaultListCellRenderer {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, ImageIcon> iconMap = new HashMap<>();
	  private Color background = new Color(255, 148, 0, 15);
	  private Color defaultBackground = (Color) UIManager.get("List.background");

	  public UnitIconRenderer() {
	      iconMap.put("clubswinger", new ImageIcon(getClass().getResource("/res/clubswinger.png")));
	      iconMap.put("spearman", new ImageIcon(getClass().getResource("/res/spearman.gif")));
	      iconMap.put("axeman", new ImageIcon(getClass().getResource("/res/axeman.gif")));
	      iconMap.put("paladin", new ImageIcon(getClass().getResource("/res/druid.gif")));
	      iconMap.put("teutonic_knight", new ImageIcon(getClass().getResource("/res/teutonic_knight.gif")));
	      iconMap.put("ram", new ImageIcon(getClass().getResource("/res/ram.gif")));
	      iconMap.put("catapult", new ImageIcon(getClass().getResource("/res/catapult.gif")));
	      iconMap.put("administrator", new ImageIcon(getClass().getResource("/res/teuton_adm.png")));
	      iconMap.put("settler", new ImageIcon(getClass().getResource("/res/settler.gif")));
	      
	      iconMap.put("leg", new ImageIcon(getClass().getResource("/res/leg.png")));
	      iconMap.put("pretorian", new ImageIcon(getClass().getResource("/res/pretorian.gif")));
	      iconMap.put("imperian", new ImageIcon(getClass().getResource("/res/imperian.gif")));
	      iconMap.put("EI", new ImageIcon(getClass().getResource("/res/EI.gif")));
	      iconMap.put("EC", new ImageIcon(getClass().getResource("/res/EC.gif")));
	      iconMap.put("roman_ram", new ImageIcon(getClass().getResource("/res/roman_ram.gif")));
	      iconMap.put("fire_catapult", new ImageIcon(getClass().getResource("/res/fire_catapult.gif")));
	      iconMap.put("roman_adm", new ImageIcon(getClass().getResource("/res/roman_adm.png")));
	      iconMap.put("roman_settler", new ImageIcon(getClass().getResource("/res/roman_settler.gif")));
	     	      
	      iconMap.put("phalanx", new ImageIcon(getClass().getResource("/res/phalanx.png")));
	      iconMap.put("swordsman", new ImageIcon(getClass().getResource("/res/swordsman.gif")));
	      iconMap.put("theutates_thunder", new ImageIcon(getClass().getResource("/res/theutates_thunder.gif")));
	      iconMap.put("druidrider", new ImageIcon(getClass().getResource("/res/druidrider.gif")));
	      iconMap.put("haeduan", new ImageIcon(getClass().getResource("/res/haeduan.gif")));
	      iconMap.put("gaul_ram", new ImageIcon(getClass().getResource("/res/gaul_ram.gif")));
	      iconMap.put("trebuchet", new ImageIcon(getClass().getResource("/res/trebuchet.gif")));
	      iconMap.put("gaul_adm", new ImageIcon(getClass().getResource("/res/gaul_adm.png")));
	      iconMap.put("gaul_settler", new ImageIcon(getClass().getResource("/res/gaul_settler.gif")));
	  }

	  @Override
	  public Component getListCellRendererComponent(JList<?> list, Object value, int index,
	                                                boolean isSelected, boolean cellHasFocus) {
	      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	      
	      this.setText(value.toString());
	      
	      switch(value.toString()) {
	    //TEUTON      
	      case "clubswinger": 
	    	  this.setIcon(iconMap.get("clubswinger"));
	    	  break;
	      case "spearman": 
	    	  this.setIcon(iconMap.get("spearman"));
	    	  break;
	      case "axeman": 
	    	  this.setIcon(iconMap.get("axeman"));
	    	  break;
	      case "paladin": 
	    	  this.setIcon(iconMap.get("paladin"));
	    	  break;
	      case "teutonic knight": 
	    	  this.setIcon(iconMap.get("teutonic_knight"));
	    	  break;
	      case "teutonic ram": 
	    	  this.setIcon(iconMap.get("ram"));
	    	  break;
	      case "catapult": 
	    	  this.setIcon(iconMap.get("catapult"));
	    	  break;
	      case "chief": 
	    	  this.setIcon(iconMap.get("administrator"));
	    	  break;
	      case "teutonic settler": 
	    	  this.setIcon(iconMap.get("settler"));
	    	  break;
	    //ROMAN  
	      case "legionnaire": 
	    	  this.setIcon(iconMap.get("leg"));
	    	  break;
	      case "pretorian": 
	    	  this.setIcon(iconMap.get("pretorian"));
	    	  break;
	      case "imperian": 
	    	  this.setIcon(iconMap.get("imperian"));
	    	  break;
	      case "EI": 
	    	  this.setIcon(iconMap.get("EI"));
	    	  break;
	      case "EC": 
	    	  this.setIcon(iconMap.get("teutonic_knight"));
	    	  break;
	      case "roman ram": 
	    	  this.setIcon(iconMap.get("roman_ram"));
	    	  break;
	      case "fire catapult": 
	    	  this.setIcon(iconMap.get("fire_catapult"));
	    	  break;
	      case "senator": 
	    	  this.setIcon(iconMap.get("roman_adm"));
	    	  break;
	      case "roman settler": 
	    	  this.setIcon(iconMap.get("roman_settler"));
	    	  break;
	    //GAUL
	      case "phalanx": 
	    	  this.setIcon(iconMap.get("leg"));
	    	  break;
	      case "swordsman": 
	    	  this.setIcon(iconMap.get("pretorian"));
	    	  break;
	      case "theutates thunder": 
	    	  this.setIcon(iconMap.get("imperian"));
	    	  break;
	      case "druidrider": 
	    	  this.setIcon(iconMap.get("EI"));
	    	  break;
	      case "haeduan": 
	    	  this.setIcon(iconMap.get("haeduan"));
	    	  break;
	      case "gaul ram": 
	    	  this.setIcon(iconMap.get("gaul_ram"));
	    	  break;
	      case "trebuchet": 
	    	  this.setIcon(iconMap.get("trebuchet"));
	    	  break;
	      case "cheiftain": 
	    	  this.setIcon(iconMap.get("gaul_adm"));
	    	  break;
	      case "gaul settler": 
	    	  this.setIcon(iconMap.get("gaul_settler"));
	    	  break;
	      }
	      
	      if (!isSelected) {
	          this.setBackground(index % 2 == 0 ? background : defaultBackground);
	      }
	      return this;
	  }
	}