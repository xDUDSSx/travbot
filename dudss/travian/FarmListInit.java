package dudss.travian;

import java.awt.EventQueue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
//import javax.swing.SwingUtilities;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class FarmListInit extends Travian{
	
	static FileWriter logfw;
	static BufferedWriter logbw;
	
	public static boolean farmlistinit() throws InterruptedException, IOException {
		System.out.println("\n** Farmlist initialization **");
 		if(LoginSuccessful == true) { 
 			
 			System.out.println("clearing table data ...");
 			while(BotFrame.model.getRowCount() > 0)
 			{
 			    BotFrame.model.removeRow(0);
 			}
 			
 			//Loads the farmlist page
 			driver.get(travian_url_farm);  
 			
			File logfile = new File(BotFrame.workingDirectoryPath + "/log.txt");
			logfile.delete();
			logfile.createNewFile();
			
			Thread.sleep(500);
			System.out.println(Travian.driver.getCurrentUrl());
			
			boolean farmlistBtnPresent = false;
			
			//Checking the farmlist button
			try {				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='build.php?tt=99&id=39']")));
				farmlistBtnPresent = true;
			} catch (NoSuchElementException | TimeoutException e) {
				System.out.println("The current village does not have a rally point built or the user has no gold club!");
				farmlistBtnPresent = false;
			}
			
			if (farmlistBtnPresent) {
				//Getting the farmlist entry container
				Boolean elementfound = webElementRetryCycleWait("//*[@id='raidList']", 10, 1000);
				if (elementfound == false) {
					System.out.print("No farmlist was found! Have you created any farmlists? Do you have gold club?");
					EventQueue.invokeLater(new Runnable() {
			    	    @Override
			    	    public void run() {
			    	    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
								    "No farmlists were found! Have you created any farmlists? Do you have gold club?",
								    "No farmlists found",
								    JOptionPane.WARNING_MESSAGE);
			    	    }
			    	  });
					BotFrame.loggingin = false;
					BotFrame.loginbtn.setIcon(null);
				}
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='raidList']")));
	 			WebElement defaultraidlist = driver.findElement(By.xpath("//*[@id='raidList']"));  
	 			
	 			//Creates a list of all present farmlists
	 			List<WebElement> farmlists = defaultraidlist.findElements(By.xpath("//div[@class='listEntry']")); 
	 			ArrayList<String> farmlists_names = new ArrayList<String>();   
	 			ArrayList<String> farmlists_id = new ArrayList<String>();
	 				
	 			//Creates an ArrayList that holds farmlistsender class instances
	 			fllist = new ArrayList<FarmListSender>();	 
	 			
	 			//For every farmlist, a new farmlistsender instance is created and saved into fllist
	 			//Also farmlist ids and names are saved (Their correlation is based on their indexes (eg. farmlist_id.get(4) is the id of farmlist_names.get(4) farmlist))
	 			for (int i = 0; i < farmlists.size(); i++) {               
	 				WebElement a = driver.findElement(By.xpath("//*[@id='" + farmlists.get(i).getAttribute("id").toString() + "']/form/div[1]/div[2]"));
	 				farmlists_id.add(farmlists.get(i).getAttribute("id").toString());
	 				farmlists_names.add(a.getText()); 
	 				
	 				n_of_flsenders++;
	 				fllist.add(new FarmListSender());
	 				
	 				//new row in the farmlistsender table is created
	 				BotFrame.model.addRow(new Object[] {false,farmlists_names.get(i), null, null, null, null, "Set profile"});
	 			}
	 			
	 			//farmlistinitclass local lists are passed to global ones (is this dumb? I dont even know)
	 			Travian.farmlists2 = farmlists;
	 			Travian.farmlists_names2 = farmlists_names;
	 			Travian.farmlists_id2 = farmlists_id;
	 			
	 			//Log file is created
	 			try {
		 			logfw = new FileWriter(logfile, true);
					logbw = new BufferedWriter(logfw);
					PrintWriter out = new PrintWriter(logbw);
					
					for(int i = 0; i< farmlists.size(); i++) {
						out.println(farmlists_names.get(i) + " == " + farmlists_id.get(i) + "\n");
					}
					
					out.println("");
					out.println("-------------------------------");
					out.println("");
					
					out.close();
					logbw.close();
					logfw.close();
				
	 			} catch (Exception e) {System.err.println("LogFile init failed"); e.printStackTrace();}
				
	 			System.out.println(farmlists_names);
	 			System.out.println(farmlists_id);
	 		
	 			System.out.println("-- Initialization complete --\n");
	 			return true;
			} else {
				Travian.farmlists2 = new ArrayList<WebElement>();
	 			Travian.farmlists_names2 = new ArrayList<String>();
	 			Travian.farmlists_id2 = new ArrayList<String>();
				
				EventQueue.invokeLater(new Runnable() {
		    	    @Override
		    	    public void run() {
		    	    	JOptionPane.showMessageDialog(BotFrame.MainGUI,
							    "Farmlist menu not found!\nDo you have GOLD CLUB? If you do the village currently selected on your account might not have a rally point built.\nSwitch to a village that has one or upgrade it to at least level 1."
							    + "",
							    "Farmlist menu not found (NO GOLDCLUB / RALLY POINT)",
							    JOptionPane.ERROR_MESSAGE);
		    	    }
		    	  });
				//BotFrame.loggingin = false;
				//BotFrame.loginbtn.setIcon(null);
				return false;
			}
 		} else {
 			System.out.print("You need to log in before initalising farmlists!");
 			return false;
 		}	
	}
	
	//A method that should wait for a WebElement to be present, there is a similar method in editfarm class that does something similar but 
	//uses  selenium's explicit wait to wait for visibility and clickability of the element
	public static boolean webElementRetryCycleWait(String xpath, int MAX_RETRY_COUNT, int delay) throws InterruptedException {
			int retryCount = 0;
			
			while(true)
			{
			    try
			    {
			    	WebElement isPresent = driver.findElement(By.xpath(xpath));
			    	return true;    
			    }
			    catch(NoSuchElementException e)
			    {
			    if(retryCount > 1) {
			    	try {	    	
				    	Thread.sleep(delay);
				    	
				    	if(retryCount > (MAX_RETRY_COUNT - 1)) {
				    		driver.get(travian_url_farm);
				    		try {
				    		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
				    		} catch (NoSuchElementException | TimeoutException ex) {
				    			e.printStackTrace();
				    		}
				    	}
				        if(retryCount > MAX_RETRY_COUNT)
				        {
				            throw new RuntimeException("element (xpath = \"" + xpath + "\") NOT found, retried " + retryCount + " times", e);
				        }
			    	} catch (RuntimeException ex) {
			    		System.out.println(e.getMessage());
			    		System.err.println("-- Element not found --");
			    		return false;
			    	}
			    }
			        System.out.println("element (xpath = \"" + xpath + "\") not found retry number " + retryCount);
			        retryCount++;
			        continue;
			   }
			}
		}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */