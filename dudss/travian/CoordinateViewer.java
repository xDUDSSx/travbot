package dudss.travian;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
//import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.JCheckBox;
//import javax.swing.JDialog;
import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.awt.event.WindowStateListener;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.JProgressBar;
import javax.swing.ImageIcon;
import net.miginfocom.swing.MigLayout;
//import java.awt.ScrollPane;
import java.awt.Toolkit;
import javax.swing.Box;
import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.UIManager;

class CoordviewerPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener {
	
   	static int[] coords; 
   	static String[] tempcoords;
    static ArrayList<String> tempxylist;
   	
    static ArrayList<String> villages;
    static ArrayList<String> names;
    static ArrayList<String> pop;
    
    static double zoomindex = 2;
    static int framex;
    static int framey;
    static int framecount = 0;
    static int FPS = 0;
    static ArrayList<Line2D> dotsV;
    static ArrayList<Line2D> dotsH;
    ArrayList<Line2D> lines;
    
    static Rectangle2D rect;
    static int selected_index = -1;
    static Boolean lines_rendered = false;
    static Boolean dots_rendered = false;
    static Boolean inDrag = false;
    
    static Point origin = new Point(CoordinateViewer.panelwidth/2, CoordinateViewer.panelwidth/2);
    static Point scrollorigin;
    static Point mousept;
    static int mouseX;
    static int mouseY;
    
    CoordviewerPanel() {
    	 addMouseMotionListener(this);
    	 addMouseWheelListener(this);
    	 addMouseListener(this);
    	 
    	 villages = new ArrayList<String>();
    	 names = new ArrayList<String>();
    	 pop = new ArrayList<String>();
    	 
    	 for(int i = 0; i < Getter.Villagelist.size(); i++) {
    		 villages.add(Getter.Villagelist.get(i));
    		 names.add(Getter.Namelist.get(i));
    		 pop.add(Getter.Populationlist.get(i));
    	 }
    	 
    	 tempxylist = new ArrayList<String>(Getter.XYlist);   	
    	 //tempxylist.remove(tempxylist.size() - 1);
    	 //tempxylist.remove(tempxylist.size() - 1);
    	 tempcoords = tempxylist.toArray(new String[tempxylist.size()]);
    	 coords = Arrays.asList(tempcoords).stream().mapToInt(Integer::parseInt).toArray();
    	 
    	 framex = Integer.parseInt(BotFrame.frameX);
    	 framey = Integer.parseInt(BotFrame.frameY);
    	 
    	 int dx = rtc(framex) - rtc(0);
         int dy = rtc(framey) - rtc(0);
         origin.setLocation(origin.x + dx, origin.y + dy);
    	 
    	 javax.swing.Timer timer = new Timer (1000/40, new ActionListener(){    		 
    		 @Override
    		 public void actionPerformed(ActionEvent e) {    		  		         
    		        framecount++;
    			 	repaint();
    		 }				   		    
    	 });
    	 
    	 timer.start();
    	 
    	 TimerTask updateFPS = new TimerTask() {
    		    public void run() {
    		    	FPS = framecount;
    		        framecount = 0;   		   
    		    }
    		};
    		
    	 java.util.Timer t = new java.util.Timer();
    	 t.scheduleAtFixedRate(updateFPS, 1000, 1000);      
    }
    
    private void drawFPS(int x, int y, int FPS, Graphics g) { 
        Graphics2D g2d = (Graphics2D) g;                  
        initG2Dhints(g2d);        
        g2d.setColor(Color.white);
        g2d.drawString("FPS: " + FPS, x, y);       
        CoordinateViewer.hidechckbox.setText("Hide viewer -- FPS: " + FPS); 
    }
    
    int randomWithRange(int min, int max)
    {
       int range = (max - min) + 1;     
       return (int)(Math.random() * range) + min;
    }
    
    private void drawArrayLines(Graphics g) {
		    Graphics2D g2d = (Graphics2D) g;
		    initG2Dhints(g2d);
	    	
	        AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f);
	               			
	        g2d.setComposite(alcom);
	        g2d.setColor(new Color(255,255,255));
	        
	        g2d.setStroke(new BasicStroke(2));	       
	        
	        lines = new ArrayList<Line2D>();
         
	        int n = 0;
	        for(int i = 0; i < coords.length; i += 2) {
	        	 Line2D line = new Line2D.Double(tc(framex * zoomindex), tc(-(framey * zoomindex)) , tc(coords[i] * zoomindex), tc(-(coords[i+1] * zoomindex)));
	        	 lines.add(line);
	        	 g2d.draw(lines.get(n));
	        	 n++;
	        }   	      	  
    }
    private void drawArrayDots(Graphics g) {
	    Graphics2D g2d = (Graphics2D) g;
	    initG2Dhints(g2d);
    	
        AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
               			
        g2d.setComposite(alcom);
        g2d.setColor(Color.red);
        g2d.setStroke(new BasicStroke(1));
        
        dotsV = new ArrayList<Line2D>();
        dotsH = new ArrayList<Line2D>();    
        
        int n = 0;
        for(int i = 0; i < coords.length; i += 2) {
        	 Line2D dotV = new Line2D.Double(tc(coords[i] * zoomindex) , tc(-(coords[i+1] * zoomindex)) - zoomindex, tc(coords[i] * zoomindex), tc(-(coords[i+1] * zoomindex)) + zoomindex);
        	 Line2D dotH = new Line2D.Double(tc(coords[i] * zoomindex) - zoomindex , tc(-(coords[i+1] * zoomindex)), tc(coords[i] * zoomindex) + zoomindex, tc(-(coords[i+1] * zoomindex)));
        	 dotsV.add(dotV);
        	 dotsH.add(dotH);
        	 g2d.draw(dotsV.get(n));
        	 g2d.draw(dotsH.get(n));
        	 n++;
        }
}	
    private void drawGrid(Graphics g) {
	    Graphics2D g2d = (Graphics2D) g;
	    initG2Dhints(g2d);
    	
        AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f);
               			
        g2d.setComposite(alcom);
        g2d.setColor(new Color(200,200,200));
        g2d.setStroke(new BasicStroke(3));	       
        
        //H
        g2d.draw(new Line2D.Double(tc(-250*zoomindex), tc(0*zoomindex ), tc(250 * zoomindex), tc(0*zoomindex)));
        //V
        g2d.draw(new Line2D.Double(tc(0*zoomindex), tc(250*zoomindex ), tc(0 * zoomindex), tc(-250*zoomindex)));
        g2d.draw(new Rectangle2D.Double(tc(-250*zoomindex), tc(-250*zoomindex),CoordinateViewer.panelwidth*zoomindex, CoordinateViewer.panelwidth*zoomindex));
        
}	
    private void initG2Dhints(Graphics2D g2d) {
    	 RenderingHints rh = new RenderingHints(
      		    RenderingHints.KEY_ANTIALIASING,
      		    RenderingHints.VALUE_ANTIALIAS_ON);
         rh.put(RenderingHints.KEY_RENDERING,
                  RenderingHints.VALUE_RENDER_QUALITY);
         g2d.setRenderingHints(rh);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        
        drawArrayLines(g);
        drawArrayDots(g); 
        drawFPS(10,15,FPS, g);
        drawGrid(g);
        
        if(selected_index != -1 && selected_index <= lines.size()) {
        	setSelected(selected_index, g);
        }
    }
    
    double tc(double x) {return CoordinateViewer.panelwidth/2 + (CoordinateViewer.panelwidth/2) + x;}
    int tc(int x) {return CoordinateViewer.panelwidth/2 + (CoordinateViewer.panelwidth/2) + x;}
    double rtc(double x) {return x - (CoordinateViewer.panelwidth/2) - CoordinateViewer.panelwidth/2;}
    int rtc(int x) {return x - (CoordinateViewer.panelwidth/2) - CoordinateViewer.panelwidth/2;}

	@Override
	public void mouseDragged(MouseEvent e) {
	/*	   int dx = e.getX() - mousept.x;
           int dy = e.getY() - mousept.y;
           origin.setLocation(origin.x + dx, origin.y + dy);
           mousept = e.getPoint();
    */
		
		if (scrollorigin != null) {
            JViewport viewPort = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, CoordinateViewer.viewerPanel);
            if (viewPort != null) {
                int deltaX = scrollorigin.x - e.getX();
                int deltaY = scrollorigin.y - e.getY();

                Rectangle view = viewPort.getViewRect();
                view.x += deltaX;
                view.y += deltaY;

                CoordinateViewer.viewerPanel.scrollRectToVisible(view);
            }
        }
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		int x,y;
		
		x = e.getX();
		y = e.getY();
		
		mouseX = x;
		mouseY = y;
		
		CoordinateViewer.lblrealX.setText("X: " + rtc(mouseX));
		CoordinateViewer.lblrealY.setText("Y: " + rtc(mouseY));
		
		rect = new Rectangle2D.Double(0,0,1,1);
		rect.setRect(x-4, y-4, 8, 8);
		
	for(int i = 0; i < lines.size(); i++)
		if(lines.get(i).intersects(rect)) {
			selected_index = i;
			break;
		} else {
			selected_index = -1;
		}
	
	}
	
	void setSelected(int i, Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		double x2 = rtc(lines.get(i).getX2());
		double y2 = rtc(lines.get(i).getY2());
		
		int realX = (int) Math.round(x2/zoomindex);
		int realY = -((int) Math.round(y2/zoomindex));	
		
		initG2Dhints(g2d);
		AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f);
		g2d.setComposite(alcom);
		
		g2d.setStroke(new BasicStroke(3));
		g2d.setColor(new Color(0, 255, 0));
     	
		Line2D line = new Line2D.Double(tc(framex*zoomindex), tc(-(framey * zoomindex)),tc(x2), tc(y2));
		g2d.draw(line);
		
		try {
			
			g2d.setColor(new Color(100,100,100));
			int textoffsetX = 13;
			
			int villagewidth = g.getFontMetrics().stringWidth(villages.get(i));
			int namewidth = g.getFontMetrics().stringWidth(names.get(i));
			int xywidth = g.getFontMetrics().stringWidth("x: 000 y: 000");
			int width = 0;
			int widthminimum = 80;
			
			if (villagewidth > namewidth && villagewidth > xywidth) {
			    width = villagewidth;
			}

			if (namewidth > villagewidth && namewidth > xywidth) {
			    width = namewidth;
			}    

			if (xywidth > namewidth && xywidth > villagewidth) {
			    width = xywidth;
			}
			
			if(width < widthminimum) {
				width = widthminimum;
			}
			
			RenderingHints rh = new RenderingHints(null);
			g2d.setRenderingHints(rh);
			
			Rectangle2D rect = new Rectangle2D.Double(mouseX + 10, mouseY - 50, (width+textoffsetX + 4 - 10), 50);		
			g2d.fill(rect);
			
			g2d.setColor(Color.white);
			
			g2d.setStroke(new BasicStroke(4,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER));
			g2d.drawLine(mouseX + (width+textoffsetX + 4), mouseY, mouseX + (width+textoffsetX + 4), mouseY - 50);
			
			g2d.drawString("x: " + realX +" y: " + realY, mouseX + textoffsetX, mouseY - 3f);
			g2d.drawString(villages.get(i), mouseX + textoffsetX , mouseY - 37f);
			g2d.drawString(names.get(i), mouseX + textoffsetX , mouseY - 26f);
			g2d.drawString("pop: " + pop.get(i), mouseX + textoffsetX , mouseY - 15f);
			
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Data missing (" + realX + "|" + realY + ") ...");
		}		
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
	       int notches = e.getWheelRotation();
	       if (notches < 0) {
	           //UP
	    	   zoomindex+= 0.08;
	       } else {
	    	   //DOWN
	    	   zoomindex-= 0.08;
	       }
	       if(zoomindex <= 0.5) {
	    	   zoomindex = 0.5;
	       }
	       if(zoomindex >= 8) {
	    	   zoomindex = 8;
	       }
	       //System.out.println(zoomindex);	       
	       //System.out.println("MouseWheelListener:" + notches);
	    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		int i = selected_index;
		if(i != -1) {
			double x2 = rtc(lines.get(i).getX2());
			double y2 = rtc(lines.get(i).getY2());
			
			int realX = (int) Math.round(x2/zoomindex);
			int realY = -((int) Math.round(y2/zoomindex));	
			
			URL mapurl = null;
			
			try {
				mapurl = new URL("https://" + BotFrame.frameServer + "/position_details.php?x=" + realX + "&y=" + realY);
			} catch (MalformedURLException e) {			
				e.printStackTrace();
			}
			openWebpage(mapurl);
			
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {	
		//int x = arg0.getX();
		//int y = arg0.getY();
		//inDrag = true;
		//mousept = arg0.getPoint();
		scrollorigin = new Point(arg0.getPoint());

	}

	public static boolean openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	            return true;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    return false;
	}

	public static boolean openWebpage(URL url) {
	    try {
	        return openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	    return false;
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
    ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(String stringValue : stringArray) {
            try {          
                result.add(Integer.parseInt(stringValue));
            } catch(NumberFormatException nfe) {
               System.out.println("Could not parse " + nfe);
            } 
        }       
        return result;
    }
}

public class CoordinateViewer extends JFrame implements TableModelListener {

	private static final long serialVersionUID = 1L;
	
	static JPanel viewerPanel;
	static JTable table;
	static DefaultTableModel model;
	private static DefaultTableModel model_loader;
	static JScrollPane viewer_scrollpane ;
	static int panelwidth = 500;
	static JProgressBar progressBar; 
	
	static JLabel lblCurrentnumber;
	static JLabel lblPercentage;
	static JLabel lblInfo;
	
	static JScrollPane scrollPane_1;
	public static JTable table_loader;
	static JButton btnStartFarmadder;
	static JButton btnPause;
	static JCheckBox hidechckbox;
	private JButton btnCloseFarmadder;
	private JLabel lblNewLabel;
	static JLabel lblrealX;
	static JLabel lblrealY;
	private Component horizontalStrut;
	private JPanel panel_2;
	private JLabel lblFrom;
	private JTextField popselectfromfield;
	private JLabel lblTo;
	private JTextField popselecttofield;
	private JButton btnPopSelect;
	private JLabel lblPopulation;
	private JCheckBox tableloadercheckall;
	private JTextArea txtrapparentlyAddingFarms;
	private JButton btnRetryAll;
	private JButton btnDeselectFarmsWith;
	
    public CoordinateViewer()  {
    	setIconImage(Toolkit.getDefaultToolkit().getImage(CoordinateViewer.class.getResource("/res/travicon.png")));
    	setMinimumSize(new Dimension(800, 600));
        initUI();
    }
    
    private void initUI() {
    	setVisible(true);
        setTitle("FarmAdder - Farm viewer");
        setSize(994, 770);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);   
        
        JPanel panel = new JPanel();
        setContentPane(panel);
        panel.setLayout(new MigLayout("", "[50%,grow][50%]", "[60%,grow][][40%,grow]"));
        
        scrollPane_1 = new JScrollPane();
        panel.add(scrollPane_1, "cell 0 0,grow");
        
        table_loader = new JTable() {
        	private static final long serialVersionUID = 1L;
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0)
                    return Boolean.class;
                return super.getColumnClass(columnIndex);
            }
            
            boolean[] columnEditables = new boolean[] {
    				true, false, false, false, false, false
            };
            
            @Override
            public boolean isCellEditable(int row, int column) {
    				return columnEditables[column];
            }
           
            @Override   
            public Component prepareRenderer(
                    TableCellRenderer renderer, int row, int column)
                {
                    Component c = super.prepareRenderer(renderer, row, column);
                           
                    if((Boolean)table_loader.getValueAt(row, 0) == true)
      	          {
      	        	  c.setForeground(Color.black);        
      	              c.setBackground(new Color(196, 255, 196));  
      	          }         	          
                    
      	          else if((Boolean)table_loader.getValueAt(row, 0) == false)
    	          {    
    	              c.setBackground(new Color(255,236,236));    
    	              c.setForeground(Color.black);   	              
    	          }
                    
                  return c;
                }    
        };
        table_loader.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        setModel_loader(new DefaultTableModel(
            	new Object[][] {
            	},
            	new String[] {
            		"", "X", "Y", "Village", "Name", "Population", "Alliance"
            	}
            ));   
        
        table_loader.setModel(getModel_loader());
        table_loader.getModel().addTableModelListener(this);
        table_loader.getTableHeader().setReorderingAllowed(false); 
        table_loader.setAutoCreateRowSorter(true);
     
        scrollPane_1.setViewportView(table_loader);

        viewer_scrollpane = new JScrollPane();
        viewer_scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        viewer_scrollpane.setViewportBorder(new LineBorder(Color.ORANGE));
        viewer_scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        panel.add(viewer_scrollpane, "cell 1 0,alignx right,aligny top");
        
        viewerPanel = new CoordviewerPanel();
        viewerPanel.setAutoscrolls(true);
        viewer_scrollpane.setViewportView(viewerPanel);
        
        viewerPanel.setPreferredSize(new Dimension(1500, 1500));   
        viewerPanel.setBackground(Color.black);
       
        
        //Sets viewport of scrollpane to center of viewer panel
        //viewer_scrollpane.getViewport().setViewPosition(new java.awt.Point(panelwidth/2, panelwidth/2));
        
        //Sets the center of viewport to inputted X,Y getter coordinates
        int cx = Integer.valueOf(BotFrame.frameX);
        int cy = Integer.valueOf(BotFrame.frameY);
        viewer_scrollpane.getViewport().setViewPosition(new Point( tc(cx) - (panelwidth/2), tc(-cy) - (panelwidth/2)));
        
        revalidate();
        viewer_scrollpane.revalidate();
        viewer_scrollpane.repaint();
		
		lblNewLabel = new JLabel("Added farms - ");
		
		panel.add(lblNewLabel, "flowx,cell 0 1");
		ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
              AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
              boolean selected = abstractButton.getModel().isSelected();
              
              if (selected == true) {
            	  viewer_scrollpane.setVisible(false);          	  
              } else {
            	  viewer_scrollpane.setVisible(true);    
              }
            }
          };
	
		hidechckbox = new JCheckBox("Hide viewer -- FPS:\r\n");
		panel.add(hidechckbox, "flowx,cell 1 1");
		 
        hidechckbox.addActionListener(actionListener);   
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, "cell 0 2,grow");
        
        table = new JTable() {

		boolean[] columnEditables = new boolean[] {
       		   		false, false, false, false, false, true
               };
               
               @Override
               public boolean isCellEditable(int row, int column) {
       				return columnEditables[column];
               }
              
               @Override   
               public Component prepareRenderer(
            	   TableCellRenderer renderer, int row, int column)
                   {
            	   Component c = super.prepareRenderer(renderer, row, column);                             
            	   		if(table_loader.getValueAt(row, 5).equals("ERROR")) 
            	   		{
                    	  c.setForeground(Color.black);     
                    	  c.setBackground(new Color(255,236,236));          
            	   		}         	                  
            	   		return c;
                   }                  
        };

        model = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {"Coords", "Village", "Player", "Pop", "Status", "Action"}
			);
        table.setModel(model);
        table.getColumnModel().getColumn(0).setPreferredWidth(77);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(150);
        table.getColumnModel().getColumn(3).setPreferredWidth(67);
        table.getColumnModel().getColumn(5).setPreferredWidth(100);
        
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        table.getTableHeader().setReorderingAllowed(false); 
        
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );	
        	
		Action retry = new AbstractAction()
	        {
	            public void actionPerformed(ActionEvent e)
	            {
	            	if(FarmListEdit.workerfinished == true) {
		            	JTable table = (JTable)e.getSource();
		                int modelRow = Integer.valueOf( e.getActionCommand() );
		                
		                if(model.getValueAt(modelRow, 4).equals("ERROR")) {
			                System.out.println("Starting retry FLAdder at row: " + modelRow);
			                
			                ArrayList<String> tempXYlist = new ArrayList<String>();
			        		List<String> tempVillagelist = new ArrayList<String>();
			        		List<String> tempNamelist = new ArrayList<String>();
			        		List<String> tempPopulationlist = new ArrayList<String>();
			        		
			        		System.out.println("Parsing farm data ...");
			        		
		    				String[] strs = model.getValueAt(modelRow, 0).toString().split(":");
		    				tempXYlist.add(strs[0]);
		    				tempXYlist.add(strs[1]);
		    				tempVillagelist.add(model.getValueAt(modelRow, 1).toString());
		    				tempNamelist.add(model.getValueAt(modelRow, 2).toString());
		    				tempPopulationlist.add(model.getValueAt(modelRow, 3).toString());
			        			
			 
			        		System.out.println("Gettings troop id ...");
			        		String t_id = FarmListEdit.resolveTroopId();
			        		System.out.println("-- Starting FarmAdder --");
			        		FarmListEdit.workerfinished = false;
			        		FarmListEdit.executeWorker(t_id, tempXYlist, tempVillagelist, tempNamelist, tempPopulationlist);
			        		
			        		model.removeRow(modelRow);
		                } else {
		                	System.out.println("Cant retry farm with OK status!");
		                }
	            	} else {
	            		System.out.println("FarmAdder is running already!");
	            	}
	            }
	        };
	         
	    ButtonColumn buttonColumn = new ButtonColumn(table, retry, 5);
		
        scrollPane.setViewportView(table);
        
        JPanel panel_1 = new JPanel();
        panel.add(panel_1, "cell 1 2,growx,aligny top");
        panel_1.setLayout(new MigLayout("", "[grow][][][][grow]", "[][][][grow][][][][grow]"));
        
        lblPercentage = new JLabel("percentage");
        panel_1.add(lblPercentage, "cell 0 0");
        
        lblCurrentnumber = new JLabel("currentnumber");
        panel_1.add(lblCurrentnumber, "cell 1 0");
        
        progressBar = new JProgressBar();
        progressBar.setMaximumSize(new Dimension(32767, 14));
        panel_1.add(progressBar, "cell 0 1 5 1,growx");
        progressBar.setPreferredSize(new Dimension(500,25));
        
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
            	if(FarmListEdit.workerfinished == false) {
        			System.out.println("-- Stopping FLAdder --");
        			FarmListEdit.stopAdder = true;
        		}
        		setVisible(false);
            	FarmListEdit.running = false;
            }
        });
        
        lblInfo = new JLabel("InfoLabel");
        panel_1.add(lblInfo, "cell 0 2");
        
        btnStartFarmadder = new JButton("Start FarmAdder");
        btnStartFarmadder.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(FarmListEdit.workerfinished == true) {
	        		ArrayList<String> tempXYlist = new ArrayList<String>();
	        		List<String> tempVillagelist = new ArrayList<String>();
	        		List<String> tempNamelist = new ArrayList<String>();
	        		List<String> tempPopulationlist = new ArrayList<String>();
	        		
	        		System.out.println("Parsing selected farm data ...");
	        		for (int i = 0; i < model_loader.getRowCount(); i++) {
	        			Object value = model_loader.getValueAt(i, 0);
	        			Boolean bool = (Boolean) value;
	        			if(bool == true) {
	        				tempXYlist.add(model_loader.getValueAt(i, 1).toString());
	        				tempXYlist.add(model_loader.getValueAt(i, 2).toString());
	        				tempVillagelist.add(model_loader.getValueAt(i, 3).toString());
	        				tempNamelist.add(model_loader.getValueAt(i, 4).toString());
	        				tempPopulationlist.add(model_loader.getValueAt(i, 5).toString());
	        			}
	        		}
	        		
	        		System.out.println("Gettings troop id ...");
	        		String t_id = FarmListEdit.resolveTroopId();
	        		System.out.println("-- Starting FarmAdder --");
	        		FarmListEdit.workerfinished = false;
	        		FarmListEdit.executeWorker(t_id, tempXYlist, tempVillagelist, tempNamelist, tempPopulationlist);
        		} else {
        			System.out.println("FarmAdder is running already!");
        		}
        	}
        });
        
        panel_2 = new JPanel();
        panel_2.setBorder(new TitledBorder(new LineBorder(new Color(255, 153, 51), 1, true), "Select filters", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        panel_1.add(panel_2, "cell 0 3 5 3,growx,aligny top");
        panel_2.setLayout(new MigLayout("", "[grow]", "[][]"));
        
        lblPopulation = new JLabel("Population:");
        panel_2.add(lblPopulation, "flowx,cell 0 0,alignx center");
        
        lblFrom = new JLabel("From:");
        panel_2.add(lblFrom, "cell 0 0,alignx center");
        
        popselectfromfield = new JTextField();
        panel_2.add(popselectfromfield, "cell 0 0,alignx center");
        popselectfromfield.setColumns(10);
        
        lblTo = new JLabel("To:");
        panel_2.add(lblTo, "cell 0 0,alignx center");
        
        popselecttofield = new JTextField();
        panel_2.add(popselecttofield, "cell 0 0,alignx center");
        popselecttofield.setColumns(10);
        
        btnPopSelect = new JButton("Select");
        btnPopSelect.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		String from = popselectfromfield.getText();
        		String to = popselecttofield.getText();
        		
        		if (from == null || from.isEmpty() || to == null || to.isEmpty()) {
        			System.out.println("PopSelect no range specified!");
        		} else {
        			try {
	        		for(int i = 0; i < model_loader.getRowCount(); i++) {
	        			int value = Integer.valueOf(String.valueOf(model_loader.getValueAt(i, 5)));
	        			int fromint = Integer.valueOf(from);
	        			int toint = Integer.valueOf(to);
	        			
	        			if (value >= fromint && value <= toint) {
	        				model_loader.setValueAt(true, i, 0);
	        			} 
	        		}
        			} catch (Exception e) {
        				System.out.println("PopSelect error - " + e.getMessage());
        			}
        		}
        	}
        });
        panel_2.add(btnPopSelect, "cell 0 0");
        
        btnDeselectFarmsWith = new JButton("Deselect farms with an alliance");
        btnDeselectFarmsWith.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        			try {
	        		for(int i = 0; i < model_loader.getRowCount(); i++) {
	        			String value = String.valueOf(model_loader.getValueAt(i, 6));
	        			
	        			if (!value.equals("")) {
	        				if((Boolean) model_loader.getValueAt(i, 0) == true) {
	        					model_loader.setValueAt(false, i, 0);
	        				}
	        			}
	        		}
        			} catch (Exception e) {
        				System.out.println("Alliance deselect error - " + e.getMessage());
        			}
        	}
        });
        panel_2.add(btnDeselectFarmsWith, "cell 0 1,alignx center");
        btnStartFarmadder.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/plus3.png")));
        panel_1.add(btnStartFarmadder, "flowx,cell 0 6");
        
        JButton btnStopAdder = new JButton("Stop");
        panel_1.add(btnStopAdder, "cell 1 6");
        btnStopAdder.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/stopicon16.png")));
        
        btnPause = new JButton("Pause");
        btnPause.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(FarmListEdit.pauseAdder == false) {
        		System.out.println("-- Pausing FLAdder --");
        			FarmListEdit.pauseAdder = true;
        			btnPause.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/play16.png")));
        			btnPause.setText("Resume");
        		} else {
        			System.out.println("-- Resuming FLAdder --");
        			FarmListEdit.pauseAdder = false;
        			btnPause.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/pause16.png")));
        			btnPause.setText("Pause");
        		}
        	}
        });
        btnPause.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/pause16.png")));
        panel_1.add(btnPause, "cell 2 6");
        
        btnCloseFarmadder = new JButton("Close FarmAdder");
        btnCloseFarmadder.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {	
        		if(FarmListEdit.workerfinished == false) {
        			System.out.println("-- Stopping FLAdder --");
        			FarmListEdit.stopAdder = true;
        		}
        		setVisible(false);
            	FarmListEdit.running = false;
        	}
        });
        panel_1.add(btnCloseFarmadder, "cell 3 6");
        
        txtrapparentlyAddingFarms = new JTextArea();
        txtrapparentlyAddingFarms.setBackground(UIManager.getColor("Button.background"));
        txtrapparentlyAddingFarms.setFont(new Font("Tahoma", Font.PLAIN, 11));
        txtrapparentlyAddingFarms.setLineWrap(true);
        txtrapparentlyAddingFarms.setText("How to use:\r\nFrom the table in the top left corner select farms you want to add or check \"Select all\".\r\nThen click Start FarmAdder, you can pause or stop it with corresponding buttons.\r\n\r\nWatch the farm added status, if its \"ERROR\" everytime, pause the FarmAdder and either tweak the delays or the global timeout (Settings>FarmAdder or Settings>General) !!");
        panel_1.add(txtrapparentlyAddingFarms, "cell 0 7 5 1,grow");
        btnStopAdder.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		//TODO: finish up, perhaps minimize into slot in main frame. For now -> quit adder and close coordviewer
        		System.out.println("-- Stopping FLAdder/CV --");
        		FarmListEdit.stopAdder = true;
        	}
        });
        
        horizontalStrut = Box.createHorizontalStrut(30);
        panel.add(horizontalStrut, "cell 1 1");
        
        lblrealX = new JLabel("X: 0");
        panel.add(lblrealX, "cell 1 1");
        
        lblrealY = new JLabel("Y: 0");
        panel.add(lblrealY, "cell 1 1,alignx left");
        
        tableloadercheckall = new JCheckBox("Select all");
        tableloadercheckall.setSelected(true);
        tableloadercheckall.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if (tableloadercheckall.isSelected() == true) {
        			try {
    	        		for(int i = 0; i < model_loader.getRowCount(); i++) {
    	        			Boolean bool = (boolean) model_loader.getValueAt(i, 0);
    	        			if (bool == false) {
    	        				model_loader.setValueAt(true, i, 0);
    	        			} 
    	        		}
            			} catch (Exception e) {
            				System.out.println("SelectAll error - " + e.getMessage());
            			}
        		} else {
        			try {
    	        		for(int i = 0; i < model_loader.getRowCount(); i++) {
    	        			Boolean bool = (boolean) model_loader.getValueAt(i, 0);
    	        			if (bool == true) {
    	        				model_loader.setValueAt(false, i, 0);
    	        			} 
    	        		}
            			} catch (Exception e) {
            				System.out.println("SelectAll error - " + e.getMessage());
            			}
        		}
        	}
        });
        panel.add(tableloadercheckall, "cell 0 1");
        
        btnRetryAll = new JButton("Retry all");
        btnRetryAll.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(FarmListEdit.workerfinished == true) {
	                ArrayList<String> tempXYlist = new ArrayList<String>();
	        		List<String> tempVillagelist = new ArrayList<String>();
	        		List<String> tempNamelist = new ArrayList<String>();
	        		List<String> tempPopulationlist = new ArrayList<String>();
	        		
	        		System.out.println("Parsing farm data ...");
	        		
	        		int numberOfRetries = 0;
	        		
  					for(int i = 0; i < model.getRowCount(); i++) {
  						if(model.getValueAt(i, 4).equals("ERROR")) {
  							String[] strs = model.getValueAt(i, 0).toString().split(":");
  			        		tempXYlist.add(strs[0]);
  			        		tempXYlist.add(strs[1]);
  			        		tempVillagelist.add(model.getValueAt(i, 1).toString());
  		  					tempNamelist.add(model.getValueAt(i, 2).toString());
  		  					tempPopulationlist.add(model.getValueAt(i, 3).toString());
  		  					
  		  					model.removeRow(i);
  						}
  						numberOfRetries = i;
  	        		}
  					
  					System.out.println("Retrying" + numberOfRetries + " farms ...");
  					
	        		System.out.println("Gettings troop id ...");
	        		String t_id = FarmListEdit.resolveTroopId();
	        		System.out.println("-- Starting FarmAdder --");
	        		FarmListEdit.workerfinished = false;
	        		FarmListEdit.executeWorker(t_id, tempXYlist, tempVillagelist, tempNamelist, tempPopulationlist);    
        		} else {
        			System.out.println("FarmAdder is already running!");
        		}
        	}
        });
        panel.add(btnRetryAll, "cell 0 1");
    }  	  
    
    int tc(int x) {return panelwidth/2 + (panelwidth/2) + x;}

	public static DefaultTableModel getModel_loader() {
		return model_loader;
	}

	public static void setModel_loader(DefaultTableModel model_loader) {
		CoordinateViewer.model_loader = model_loader;
	}
	
	public void tableChanged(TableModelEvent e) {
        int row = e.getFirstRow();
        TableModel model = (TableModel)e.getSource();
       
        if (e.getType() == TableModelEvent.UPDATE)
        {
            int column = e.getColumn();
            
            if (column == 0)
            {        
            	//can do something on select
            }   
            
        }            
        table.repaint();
        table_loader.repaint();
	}
	
}

/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */