package dudss.travian;

import java.awt.BorderLayout;
import java.awt.CardLayout;
//import java.awt.Color;
import java.awt.Dimension;
//import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
//import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
//import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
//import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.prompt.PromptSupport;

import net.miginfocom.swing.MigLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Component;

import javax.swing.border.EtchedBorder;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import dudss.travian.autoevade.EvadeVillageEntry;

import com.jgoodies.forms.layout.FormSpecs;
import java.awt.Window.Type;
//import javax.swing.BoxLayout;
//import java.awt.GridLayout;

public class Settings extends JFrame {

	private JList<Object> list = new JList<Object>();
	private CardLayout card = new CardLayout();
	public static JTextField serverfield;
	public static JTextArea useragentfield;
	public static JCheckBox httpcheckbox;
	public static JCheckBox logincheck;
	public static JCheckBox chckbxUseUseragent;
	public static JCheckBox proxycheckbox;
	public static JCheckBox loggingcheckbox;
	public static JCheckBox chckbxOverrideDir;
	public static JCheckBox headlessCheck;
	
	public static JCheckBox attack_lost_box;
	public static JCheckBox attack_win_loss_box;
	public static JTextField FARMADDER_INIT_DELAY_FIELD;
	public static JTextField FARMADDER_EXPAND_DELAY_FIELD;
	public static JTextField FARMADDER_ADD_DELAY_FIELD;
	public static JTextField proxyfield;
	public static JTextField FARMLISTSENDER_INIT_DELAY_FIELD;
	public static JTextField FARMLISTSENDER_EXPAND_DELAY_FIELD;
	public static JTextField FARMLISTSENDER_CHECK_DELAY_FIELD;
	public static JTextField FARMLISTSENDER_SEND_DELAY_FIELD;
	public static JTextField timeoutfield;
	public static JTextField dirTextField;
	
	public static JComboBox<String> villageComboBox;
	public static JTextField cropBelow;
	public static JTextField cropRate;
	public static JTextField cropAbove;
	public static JRadioButton rdbtnOverflow;
	public static JRadioButton rdbtnUnderflow;
	
	public static JTextField fieldWood;
	public static JTextField fieldClay;
	public static JTextField fieldIron;
	public static JTextField fieldCrop;
	
	public static JPanel autoEvadePanel;
	public static JScrollPane autoEvadeScrollPane;
	public static JTextField autoEvadeScanInterval;
	public static JTextField autoEvadeDelay;
	
	public static void setUIFont (javax.swing.plaf.FontUIResource f){
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get (key);
			if (value instanceof javax.swing.plaf.FontUIResource)
			UIManager.put (key, f);
		}
	} 
	
	public Settings() {
		setType(Type.UTILITY);
		setTitle("TravBot settings");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Settings.class.getResource("/res/travicon.png")));
		//setPreferredSize(new Dimension(600, 350));
		setMinimumSize(new Dimension(350,350));
		setAlwaysOnTop(false);
		
		setUIFont(new javax.swing.plaf.FontUIResource("Helvetica",Font.PLAIN, 12));
		
		addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                BotFrame.settings_btnstate = !BotFrame.settings_btnstate;
            }
        });
		
		Object[] items = new Object[] {
        		"General", "Farmlist sender", "Farm adder", "Crop checker", "Auto evader", "Proxy", "Dev"
        };
        getContentPane().setLayout(new MigLayout("ins 0, wrap 1", "[grow]", "[grow]"));
        
        JPanel panel_3 = new JPanel();
        getContentPane().add(panel_3, "cell 0 0,grow");
        panel_3.setLayout(new MigLayout("ins 0, wrap 1", "[][grow]", "[]"));
        
        JPanel panel = new JPanel();
        panel_3.add(panel, "cell 1 0,grow");
        panel.setLayout(card);

	    JPanel general = new JPanel();
	    general.setBackground(SystemColor.menu);
	    general.setBorder(new EmptyBorder(5, 5, 5, 5));
	    panel.add(general, "General");
	    
	    JPanel dev = new JPanel();
	    dev.setBorder(new EmptyBorder(5, 5, 5, 5));
	    dev.setBackground(SystemColor.menu);
	    panel.add(dev, "Dev");
	    dev.setLayout(new MigLayout("", "[]", "[][]"));
	    
	    JPanel farmadder  = new JPanel();
	    farmadder.setBackground(SystemColor.menu);
	    farmadder.setBorder(new EmptyBorder(5, 5, 5, 5));
	    panel.add(farmadder, "Farm adder");
	    farmadder.setLayout(new MigLayout("", "[][][][][grow]", "[][][][][][][][][][grow]"));
	    
	    JPanel farmlistsender = new JPanel();
	    farmlistsender.setBackground(SystemColor.menu);
	    panel.add(farmlistsender, "Farmlist sender");
	    farmlistsender.setLayout(new MigLayout("", "[][][]\r\n", "[][][][][][grow][]"));
      
        JPanel proxy = new JPanel();
        proxy.setBackground(SystemColor.menu);
        proxy.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.add(proxy, "Proxy");
        proxy.setLayout(new MigLayout("", "[grow]", "[][][][]"));
        
        proxycheckbox = new JCheckBox("Use proxy");
        proxycheckbox.setBackground(SystemColor.menu);
        proxycheckbox.setOpaque(false);
        proxy.add(proxycheckbox, "cell 0 0");
        
        JLabel lblProxyServer = new JLabel("Proxy server ( ip : port)");
        lblProxyServer.setBackground(SystemColor.menu);
        proxy.add(lblProxyServer, "cell 0 1");
        
        proxyfield = new JTextField();
        proxyfield.setBackground(SystemColor.window);
        proxy.add(proxyfield, "cell 0 2,growx");
        proxyfield.setColumns(10);
        
        JLabel lblProxiesThatRequire = new JLabel("Proxies that require authentication will probably not work.");
        lblProxiesThatRequire.setBackground(SystemColor.menu);
        proxy.add(lblProxiesThatRequire, "cell 0 3");
                    
       JLabel lblNewLabel_3 = new JLabel("FARMADDER_INIT_DELAY:\r\n");
       lblNewLabel_3.setBackground(SystemColor.menu);
       farmadder.add(lblNewLabel_3, "cell 0 1,alignx left");
       
       JLabel lblNewLabel_5 = new JLabel("Farm adder delays - if farm adder reports -element not found- errors you can edit delays here");
       lblNewLabel_5.setBackground(SystemColor.menu);
       farmadder.add(lblNewLabel_5, "cell 0 0 4 1");
           
        FARMADDER_INIT_DELAY_FIELD = new JTextField();
        FARMADDER_INIT_DELAY_FIELD.setText("400");
        farmadder.add(FARMADDER_INIT_DELAY_FIELD, "cell 1 1");
        FARMADDER_INIT_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs = new JLabel("ms");
        farmadder.add(lblMs, "cell 1 1");
        
        JLabel lblFarmadderexpanddelay = new JLabel("FARMADDER_EXPAND_DELAY:");
        lblFarmadderexpanddelay.setBackground(SystemColor.menu);
        farmadder.add(lblFarmadderexpanddelay, "flowx,cell 0 2,alignx left");
        
        JLabel lblFarmadderadddelay = new JLabel("FARMADDER_ADD_DELAY:");
        lblFarmadderadddelay.setBackground(SystemColor.menu);
        farmadder.add(lblFarmadderadddelay, "cell 0 3,alignx left");
        
        FARMADDER_ADD_DELAY_FIELD = new JTextField();
        FARMADDER_ADD_DELAY_FIELD.setText("250");
        farmadder.add(FARMADDER_ADD_DELAY_FIELD, "cell 1 3");
        FARMADDER_ADD_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs_1 = new JLabel("ms");
        farmadder.add(lblMs_1, "cell 1 3");
        
        FARMADDER_EXPAND_DELAY_FIELD = new JTextField();
        FARMADDER_EXPAND_DELAY_FIELD.setText("150");
        farmadder.add(FARMADDER_EXPAND_DELAY_FIELD, "cell 1 2");
        FARMADDER_EXPAND_DELAY_FIELD.setColumns(10);
        
        JLabel lblNewLabel_1 = new JLabel("Stop farm sending when:");
        lblNewLabel_1.setBackground(SystemColor.menu);
        farmlistsender.add(lblNewLabel_1, "cell 0 0");
        
        attack_lost_box = new JCheckBox("");
        attack_lost_box.setBackground(SystemColor.menu);
        attack_lost_box.setOpaque(false);
        attack_lost_box.setSelected(true);
        farmlistsender.add(attack_lost_box, "flowx,cell 0 1");
        
        attack_win_loss_box = new JCheckBox("");
        attack_win_loss_box.setBackground(SystemColor.menu);
        attack_win_loss_box.setOpaque(false);
        attack_win_loss_box.setSelected(true);
        farmlistsender.add(attack_win_loss_box, "flowx,cell 0 2");
        
        JLabel lblFarmlistSenderCustom = new JLabel("Farmlist sender custom delays:");
        lblFarmlistSenderCustom.setBackground(SystemColor.menu);
        farmlistsender.add(lblFarmlistSenderCustom, "cell 0 5");
        
        JLabel lblAttackLost = new JLabel("Attacker lost");
        lblAttackLost.setBackground(SystemColor.menu);
        lblAttackLost.setIcon(new ImageIcon(Settings.class.getResource("/res/lost_attacker.png")));
        farmlistsender.add(lblAttackLost, "cell 0 1");
        
        JLabel lblNewLabel_2 = new JLabel("Attacker won with a loss");
        lblNewLabel_2.setBackground(SystemColor.menu);
        lblNewLabel_2.setIcon(new ImageIcon(Settings.class.getResource("/res/won_attacker_2.png")));
        farmlistsender.add(lblNewLabel_2, "cell 0 2");
        
        JPanel panel_1 = new JPanel();
        panel_1.setBackground(SystemColor.menu);
        farmlistsender.add(panel_1, "cell 0 6,growx,aligny top");
        panel_1.setLayout(new MigLayout("", "[][grow][grow][][][]", "[][][][][][][][]"));
        
        JLabel lblFarmlistsenderinitdelay = new JLabel("FARMLISTSENDER_INIT_DELAY:");
        lblFarmlistsenderinitdelay.setBackground(SystemColor.menu);
        panel_1.add(lblFarmlistsenderinitdelay, "cell 0 0");
        
        FARMLISTSENDER_INIT_DELAY_FIELD = new JTextField();
        FARMLISTSENDER_INIT_DELAY_FIELD.setText("500");
        panel_1.add(FARMLISTSENDER_INIT_DELAY_FIELD, "cell 1 0,growx,aligny top");
        FARMLISTSENDER_INIT_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs_2 = new JLabel("ms");
        lblMs_2.setBackground(SystemColor.menu);
        panel_1.add(lblMs_2, "cell 2 0");
        
        JLabel lblNewLabel_6 = new JLabel("FARMLISTSENDER_EXPAND_DELAY:");
        lblNewLabel_6.setBackground(SystemColor.menu);
        panel_1.add(lblNewLabel_6, "cell 0 1");
        
        FARMLISTSENDER_EXPAND_DELAY_FIELD = new JTextField();
        FARMLISTSENDER_EXPAND_DELAY_FIELD.setText("250");
        panel_1.add(FARMLISTSENDER_EXPAND_DELAY_FIELD, "cell 1 1,growx");
        FARMLISTSENDER_EXPAND_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs_5 = new JLabel("ms");
        lblMs_5.setBackground(SystemColor.menu);
        panel_1.add(lblMs_5, "cell 2 1");
        
        JLabel lblFarmlistsendercheckdelay = new JLabel("FARMLISTSENDER_CHECK_DELAY:");
        lblFarmlistsendercheckdelay.setBackground(SystemColor.menu);
        panel_1.add(lblFarmlistsendercheckdelay, "cell 0 2");
        
        FARMLISTSENDER_CHECK_DELAY_FIELD = new JTextField();
        FARMLISTSENDER_CHECK_DELAY_FIELD.setText("100");
        panel_1.add(FARMLISTSENDER_CHECK_DELAY_FIELD, "cell 1 2,growx");
        FARMLISTSENDER_CHECK_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs_3 = new JLabel("ms");
        lblMs_3.setBackground(SystemColor.menu);
        panel_1.add(lblMs_3, "cell 2 2");
        
        JLabel lblFarmlistsendersenddelay = new JLabel("FARMLISTSENDER_SEND_DELAY:");
        lblFarmlistsendersenddelay.setBackground(SystemColor.menu);
        panel_1.add(lblFarmlistsendersenddelay, "cell 0 3");
        
        FARMLISTSENDER_SEND_DELAY_FIELD = new JTextField();
        FARMLISTSENDER_SEND_DELAY_FIELD.setText("100");
        panel_1.add(FARMLISTSENDER_SEND_DELAY_FIELD, "cell 1 3,growx");
        FARMLISTSENDER_SEND_DELAY_FIELD.setColumns(10);
        
        JLabel lblMs_4 = new JLabel("ms");
        lblMs_4.setBackground(SystemColor.menu);
        panel_1.add(lblMs_4, "cell 2 3");
        
        JLabel lbldelaysWillDirectly = new JLabel("Delays will directly affect farmlist sending speed");
        lbldelaysWillDirectly.setBackground(SystemColor.menu);
        panel_1.add(lbldelaysWillDirectly, "cell 0 5");
        
        JLabel lblNewLabel_7 = new JLabel("(All data is saved on login)");
        lblNewLabel_7.setBackground(SystemColor.menu);
        panel_1.add(lblNewLabel_7, "cell 0 6 2 1");
        
        JButton btnSaveData_1 = new JButton("Save data");
        btnSaveData_1.setBackground(UIManager.getColor("Button.background"));
        btnSaveData_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		BotFrame.saveDelays();
        	}
        });
        panel_1.add(btnSaveData_1, "cell 0 7");
        
              JLabel lblNewLabel_4 = new JLabel("ms");
              farmadder.add(lblNewLabel_4, "cell 1 2");
              
              JLabel lblDelaysWillDirectly = new JLabel("Delays will directly affect FarmAdder speed");
              lblDelaysWillDirectly.setBackground(SystemColor.menu);
              farmadder.add(lblDelaysWillDirectly, "cell 0 5");
              
              JLabel lblallDataIs = new JLabel("(All data is saved on login)");
              lblallDataIs.setBackground(SystemColor.menu);
              farmadder.add(lblallDataIs, "cell 0 6");
              
              JButton btnSaveData = new JButton("Save data");
              btnSaveData.setBackground(UIManager.getColor("Button.background"));
              btnSaveData.addActionListener(new ActionListener() {
              	public void actionPerformed(ActionEvent arg0) {
              		BotFrame.saveDelays();
              	}
              });
              farmadder.add(btnSaveData, "cell 0 7");
              
              JTextArea txtrTipLoweringThese = new JTextArea();
              txtrTipLoweringThese.setLineWrap(true);
              txtrTipLoweringThese.setBackground(SystemColor.menu);
              txtrTipLoweringThese.setFont(new Font("Tahoma", Font.PLAIN, 11));
              txtrTipLoweringThese.setText("Tip: Lowering these delays might speed up the addition process, but if you set them too low, the adder WILL encounter errors (because of some predefined JS animations). \r\nIf you are encoutering errors all the time, you can do the opposite and increase these delays.\r\nDisclaimer: Never leave FLAdder running if it encounters ERROR with every farm, pause it and tweak delays.");
              farmadder.add(txtrTipLoweringThese, "cell 0 9 3 1,grow");
              
		general.setLayout(new MigLayout("", "[grow]", "[][][][][][][][][][][]"));
		
		headlessCheck = new JCheckBox("Headless driver");
		headlessCheck.setBackground(SystemColor.menu);
		headlessCheck.setSelected(true);
		general.add(headlessCheck, "cell 0 0,grow");
		 
		JLabel lblNewLabel = new JLabel("Set server:");
		lblNewLabel.setBackground(SystemColor.menu);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		general.add(lblNewLabel, "cell 0 1");
		
		serverfield = new JTextField();
		general.add(serverfield, "cell 0 2,growx");
		serverfield.setColumns(10);
		PromptSupport.setPrompt("ts1.travian.com", serverfield);
		JSeparator separator = new JSeparator();
		general.add(separator, "cell 0 3");
		
		chckbxUseUseragent = new JCheckBox("Use useragent");
		chckbxUseUseragent.setBackground(SystemColor.menu);
		chckbxUseUseragent.setOpaque(false);
		chckbxUseUseragent.setSelected(true);
		general.add(chckbxUseUseragent, "cell 0 4");
		
		JLabel lblSetUseragent = new JLabel("Set useragent: (you can get it here for example: http://www.whatsmyua.info )");
		lblSetUseragent.setBackground(SystemColor.menu);
		general.add(lblSetUseragent, "cell 0 5");
		
		useragentfield = new JTextArea();
		useragentfield.setLineWrap(true);
		useragentfield.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		useragentfield.setRows(3);
		PromptSupport.setPrompt("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)\n Chrome/65.0.3325.181 Safari/537.36", useragentfield);
		general.add(useragentfield, "cell 0 6,growx");
		useragentfield.setColumns(10);
		
		JLabel lblGlobalWaitTimeout = new JLabel("Global WebElement wait timeout:");
		lblGlobalWaitTimeout.setBackground(SystemColor.menu);
		general.add(lblGlobalWaitTimeout, "flowx,cell 0 7");
		
		chckbxOverrideDir = new JCheckBox("Override current working directory");
		chckbxOverrideDir.setBackground(SystemColor.menu);
		general.add(chckbxOverrideDir, "flowx,cell 0 8");
		
		httpcheckbox = new JCheckBox("Use direct HTTP verification request");
		httpcheckbox.setBackground(SystemColor.menu);
		httpcheckbox.setOpaque(false);
		dev.add(httpcheckbox, "cell 0 1,alignx left");
		
		logincheck = new JCheckBox("Remember settings and login data");
		logincheck.setBackground(SystemColor.menu);
		logincheck.setOpaque(false);
		logincheck.setSelected(true);
		logincheck.setToolTipText("Different method of contacting the verification server");
		general.add(logincheck, "flowx,cell 0 9,alignx left");
		
		timeoutfield = new JTextField();
		timeoutfield.setText("15");
		general.add(timeoutfield, "cell 0 7");
		timeoutfield.setColumns(10);
		
		JButton btnGenerate = new JButton("Generate console log");
		btnGenerate.setBackground(UIManager.getColor("Button.background"));
		btnGenerate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				BufferedWriter out = null;
				try {
					out = new BufferedWriter(new FileWriter("consolelog.txt"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					Console.textArea.write(out);
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Console log generated in TravBot root directory");
			}
		});
		
		loggingcheckbox = new JCheckBox("Enable driver performance logging");
		loggingcheckbox.setBackground(SystemColor.menu);
		dev.add(loggingcheckbox, "cell 0 0");
		general.add(btnGenerate, "flowx,cell 0 10");
		
		JButton btnReopenConsole = new JButton("Reopen console");
		btnReopenConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					BotFrame.dialog = new Console();
					BotFrame.dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					BotFrame.dialog.setVisible(true);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		general.add(btnReopenConsole, "cell 0 10");
		
		dirTextField = new JTextField();
		dirTextField.setFont(new Font("Tahoma", Font.PLAIN, 9));
		general.add(dirTextField, "cell 0 8,growx");
		dirTextField.setColumns(10);
		
		JPanel cropchecker = new JPanel();
		cropchecker.setBorder(new EmptyBorder(5, 5, 5, 5));
		cropchecker.setBackground(SystemColor.menu);
		panel.add(cropchecker, "Crop checker");
		cropchecker.setLayout(new MigLayout("", "[grow][grow]", "[][][][][][grow][][][][][][grow]"));
		
		JLabel lblTargetVillage = new JLabel("Target village:");
		lblTargetVillage.setBackground(SystemColor.menu);
		cropchecker.add(lblTargetVillage, "flowx,cell 0 0,growx");
		
		villageComboBox = new JComboBox<String>();
		villageComboBox.setEnabled(false);
		villageComboBox.setMinimumSize(new Dimension(200, 22));
		cropchecker.add(villageComboBox, "cell 1 0,growx");
		
		rdbtnUnderflow = new JRadioButton("Underflow");
		rdbtnUnderflow.setBackground(SystemColor.menu);
		rdbtnUnderflow.setSelected(true);
		cropchecker.add(rdbtnUnderflow, "cell 0 1");
		
		JLabel lblExchangeCropWhen = new JLabel("when crop is below:");
		lblExchangeCropWhen.setBackground(SystemColor.menu);
		cropchecker.add(lblExchangeCropWhen, "flowx,cell 0 2,alignx left");
		
		cropBelow = new JTextField();
		cropBelow.setText("5");
		cropchecker.add(cropBelow, "flowx,cell 1 2,growx");
		cropBelow.setColumns(10);
		
		rdbtnOverflow = new JRadioButton("Overflow");
		rdbtnOverflow.setBackground(SystemColor.menu);
		cropchecker.add(rdbtnOverflow, "flowx,cell 0 3");      
		
		JTextPane txtpnTheNumberAbove = new JTextPane();
		txtpnTheNumberAbove.setBackground(SystemColor.menu);
		cropchecker.add(txtpnTheNumberAbove, "cell 0 3,grow");
		
		JLabel lblExchangeCropWhen_1 = new JLabel("Exchange when crop is above:");
		lblExchangeCropWhen_1.setBackground(SystemColor.menu);
		cropchecker.add(lblExchangeCropWhen_1, "cell 0 4,alignx left");
		
		cropAbove = new JTextField();
		cropAbove.setText("90");
		cropchecker.add(cropAbove, "flowx,cell 1 4,growx");
		cropAbove.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		cropchecker.add(panel_2, "cell 0 6 2 1,grow");
		panel_2.setLayout(new MigLayout("", "[][grow][grow][grow][grow]", "[][]"));
		
		JLabel lblWood = new JLabel("Wood:");
		panel_2.add(lblWood, "cell 1 0");
		
		JLabel lblBrick = new JLabel("Clay:");
		panel_2.add(lblBrick, "cell 2 0");
		
		JLabel lblIron = new JLabel("Iron:");
		panel_2.add(lblIron, "cell 3 0");
		
		JLabel lblCrop = new JLabel("Crop:");
		panel_2.add(lblCrop, "cell 4 0");
		
		JLabel label = new JLabel("%");
		panel_2.add(label, "cell 0 1,alignx trailing");
		
		fieldWood = new JTextField();
		fieldWood.setText("0");
		panel_2.add(fieldWood, "flowx,cell 1 1,growx");
		fieldWood.setColumns(10);
		
		fieldClay = new JTextField();
		fieldClay.setText("0");
		panel_2.add(fieldClay, "cell 2 1,growx");
		fieldClay.setColumns(10);
		
		fieldIron = new JTextField();
		fieldIron.setText("0");
		panel_2.add(fieldIron, "cell 3 1,growx");
		fieldIron.setColumns(10);
		
		fieldCrop = new JTextField();
		fieldCrop.setText("100");
		panel_2.add(fieldCrop, "cell 4 1,growx");
		fieldCrop.setColumns(10);
		
		JLabel lblResourcesWillGet = new JLabel("Resources will get balanced by the NPC trader");
		lblResourcesWillGet.setBackground(SystemColor.menu);
		cropchecker.add(lblResourcesWillGet, "cell 0 7 2 1");
		
		JLabel lblYouCanSet = new JLabel("You can set a resource to 9999999 to trade everything else into it");
		lblYouCanSet.setBackground(SystemColor.menu);
		cropchecker.add(lblYouCanSet, "cell 0 8 2 1");
		
		JLabel lblCropCheckerPolling = new JLabel("Crop checker polling rate (sec)");
		lblCropCheckerPolling.setBackground(SystemColor.menu);
		cropchecker.add(lblCropCheckerPolling, "cell 0 10,alignx left");
		
		cropRate = new JTextField();
		cropRate.setText("600");
		cropchecker.add(cropRate, "cell 1 10,growx");
		cropRate.setColumns(10);
		
		JLabel lblTheseSettingsAre = new JLabel("These settings are applied during runtime and should be set AFTER logging in!");
		lblTheseSettingsAre.setBackground(SystemColor.menu);
		cropchecker.add(lblTheseSettingsAre, "cell 0 11 2 1,aligny bottom");
		
		list = new JList<Object>(items);
		panel_3.add(list, "cell 0 0,alignx left,growy");
		list.setModel(new DynamicListModel<Object>(new String[] {"General", "Farmlist sender", "Farm adder", "Crop checker", "Auto evader", "Proxy", "Dev"}));
		list.setSelectedIndex(0);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    String card = BotFrame.langUtils.getTemplatePhraseFor(list.getSelectedValue().toString());
                    CardLayout cL = (CardLayout) (panel.getLayout());
                    cL.show(panel, card);                   
                }
            }
        });
        
        ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(rdbtnUnderflow); 
		bgroup.add(rdbtnOverflow);
		
		JLabel label_1 = new JLabel("%");
		cropchecker.add(label_1, "cell 1 2,alignx left");
		
		JLabel label_2 = new JLabel("%");
		cropchecker.add(label_2, "cell 1 4,alignx left");
		
		JPanel autoevade = new JPanel();
		autoevade.setBorder(new EmptyBorder(5, 5, 5, 5));
		autoevade.setBackground(SystemColor.menu);
		panel.add(autoevade, "Auto evader");
		autoevade.setLayout(new MigLayout("", "[grow][grow]", "[][][][grow]"));
		
		JLabel lblAutoevadeScanningInterval = new JLabel("AutoEvade scanning interval (sec): ");
		autoevade.add(lblAutoevadeScanningInterval, "cell 0 0,alignx left");
		
		autoEvadeScanInterval = new JTextField();
		autoEvadeScanInterval.setText("1200");
		autoevade.add(autoEvadeScanInterval, "cell 1 0,growx");
		autoEvadeScanInterval.setColumns(10);
		
		JLabel lblEvadeWhenThe = new JLabel("Evade when the attack is arriving in (sec):\r\n");
		autoevade.add(lblEvadeWhenThe, "cell 0 1,alignx left");
		
		autoEvadeDelay = new JTextField();
		autoEvadeDelay.setText("30");
		autoevade.add(autoEvadeDelay, "cell 1 1,growx");
		autoEvadeDelay.setColumns(10);
		
		JLabel lblSetTheTarget = new JLabel("Where to send the evasion (each village has its own target):");
		autoevade.add(lblSetTheTarget, "cell 0 2");
		
		autoEvadeScrollPane = new JScrollPane();
		autoEvadeScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		autoevade.add(autoEvadeScrollPane, "cell 0 3 2 1,grow");
		
		autoEvadePanel = new JPanel();
		autoEvadeScrollPane.setRowHeaderView(autoEvadePanel);
		autoEvadePanel.setLayout(new MigLayout("", "[]", "[][]"));

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
	}

}
