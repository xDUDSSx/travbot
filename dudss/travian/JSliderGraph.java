package dudss.travian;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

public class JSliderGraph extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int width;
	int height;
	
	int gap = 40;
	
	Color backgroundLineColor = new Color(0, 60, 0);
	Color foregroundLineColor = new Color(0, 255, 0);
	Color dotColor = new Color(0, 191, 0);
	
	AlphaComposite alcomF = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
	AlphaComposite alcomB = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f);
	
	SliderGraph sliderGraph;
	
	JSliderGraph(int width, int height) {
		this.width = width;
		this.height = height;
		
		setMinimumSize(new Dimension (width, height));

		MigLayout layout = new MigLayout("gap 0, gapx 0, gapy 0, fill, insets 0", "[" + gap +"px][" + (width - gap)+ "px]", "[" + (height - gap)+ "px][" + gap + "px]");
		setLayout(layout);
		
		sliderGraph = new SliderGraph(width-gap, height-gap);
		sliderGraph.setSize(width-gap, height-gap);
		add(sliderGraph, "cell 1 0,grow, gap 0");
		
		JPanel leftDes = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(Color.BLACK);
				g2d.fillRect(0, 0, getWidth(), getHeight());
				
				BasicStroke basic = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
				g2d.setStroke(basic);
				g2d.setColor(Color.WHITE);
				
				double heightPercentage = (getHeight()/2.0)/100.0;

				g2d.setColor(backgroundLineColor);
				g2d.setComposite(alcomB);
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*200)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*200)));
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*150)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*150)));
				
				g2d.setColor(foregroundLineColor);
				g2d.setComposite(alcomF);
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*100)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*100)));
				g2d.setColor(backgroundLineColor);
				g2d.setComposite(alcomB);
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*50)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*50)));
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*25)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*25)));
				g2d.drawLine(0, (int)Math.round(getHeight() - (heightPercentage*10)), getWidth(), (int)Math.round(getHeight() - (heightPercentage*10)));
				
				g2d.setColor(Color.green);
				g2d.setComposite(alcomF);
				g2d.drawString("200%", getWidth()/2 - (g.getFontMetrics().stringWidth("200%")/2) ,  (int)Math.round(getHeight() - (heightPercentage*200)) + (g.getFontMetrics().getHeight()) - 3);
				g2d.drawString("150%", getWidth()/2 - (g.getFontMetrics().stringWidth("150%")/2) ,  (int)Math.round(getHeight() - (heightPercentage*150)) + (g.getFontMetrics().getHeight()) - 3);
				g2d.drawString("100%", getWidth()/2 - (g.getFontMetrics().stringWidth("100%")/2) ,  (int)Math.round(getHeight() - (heightPercentage*100)) + (g.getFontMetrics().getHeight()) - 3);
				g2d.drawString("50%", getWidth()/2 - (g.getFontMetrics().stringWidth("50%")/2) ,    (int)Math.round(getHeight() - (heightPercentage*50)) + (g.getFontMetrics().getHeight()) - 3);
				g2d.drawString("25%", getWidth()/2 - (g.getFontMetrics().stringWidth("25%")/2) ,   (int)Math.round(getHeight() - (heightPercentage*25)) + (g.getFontMetrics().getHeight()) - 3);
				g2d.drawString("10%", getWidth()/2 - (g.getFontMetrics().stringWidth("10%")/2) ,    (int)Math.round(getHeight() - (heightPercentage*10)) + (g.getFontMetrics().getHeight()) - 3);
			}
		};

		leftDes.setSize(gap, height);
		add(leftDes, "cell 0 0,grow");
				
		JPanel rightDes = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(Color.BLACK);
				g2d.fillRect(0, 0, getWidth(), getHeight());
				
				double widthPercentage = (getWidth()/2.0)/100.0;
				
				g2d.setColor(Color.green);
				for(int i = 0; i < 24; i++) {
					g2d.drawString((i + 1) + "h", (int)Math.round(((getWidth() / 24) * i) - (g.getFontMetrics().stringWidth((i + 1) + "h")/2)) + 8, getHeight()/2 - (g.getFontMetrics().getHeight()/2));
				}
			}
		};
		rightDes.setSize(width, gap);
		add(rightDes, "cell 1 1,grow");
		
		JPanel extra = new JPanel();
		extra.setBackground(Color.black);
		add(extra, "cell 0 1, grow");
		
		/*class ResizeAdapter extends ComponentAdapter {
			public void componentResized(ComponentEvent e) {
				int WIDTH = JSliderGraph.this.getWidth();
				int HEIGHT = JSliderGraph.this.getHeight();				
				layout.setColumnConstraints("[" + gap +"px][" + (WIDTH - gap)+ "px]");
				layout.setRowConstraints("[" + (HEIGHT - gap)+ "px][" + gap + "px]");	
				//System.out.println("JSliderSize: " + WIDTH + "-" + HEIGHT);
		    }
		}	
		this.addComponentListener(new ResizeAdapter());
		*/	
	}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */