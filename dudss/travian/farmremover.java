package dudss.travian;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class FarmRemover extends Travian implements Runnable {
	String id;
	String keyword;
	
	FarmRemover(String id, String keyword) {
		this.id = id;
		this.keyword = keyword;
	}
	
	public void run() {
		try {
			farmremove(id, keyword);
			driverInUse.set(false);
		} catch (Exception e) {
			System.err.println("Removefarm Thread run error!");
			e.printStackTrace();
		}
	}
	void farmremove(String id, String keyword) throws InterruptedException {
		
		ArrayList<String> slotsToDelete = new ArrayList<String>();
		
		while(!Travian.driverInUse.compareAndSet(false, true)) {
			System.out.println("editfarmclass waiting for driver access (1 sec) ...");
			Thread.sleep(1000);
		}
		Actions actions = new Actions(driver);
	
		Travian.driver.get(travian_url_farm);
		System.out.println(driver.getCurrentUrl());
		Boolean listcontentIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[2]", 10, 500);
		if (listcontentIsPresent == false) {
			System.err.println("Could not find farmlist id: " + id + " - could not find element");
		} 
		WebElement listcontent = driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[2]"));
		
		if (listcontent.getAttribute("class").equals("listContent hide")) {
			System.out.println("Expanding fl ...");
			Boolean expanderIsPresent = FarmListEdit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[1]/div[1]/div/div", 10, 500, 1);
			if (expanderIsPresent == false) {
				System.err.println("Farmlist expansion failed - could not find element");	
			}					
			WebElement expand = driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[1]/div[1]/div/div"));	
			Boolean loadingimgIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[1]/div[2]/img", 10, 500);
			if (loadingimgIsPresent == false) {
				System.err.println("Could not find loading img");
			} 
			WebElement loadingimg = driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[1]/div[2]/img"));
			actions.moveToElement(expand).click().perform();
			Thread.sleep(50);
			if(loadingimg.getAttribute("class").equals("loading")) {
				while(loadingimg.getAttribute("class").equals("loading")) {
					System.out.println("FL loading, polling rate: 100");
					Thread.sleep(100);
				}
			}
			if(loadingimg.getAttribute("class").equals("loading hide")) {
				System.out.println("Loading finished");
			}
		}
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list" + id + "']/form/div[2]/div[1]/table/tbody/tr")));
		List<WebElement> rows = Travian.driver.findElements(By.xpath("//*[@id='list" + id + "']/form/div[2]/div[1]/table/tbody/tr"));
		for(WebElement row : rows) {
			String rowid = row.getAttribute("id");  // slot-row-110561 ->
			String idonly = rowid.replace("slot-row-", ""); // -> 110561 (id itself)	
			
			boolean exists = Travian.driver.findElements(By.xpath("//*[@id='" + rowid + "']/td[2]/a")).size() != 0;
			if(exists == true) {
				WebElement farmname = Travian.driver.findElement(By.xpath("//*[@id='" + rowid + "']/td[2]/a"));
				
				String name = farmname.getText();
				System.out.println("Slot " + rowid + "name: " + name);
				
				if (name.contains(keyword)) {
					slotsToDelete.add(idonly);
				}
			} else {
				System.out.println("Could not find slot " + rowid);
			}
		}	
		
		System.out.println("FL farms parsed - deleting");
		System.out.println("Deleting farms: " + slotsToDelete.toString());
		
		for (String x : slotsToDelete) {
			Thread.sleep(50);
			System.out.println("deleting slot " + x);
			Travian.runscript("Travian.Game.RaidList.deleteSlot(" + x + ", false)");
			
			Boolean loadingimgIsPresentAgain = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id + "']/form/div[1]/div[2]/img", 10, 500);
			if (loadingimgIsPresentAgain == false) {
				System.err.println("Could not find loading img");
			} 
			WebElement loadingimgagain = driver.findElement(By.xpath("//*[@id='list" + id + "']/form/div[1]/div[2]/img"));
			
			if(loadingimgagain.getAttribute("class") == "loading") {
				while(loadingimgagain.getAttribute("class") == "loading") {
					System.out.println("FL loading, polling rate: 70ms");
					Thread.sleep(70);
				}
			}			
		}
	}
}
