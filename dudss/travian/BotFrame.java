package dudss.travian;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.SystemColor;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;

import dudss.travian.autoevade.AutoEvader;
import dudss.travian.autoevade.EvadeVillageEntry;
import dudss.travian.crop.CropChecker;
import dudss.travian.lang.LanguageUtils;
import net.miginfocom.swing.MigLayout;

public class BotFrame extends JFrame implements TableModelListener, MouseListener {

	private static final long serialVersionUID = 1L;
	
	static String ver = "v2.2u";
	
	public static String frameUsername;
	public static String framePassword;
	public static String frameServer;
	public static String frameUserAgent;
	
	static Boolean frameNewFL;
	
	static String frameX;
	static String frameY;
	static String frameRange;
	static String frame_idedit;
	
	static Boolean frameCheckNatars;
	static Boolean frameAntiNoob;
	static String frameMaxDiff;
	
	static JCheckBox checknatars;
	static JCheckBox antinoob;
	static JTextField maxdiff;
	
	static String frameNumTroops;
	static String frame_getterurl;

	static Date date;
	
	public static int timeout = 15;
	
	private JLayeredPane contentPane;
	private JTextField userField;
	private JTextField xField;
	private JTextField yField;
	private JTextField rangeField;
	private JTextField troopField;
	public static JTable table;

	public static JButton initbtn;
	public static JButton btnGetter;
	static Boolean getter_btnstate;
	static Boolean getter_btnclicked = false;
	static Boolean settings_btnstate = false;
	public static DefaultTableModel model;
	public static Boolean tableignore = false;
	public static Boolean frameexpanded = false;
	public static JButton loginbtn;
	public static JComboBox<Object> IdComboBox;
	public static JComboBox<Object> UnitComboBox;
	public static JComboBox<Object> villageComboBox;
	
	public static JPanel getterpanel;
	public static JPanel advancedpanel;
	public static JTabbedPane farmAdderTabbedPane;
	public static JDialog dlg;
	public static JProgressBar dpb;
	public static JLabel dlabel;
	static JLabel helpeditor;
	
	static Boolean loggingin = false;
	
	public static Boolean remembercheck;
	public static Boolean headless;
	public static Boolean useragentcheck;
	public static Boolean useproxy;
	public static Boolean enablelogging;
	public static String proxy;
	public static String ip;
	public static String cryptodata;
	
	public static String workingDirectoryPath = System.getProperty("user.dir");

	private JPasswordField passwordfield;
	
	public static ImageIcon loadingicon;
	private final JPanel extenderpanel = new JPanel();
	
	public static int FARMADDER_INIT_DELAY;
	public static int FARMADDER_EXPAND_DELAY;
	public static int FARMADDER_ADD_DELAY;
	
	public static int FARMLISTSENDER_INIT_DELAY;
	public static int FARMLISTSENDER_EXPAND_DELAY;
	public static int FARMLISTSENDER_CHECK_DELAY;
	public static int FARMLISTSENDER_SEND_DELAY;

	public static BotFrame MainGUI;
	public static Settings SettingsGUI;
	public static Console dialog;
	public static JTextField removerTextField;
	public static JComboBox<Object> removerComboBox;
	public static String frame_remover_id;
	public static String frame_remover_keyword;
	public static JButton btnRemove;
	private JTextArea textField;
	
	//outdated
	static JButton cropbtn;
	
	//current
	static JButton btnCropchecker;
	static JButton btnAutoEvade;
	
	public static boolean langDialog = false;
	public static LanguageUtils langUtils;
	public static HashMap<Component, Integer> mainFrameLang = new HashMap<Component, Integer>();
	
	public static Logger logger;
	private JLabel errorlbl;
	
	/**
	 * TravBot - Travian FL automation tool by DUDSS - http://travbot.xf.cz/
	 */
	
	public static void main(String[] args) {
		logger = Logger.getLogger(BotFrame.class.getSimpleName());
		logger.setLevel(Level.FINER);
		
		//Thread EVENTQUEUE
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				date = new Date();			
				
				langUtils = new LanguageUtils();
				
				//OS check
				if (SystemUtils.IS_OS_WINDOWS) {
						logger.info("Windows detected.");
						ver = ver.substring(0, ver.length()-1) + "win";
					} else if (SystemUtils.IS_OS_MAC) {
						logger.info("MacOS detected.");
						ver = ver.substring(0, ver.length()-1) + "mac";
					} else if (SystemUtils.IS_OS_LINUX) {
						logger.info("Linux detected.");
						ver = ver.substring(0, ver.length()-1) + "lin";
					} else {
						logger.warning("This operating system is not supported!");
						ver = ver.substring(0, ver.length()-1) + "x";
						JOptionPane.showMessageDialog(null,
						"This operating system is not supported. (" + SystemUtils.OS_NAME + ")",
						"Unsupported OS",
						JOptionPane.ERROR_MESSAGE);
					    System.exit(0);
				}

				try {
					System.out.println("Starting main GUI");					
					MainGUI = new BotFrame();
					BotFrame.translate(MainGUI);			
					MainGUI.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		//Thread MAIN
	}
	
	public static void translate(JFrame frame) {
		langUtils.setLanguage(langUtils.selectedLanguage);
		for (Component c : getAllComponents(frame)) {
			if (c instanceof AbstractButton) {
				((AbstractButton)c).setText(langUtils.getPhraseFor(((AbstractButton)c).getText()));
				c.setPreferredSize(null);
			} else 
			if (c instanceof JLabel) {
				((JLabel)c).setText(langUtils.getPhraseFor(((JLabel)c).getText()));
				c.setPreferredSize(null);
			}
			if (c instanceof JTable) {
				for (int i = 0; i < ((JTable)c).getColumnModel().getColumnCount(); i++) {
					if (((JTable)c).getColumnModel().getColumn(i).getHeaderValue() instanceof String) {
						((JTable)c).getColumnModel().getColumn(i).setHeaderValue(langUtils.getPhraseFor((String)((JTable)c).getColumnModel().getColumn(i).getHeaderValue()));
					}
				}
			}
			if (c instanceof JList && ((JList)c).getModel() instanceof DynamicListModel) {
				DynamicListModel model = (DynamicListModel) ((JList)c).getModel();
				
				for (int i = 0; i < model.getSize(); i++) {
					if (model.getElementAt(i) instanceof String) {
						model.setElementAt(i, langUtils.getPhraseFor(((String)model.getElementAt(i))));
					}
				}
				
				((JList)c).updateUI();		
			}
			
			if (frame == SettingsGUI) {
				SettingsGUI.getContentPane().revalidate();
				SettingsGUI.pack();
				SettingsGUI.setTitle(langUtils.getPhraseFor(SettingsGUI.getTitle()));
			}

			c.repaint();
		}
	}
	
	public static void setUIFont (javax.swing.plaf.FontUIResource f){
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
		    Object key = keys.nextElement();
		    Object value = UIManager.get (key);
		    if (value instanceof javax.swing.plaf.FontUIResource) UIManager.put (key, f);
	    }
	} 
	
	void initUI() throws URISyntaxException, IOException  {
		//Settings init	
		SettingsGUI = new Settings();
		SettingsGUI.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SettingsGUI.setVisible(false);
		translate(SettingsGUI);	
		
		setUIFont(new javax.swing.plaf.FontUIResource("Helvetica",Font.PLAIN, 12));
		setIconImage(Toolkit.getDefaultToolkit().getImage(BotFrame.class.getResource("/res/travicon.png")));
		setResizable(false);
		setTitle("TravBot " + ver + " - Travian farmlist bot by DUDSS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 728 + 12, 449 + 10 + 44);
		//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());	
		UIManager.put("ProgressBar.selectionBackground", Color.black);
	    UIManager.put("ProgressBar.selectionForeground", Color.white);
	    UIManager.put("ProgressBar.foreground", new Color(46, 209, 31));
		
		loadingicon = new ImageIcon(getClass().getResource("/res/loadingsmall.gif"));
		ImageIcon plus = new ImageIcon(getClass().getResource("/res/plus3.png"));
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton btnSettings = new JButton("Settings");
		mainFrameLang.put(btnSettings, 0);
		
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (settings_btnstate == false) {
					SettingsGUI.setVisible(true);
					settings_btnstate = true;
				} else {
					SettingsGUI.setVisible(false);
					settings_btnstate = false;
				}
			}
		});
		btnSettings.setBorderPainted(false); 
		btnSettings.setContentAreaFilled(false); 
		btnSettings.setFocusPainted(false); 
		btnSettings.setOpaque(false);
		menuBar.add(btnSettings);
		extenderpanel.setBackground(Color.WHITE);
		
		btnGetter = new JButton("");
		btnGetter.setOpaque(false);
		btnGetter.setEnabled(false);
		Image img = Toolkit.getDefaultToolkit().getImage(BotFrame.class.getResource("/res/downarrowtext2farm.png"));
		Border emptyBorder = BorderFactory.createEmptyBorder();
		
		btnGetter.setBorder(emptyBorder);
	    btnGetter.setIcon(new ImageIcon(img));
	    
	    JCheckBox chckbxMinimizeToTray = new JCheckBox("Minimize to tray");
	    mainFrameLang.put(chckbxMinimizeToTray, 4);
	    chckbxMinimizeToTray.setFont(new Font("Tahoma", Font.PLAIN, 11));
	    menuBar.add(chckbxMinimizeToTray);
	    chckbxMinimizeToTray.setOpaque(false);
	    
	    String cropCheckerOn = "CropChecker : ON";
	    String cropCheckerOff = "CropChecker : OFF";
	    
	    menuBar.add(Box.createHorizontalGlue());
		btnCropchecker = new JButton(cropCheckerOff);    	    
		btnCropchecker.setEnabled(false);
		btnCropchecker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (CropChecker.running) {
					CropChecker.stop();
					btnCropchecker.setText(cropCheckerOff);
					btnCropchecker.setBackground(new Color(205, 92, 92));
				} else {
					CropChecker.start();
					btnCropchecker.setText(cropCheckerOn);
					btnCropchecker.setBackground(new Color(127, 255, 0));
				}
			}
		});
		
		String autoEvaderOn = "AutoEvader : ON";
	    String autoEvaderOff = "AutoEvader : OFF";
		    
		btnAutoEvade = new JButton(autoEvaderOff);
		btnAutoEvade.setEnabled(false);
		btnAutoEvade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (AutoEvader.running) {
					AutoEvader.stop();
					btnAutoEvade.setText(autoEvaderOff);
					btnAutoEvade.setBackground(new Color(205, 92, 92));
				} else {
					AutoEvader.start();
					btnAutoEvade.setText(autoEvaderOn);
					btnAutoEvade.setBackground(new Color(127, 255, 0));
				}
			}
		});
		menuBar.add(btnAutoEvade);
		menuBar.add(btnCropchecker);
		menuBar.add(btnGetter);
		btnGetter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getterbtnclick();
			}
		});
		btnGetter.setFont(new Font("Tahoma", Font.PLAIN, 11));
	    
	    Component horizontalGlue = Box.createHorizontalGlue();
	    menuBar.add(horizontalGlue);
	    
	    JButton btnLang = new JButton("");
	    btnLang.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {	    		
    			//LANGUAGES
	    		Map<Object, Icon> langIcons = new HashMap<Object, Icon>();
	    		langIcons.put("English", new ImageIcon(BotFrame.class.getResource("/res/GB.png")));
	    		langIcons.put("Čeština", new ImageIcon(BotFrame.class.getResource("/res/CZ.png")));
	    		langIcons.put("Suomi", new ImageIcon(BotFrame.class.getResource("/res/FI.png")));
	    		langIcons.put("עִברִית", new ImageIcon(BotFrame.class.getResource("/res/IL.png")));
	    		langIcons.put("Deutsch", new ImageIcon(BotFrame.class.getResource("/res/DE.png")));
	    		
	    		JComboBox<Object> langCombo = new JComboBox<Object>(new Object[] { "English", "Čeština", "Suomi", "עִברִית", "Deutsch"});
	    		langCombo.setRenderer(new IconListRenderer(langIcons));		    		
				langCombo.setSelectedIndex(langUtils.selectedLanguage);
				//create a JDialog and add JOptionPane to it 
				JDialog diag = new JDialog(BotFrame.MainGUI, true);					
				langCombo.addItemListener(new ItemListener() {
	    			public void itemStateChanged(ItemEvent arg0) {
	    				if (arg0.getStateChange() == ItemEvent.SELECTED) {
	    					langUtils.setLanguage(langCombo.getSelectedIndex());
	    					translate(MainGUI);	
	    					translate(SettingsGUI);		    					
	    					btnLang.setIcon(langIcons.get(langCombo.getSelectedItem()));
	    					diag.dispose();
	    				}
	    			}
	    		});
				
				diag.getContentPane().add(langCombo);
				diag.addWindowListener(new WindowAdapter() {
				    @Override
				    public void windowClosed(WindowEvent e) {
				        langDialog = false;
				    }
				});
				diag.setTitle(langUtils.getPhraseFor("Language select"));					
				diag.getContentPane().add(langCombo);
				diag.pack();
				diag.setSize(300, diag.getHeight());
				diag.setLocationRelativeTo(BotFrame.MainGUI.getContentPane());
				diag.setVisible(true);
				langDialog = true;					
	    	}
	    });
	    btnLang.setBorder(emptyBorder);
	    btnLang.setIcon(new ImageIcon(BotFrame.class.getResource("/res/GB.png")));
	    menuBar.add(btnLang);
	    
	    Component horizontalStrut = Box.createHorizontalStrut(20);
	    menuBar.add(horizontalStrut);
			
		contentPane = new JLayeredPane();
		//contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		addWindowListener(new WindowAdapter()
	        {
	            @Override
	            public void windowClosing(WindowEvent e)
	            {
	                System.out.println("Closed");
	                Travian.quitmethod();
	            }
	        });
			
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 146, 712, 245);
		contentPane.add(scrollPane);

		model = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"#", "Farmlist name", "Interval", "MIN", "MAX", "Last sent", "Profile"
				}
			);
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );	
		
		//Farmlist sender table
		table = new JTable(model) {

            private static final long serialVersionUID = 1L;
            
            //Make Boolean object look like checkbox
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0)
                    return Boolean.class;
                return super.getColumnClass(columnIndex);
            }
            
            boolean[] columnEditables = new boolean[] {
    				true, false, true, true, true, false, true
            };
            
            @Override
            public boolean isCellEditable(int row, int column) {
    				return columnEditables[column];
            }
           
            //Edits table row background color - Red = never used, Green = active, White = used and stopped
            @Override   
            public Component prepareRenderer(
                    TableCellRenderer renderer, int row, int column)
                {
                    Component c = super.prepareRenderer(renderer, row, column);
                           
                    if((Boolean)table.getValueAt(row, 0) == true)
      	          {
      	        	  c.setForeground(Color.black);        
      	              c.setBackground(new Color(196, 255, 196));  
      	          }    
      	          
                  //if last sent table value is null (means that FL was never sent before)
      	          else if(table.getValueAt(row, 5) != null) 
      	          {
      	        	 c.setBackground(Color.white);    
     	             c.setForeground(Color.black); 
      	          }    
                    
      	          else if((Boolean)table.getValueAt(row, 0) == false)
    	          {    
    	              c.setBackground(new Color(255,236,236));    
    	              c.setForeground(Color.black);   	              
    	          }
                    
                  return c;
                }    
        };
        
        table.setOpaque(false);
		table.setFont(new Font("Arial Narrow", Font.BOLD, 14));
		table.setModel(model);
		
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(6).setResizable(false);
		
		table.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		
		table.getModel().addTableModelListener(this);
		
		float[] columnWidthPercentage = {0.05f, 0.35f, 0.07f, 0.07f, 0.07f, 0.2f, 0.14f};
		
		int tW = 681;
	    TableColumn column;
	    TableColumnModel jTableColumnModel = table.getColumnModel();
	    int cantCols = jTableColumnModel.getColumnCount();
	    for (int i = 0; i < cantCols; i++) {
	        column = jTableColumnModel.getColumn(i);
	        int pWidth = Math.round(columnWidthPercentage[i] * tW);
	        column.setPreferredWidth(pWidth);
	    }
	
		scrollPane.setOpaque(false);
		scrollPane.getViewport().setOpaque(false);
		scrollPane.setViewportView(table);
		
		Action setProfile = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
            	JTable table = (JTable)e.getSource();
                int modelRow = Integer.valueOf( e.getActionCommand() );
                
                System.out.println("row: " + modelRow);
                
                Profiler profiler = new Profiler(modelRow);
                profiler.setVisible(true);
                
            }
        };
         
        ButtonColumn buttonColumn = new ButtonColumn(table, setProfile, 6);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

		getter_btnstate = false;
		extenderpanel.setBounds(727, 0, 190, 393);
		contentPane.add(extenderpanel);
		extenderpanel.setLayout(new MigLayout("", "[grow]", "[][][][][][][]"));
		
		JLabel lblNewLabel_2 = new JLabel("Farm remover - Select farmlist");
		lblNewLabel_2.setEnabled(false);
		lblNewLabel_2.setVisible(false);
		
		cropbtn = new JButton("     Checkmodule: OFF     ");
		extenderpanel.add(cropbtn, "cell 0 0");	
		cropbtn.setEnabled(false);
		
		cropbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(CheckModule.checkrunning == false) {
					CheckModule.startscan();		
					cropbtn.setBackground(new Color(196, 255, 196));
					cropbtn.setText("     Checkmodule: ON     ");
					CheckModule.checkrunning = true;
					System.out.println("@checkmodule started");
			
				} else
				
				if(CheckModule.checkrunning == true) {
					CheckModule.stopscan();
					cropbtn.setBackground(new Color(255,236,236));
					cropbtn.setText("Checkmodule: OFF");
					CheckModule.checkrunning = false;
					System.out.println("@checkmodule stopped");
				}
			}
		});
		extenderpanel.setBackground(Color.WHITE);
		extenderpanel.add(cropbtn);
		
		textField = new JTextArea();
		extenderpanel.add(textField, "cell 0 1,growx");
		textField.setColumns(10);
		textField.setFont(new Font("Monospaced", Font.PLAIN, 11));
		textField.setText("Checkmodule is currently merged cropchecker and attack warnings, attack warnings are not finished. And cropchecker checks if crop is under 10 000, in that case it \"should\" trade every other resource into crop using NPC trader (will cost gold). I do not recommend using it. Its not tested properly");
		textField.setLineWrap(true);
		textField.setWrapStyleWord(true);

		extenderpanel.add(lblNewLabel_2, "cell 0 2");
		
		removerComboBox = new JComboBox<Object>();
		removerComboBox.setEnabled(true);
		removerComboBox.setVisible(true);
		extenderpanel.add(removerComboBox, "cell 0 3,growx");
		
		JLabel lblNewLabel_1 = new JLabel("Keyword:");
		lblNewLabel_1.setVisible(false);
		extenderpanel.add(lblNewLabel_1, "flowx,cell 0 4");
		
		removerTextField = new JTextField();
		removerTextField.setVisible(true);
		removerTextField.setEnabled(true);
		extenderpanel.add(removerTextField, "cell 0 4");
		PromptSupport.setPrompt("\"Natars\"", removerTextField);
		removerTextField.setColumns(10);
		
		//Button to start remover thread, button is hidden and unusable, dont change that
		btnRemove = new JButton("Remove");
		btnRemove.setVisible(true);
		btnRemove.setEnabled(true);
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame_remover_id = Travian.farmlists_id2.get(removerComboBox.getSelectedIndex());
				frame_remover_id = frame_remover_id.replace("list", "");
				frame_remover_keyword = removerTextField.getText();
				new Thread(new FarmRemover(frame_remover_id, frame_remover_keyword)).start();
			}
		});
		extenderpanel.add(btnRemove, "cell 0 5,alignx center");
		
		JButton btnTest = new JButton("Map scanner");
		btnTest.setEnabled(false);
		extenderpanel.add(btnTest, "cell 0 6,alignx center");
		btnTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Travian.driver.get("https://" + frameServer + "/karte.php?x=500&y=500");
				
				try {
					FarmListEdit.webElementRetryCycleWait("//*[@id='xCoordInputMap']", 5, 500, 1);
					FarmListEdit.webElementRetryCycleWait("//*[@id='yCoordInputMap']", 5, 500, 1);
					FarmListEdit.webElementRetryCycleWait("//*[@id='mapCoordEnter']/div/div/div/div/button/div/div[2]", 5, 500, 1);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}

				WebElement x = Travian.driver.findElement(By.xpath("//*[@id='xCoordInputMap']"));
				WebElement y = Travian.driver.findElement(By.xpath("//*[@id='yCoordInputMap']"));
				WebElement ok = Travian.driver.findElement(By.xpath("//*[@id='mapCoordEnter']/div/div/div/div/button/div/div[2]"));	
				
				int randomX = ThreadLocalRandom.current().nextInt(50, 500);
				int randomY = ThreadLocalRandom.current().nextInt(50, 500);
				x.sendKeys(String.valueOf(randomX));
				y.sendKeys(String.valueOf(randomY));
				
				ok.click();
				
				System.out.println("Retrieving browser performance data ...");
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				LogEntries les = Travian.driver.manage().logs().get(LogType.PERFORMANCE);
				String ajaxToken = "not_found";
				String cookie ="not_found";
				//	String requesttype = "not_found";
				System.out.println("Getting ajaxToken ...");
				    for (LogEntry le : les) {
				    	if (!le.getMessage().contains("Tracing.")) {
				    		System.out.println(le.getMessage());
				    		if(le.getMessage().contains("ajaxToken=")) {			    			
				    			String payload = le.getMessage();	
				    			//System.out.println(payload);
								ajaxToken = StringUtils.substringBetween(payload, "ajaxToken=", "\"");
								cookie = StringUtils.substringBetween(payload, "Cookie", "\"");
				    		}
				    	}
				    }   
				System.out.println("AjaxToken:" + ajaxToken);
				/*
				//Well this doesnt seem to work very well 
				System.out.println("Initiating XMLhttpRequest ... ");
				try {
					HttpClient httpclient = HttpClients.createDefault();
					HttpPost httppost = new HttpPost("https://" + frameServer + "/ajax.php");
					
					System.out.println("https://" + frameServer + "/ajax.php");
					
					// Request parameters and other properties.
					List<NameValuePair> params = new ArrayList<NameValuePair>(4);
					
					String tileX = "0";
					String tileY = "0";
					
					params.add(new BasicNameValuePair("cmd", "viewTileDetails"));
					params.add(new BasicNameValuePair("x", tileX));
					params.add(new BasicNameValuePair("y", tileY));
					params.add(new BasicNameValuePair("ajaxToken", ajaxToken));
					
					for(int i = 0; i<params.size(); i++) {
						System.out.println(params.get(i).toString());
					}
					
					httppost.setHeader("Pragma", "no-cache");
					httppost.setHeader("X-Request", "JSON");
					httppost.setHeader("User-Agent", frameUserAgent);
					httppost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					httppost.setHeader("Cache-Control", "no-cache");
					httppost.setHeader("X-Requested-With", "XMLHttpRequest");
					
					httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
				
					/*
					"headers": [
					            {
					              "name": "Pragma",
					              "value": "no-cache"
					            },
					            {
					              "name": "Origin",
					              "value": "https://ts5.travian.cz"
					            },
					            {
					              "name": "Accept-Encoding",
					              "value": "gzip, deflate, br"
					            },
					            {
					              "name": "Host",
					              "value": "ts5.travian.cz"
					            },
					            {
					              "name": "X-Request",
					              "value": "JSON"
					            },
					            {
					              "name": "User-Agent",
					              "value": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
					            },
					            {
					              "name": "Content-Type",
					              "value": "application/x-www-form-urlencoded; charset=UTF-8"
					            },
					            {
					              "name": "Accept-Language",
					              "value": "cs-CZ,cs;q=0.9"
					            },
					            {
					              "name": "Accept",
					              "value": "application/json, text/javascript, ; q=0.01"
					            },
					            {
					              "name": "Cache-Control",
					              "value": "no-cache"
					            },
					            {
					              "name": "X-Requested-With",
					              "value": "XMLHttpRequest"
					            },
					            {
					              "name": "Cookie",
					              "value": "CAD=fc0b53113f5c4a46%23%23%23%23%23%23; travian-language=cs-CZ; sess_id=70116a5e472c7d3dbeb724f6495cbedc; lowRes=0; T3E=gyFxIb4%2FH%2F%2ByVR3Jby%2BucA%3D%3D%3Acw9i63oIs1tvJm897IAsYPEFdjFR2jbfmUs%2BSlYcRP5jTCvONnVrFlkupZWjuL8wu%2B6GD2BB3rBKQwHd%2FPZrVprIJXdGhm8Kt2n%2FzILvdaMreAHYP4uEipZGCrc%2B1TxP%3ArLpIWvaZe28yurgHF7glmilm2MlDKkxIxaKAqlI40yU%3D; mapId1=%7B%22grid%22%3Atrue%7D"
					            },
					            {
					              "name": "Connection",
					              "value": "keep-alive"
					            },
					            {
					              "name": "Referer",
					              "value": "https://ts5.travian.cz/karte.php?x=-58&y=-15"
					            },
					            {
					              "name": "Content-Length",
					              "value": "74"
					            }
					
					            
					//Execute and get the response.
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					//String responseString = new BasicResponseHandler().handleResponse(response);
					//System.out.println("XMLhttpRequest response: \n");
					//System.out.println(responseString);
					
					if (entity != null) {				       
						String content =  EntityUtils.toString(entity);
						System.out.println("XMLhttpRequest response: \n");
						System.out.println(content);
					}
					
				/*	if (entity != null) {
					    InputStream instream = entity.getContent();
					    try {
					        
					    } finally {
					        instream.close();
					    }
					}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				 */
			}
		});
		contentPane.setLayer(btnTest, 1);
		
		helpeditor = new JLabel();
		helpeditor.setFont(new Font("Arial", Font.PLAIN, 10));
		helpeditor.setBounds(220, 0, 478, 117);;
		helpeditor.setText("<html><p><strong>SENDING farmlists</strong> - In the table below specify interval and time deviations (MIN/MAX). Check the checkbox to start. (in order to change interval or deviatons you need to stop sender and start it again with new values</p>\r\n" + 
				"<p><strong>ADDING farms to FL</strong> - Use Farm Adder to add farms into existing or new farmlist. Specify the origin for search (x,y of your village) and search range. You also need to select what farmlist to add to or in what village the new farmlist should be created. Lastly select type and number of troops and click \"Start\".</p>\r\n" + 
				"<p>For more in-depth information, click the button \"How to use?\" below.</p></html>");
		SimpleAttributeSet bold = new SimpleAttributeSet();
	    StyleConstants.setBold(bold, true);
	    StyleConstants.setForeground(bold, Color.black);
		
		
		contentPane.add(helpeditor); 
		
		final URI donate_uri = new URI("http://www.travbot.xf.cz/index.html#donatebutton");
		final URI site_uri = new URI("http://www.travbot.xf.cz/");
		final URI how_uri = new URI("http://www.travbot.xf.cz/howtouse.html");
		final URI change_uri = new URI("http://www.travbot.xf.cz/changelog.html");
		
	    class OpenDonateUrlListener implements ActionListener {	
		      @Override public void actionPerformed(ActionEvent e) {
		    	  if (Desktop.isDesktopSupported()) {
		    	      try {
		    	        Desktop.getDesktop().browse(donate_uri);
		    	      } catch (IOException exp) { 
		    	    	  exp.printStackTrace(); System.err.println("\n@@ URLOpen IOException @@\n"); 
		    	      }
		    	  } else { 
		    		   System.err.println("\n@@ OpenUrlListener Exception : Cannot open webpage in browser! @@\n");
		    	  }
		      }
	    }
	    class OpenSiteUrlListener implements ActionListener {	
		      @Override public void actionPerformed(ActionEvent e) {
		    	  if (Desktop.isDesktopSupported()) {
		    		  try {
		    	    	  	Desktop.getDesktop().browse(site_uri);
		    	      } catch (IOException exp) { 
		    	    	  	exp.printStackTrace(); System.err.println("\n@@ URLOpen IOException @@\n"); 
		    	      }
		    	  } else { 
		    		  	System.err.println("\n@@ OpenUrlListener Exception : Cannot open webpage in browser! @@\n");
		    	  }
		      }
	    }
	    class OpenHowUrlListener implements ActionListener {	
		      @Override public void actionPerformed(ActionEvent e) {
		    	  if (Desktop.isDesktopSupported()) {
		    	      try {
		    	        Desktop.getDesktop().browse(how_uri);
		    	      } catch (IOException exp) { 
		    	    	  exp.printStackTrace(); System.err.println("\n@@ URLOpen IOException @@\n"); 
		    	      }
		    	    } else { 
		    	    	System.err.println("\n@@ OpenUrlListener Exception : Cannot open webpage in browser! @@\n");
		    	    }
		      }
		    }
	    class OpenChangeUrlListener implements ActionListener {	
		      @Override public void actionPerformed(ActionEvent e) {
		    	  if (Desktop.isDesktopSupported()) {
		    	      try {
		    	        Desktop.getDesktop().browse(change_uri);
		    	      } catch (IOException exp) { 
		    	    	  exp.printStackTrace(); System.err.println("\n@@ URLOpen IOException @@\n"); 
		    	      }
		    	    } else { 
		    	    	System.err.println("\n@@ OpenUrlListener Exception : Cannot open webpage in browser! @@\n");
		    	    }
		      }
		    }
				
		//System tray minimizing
		if (!SystemTray.isSupported()) {
	        System.err.println("SystemTray is not supported");
	    } else {
	    	Image image = Toolkit.getDefaultToolkit().getImage(BotFrame.class.getResource("/res/trayicon.png"));
	    	final PopupMenu popup = new PopupMenu();
			final TrayIcon trayIcon = new TrayIcon(image, "TravBot - " + ver, popup);
			final SystemTray tray = SystemTray.getSystemTray();
			 
			MenuItem openItem = new MenuItem(langUtils.getPhraseFor("Open"));
		    openItem.addActionListener(new ActionListener() {
                @SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
                	dialog.show();
                	setVisible(true);  	
                }
            });
		    
		    MenuItem exitItem = new MenuItem(langUtils.getPhraseFor("Exit"));
		    exitItem.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		            Travian.quitmethod();
		            System.exit(0);
		        }
		    });
		    
	        trayIcon.addActionListener(new ActionListener() {
	            @SuppressWarnings("deprecation")
				@Override
	            public void actionPerformed(ActionEvent e) {
	            	dialog.show();
	            	setVisible(true);	
	            	setState(Frame.NORMAL);
	            	tray.remove(trayIcon);
	            }
	        });
	        
		    popup.add(openItem);
		    popup.addSeparator();
		    popup.add(exitItem);
		   
		    trayIcon.setPopupMenu(popup);
		    
		    //If window state changes, program gets added to the tray and is hidden
		    addWindowStateListener(new WindowStateListener() {
	            @SuppressWarnings("deprecation")
				public void windowStateChanged(WindowEvent e) {
	                if(e.getNewState()==ICONIFIED){
	                	if(chckbxMinimizeToTray.isSelected() == true) {
		                	try {
		                        tray.add(trayIcon);
		                        setVisible(false);
		                        dialog.hide();
		                        System.out.println("added to SystemTray");
		                    } catch (AWTException ex) {
		                        System.out.println("unable to add to tray");
		                    }
	                	}
	                }
	            }
		    });
	    }
		
		//JFrame content pane is a layered pane, but layers are not set properly individually. I need it to be layered pane to show a background image.
		//This cycles through every component in contentPane (<-- name of the layered pane (dont even ask why O.o)) and sets its layer to 1
		//Set contentPane layers
		List<Component> components = getAllComponents(contentPane);
		for (Component c : components) {
			contentPane.setLayer(c, 1);
		}
		
		//background image (layer is 0 (bottom))
		JLabel backgroundlabel = new JLabel();
		backgroundlabel.setIcon(new ImageIcon(getClass().getResource("/res/contentpanetest.png")));
		backgroundlabel.setBounds(0, 0, 724,439);
		contentPane.add(backgroundlabel);
		contentPane.setLayer(backgroundlabel, 0);
 
		UIManager.put("TabbedPane.selected", new Color(255, 190, 107));
		
		farmAdderTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		farmAdderTabbedPane.setVisible(false);
		contentPane.setLayer(farmAdderTabbedPane, 1);
		farmAdderTabbedPane.setBounds(220, 0, 502, 142);

		contentPane.add(farmAdderTabbedPane);
		
		getterpanel = new JPanel();
		advancedpanel = new JPanel();
		farmAdderTabbedPane.addTab(langUtils.getPhraseFor("Farm adder"), null, getterpanel, null);
		farmAdderTabbedPane.addTab(langUtils.getPhraseFor("Advanced"), null, advancedpanel, null);
		farmAdderTabbedPane.setBackgroundAt(0, new Color(255, 219, 173));
		farmAdderTabbedPane.setBackgroundAt(1, new Color(255, 219, 173));
		
		//getterpanel.setVisible(true);
		getterpanel.setBackground(Color.WHITE);
		
		//advancedpanel.setVisible(true);
		advancedpanel.setBackground(Color.WHITE);
		advancedpanel.setLayout(new MigLayout("", "[][][grow]", "[][]"));
		
		checknatars = new JCheckBox("Include natars");
		checknatars.setFont(new Font("Tahoma", Font.PLAIN, 11));
		checknatars.setOpaque(false);
		advancedpanel.add(checknatars, "cell 0 0");
		
		JLabel lblMaxPopDifference = new JLabel("Max pop difference:");
		advancedpanel.add(lblMaxPopDifference, "cell 1 0,alignx trailing");
		
		maxdiff = new JTextField();
		maxdiff.setText("2");
		advancedpanel.add(maxdiff, "cell 2 0,growx");
		maxdiff.setColumns(10);
		
		antinoob = new JCheckBox("Enable antiNoob module (3 days minimum playtime)");
		antinoob.setFont(new Font("Tahoma", Font.PLAIN, 11));
		antinoob.setOpaque(false);
		antinoob.setSelected(true);
		advancedpanel.add(antinoob, "cell 0 1");
		
		JCheckBox newFLcheckbox = new JCheckBox("Create new FL (100+)");
		newFLcheckbox.setFont(new Font("Tahoma", Font.PLAIN, 11));
		newFLcheckbox.setBackground(Color.WHITE);
		newFLcheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(newFLcheckbox.isSelected() == true) {
					IdComboBox.setEnabled(false);
					villageComboBox.setEnabled(true);
				} else {
					IdComboBox.setEnabled(true);
					villageComboBox.setEnabled(false);
				}
					
			}
		});
		getterpanel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][][]", "[][][]"));
		
		IdComboBox = new JComboBox<Object>();
		getterpanel.add(IdComboBox, "cell 0 0 10 1,growx,aligny top"); 
		
		JLabel label_1 = new JLabel("<-- Use existing FL or  -->");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		getterpanel.add(label_1, "cell 10 0,growx,aligny center");
		
		xField = new JTextField();
		getterpanel.add(xField, "cell 0 1,grow");
		xField.setColumns(10);
		PromptSupport.setPrompt("  x", xField);
		
		JLabel label = new JLabel(":");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		getterpanel.add(label, "cell 2 1,growx,aligny center");
		
		yField = new JTextField();
		getterpanel.add(yField, "cell 3 1,grow");
		yField.setColumns(10);
		PromptSupport.setPrompt("  y", yField);
		
		JLabel lblRange = new JLabel("Range:");
		getterpanel.add(lblRange, "cell 5 1,alignx left,aligny center");
		lblRange.setFont(new Font("Sitka Text", Font.PLAIN, 11));
		
		rangeField = new JTextField();
		getterpanel.add(rangeField, "cell 7 1,grow");
		rangeField.setColumns(10);
		getterpanel.add(newFLcheckbox, "cell 13 0 2 1,growx,aligny bottom");
		
		JLabel lblVillage = new JLabel("In village: ");
		getterpanel.add(lblVillage, "cell 9 1 3 1,alignx right,aligny center");
		
		villageComboBox = new JComboBox<Object>();
		villageComboBox.setEnabled(false);
		getterpanel.add(villageComboBox, "cell 11 1 4 1,grow");
		
		UnitComboBox = new JComboBox<Object>();
		UnitComboBox.setRenderer(new UnitIconRenderer());
		getterpanel.add(UnitComboBox, "cell 0 2 10 1,growx,aligny top");
		
		UnitComboBox.addItem("Select troop type");
		UnitComboBox.addItem("-- Teutons --");
		
		UnitComboBox.addItem("clubswinger");
		UnitComboBox.addItem("spearman");
		UnitComboBox.addItem("axeman");
		UnitComboBox.addItem("paladin");
		UnitComboBox.addItem("teutonic knight");
		UnitComboBox.addItem("teutonic ram");
		UnitComboBox.addItem("catapult");
		UnitComboBox.addItem("chief");
		UnitComboBox.addItem("teutonic settler");
		
		UnitComboBox.addItem("-- Romans --");
		
		UnitComboBox.addItem("legionnaire");
		UnitComboBox.addItem("pretorian");
		UnitComboBox.addItem("imperian");
		UnitComboBox.addItem("EI");
		UnitComboBox.addItem("EC");
		UnitComboBox.addItem("roman ram");
		UnitComboBox.addItem("fire catapult");
		UnitComboBox.addItem("senator");
		UnitComboBox.addItem("roman settler");
		
		UnitComboBox.addItem("-- Gauls --");
		
		UnitComboBox.addItem("phalanx");
		UnitComboBox.addItem("swordsman");
		UnitComboBox.addItem("theutates thunder");
		UnitComboBox.addItem("druidrider");
		UnitComboBox.addItem("haeduan");
		UnitComboBox.addItem("gaul ram");
		UnitComboBox.addItem("trebuchet");
		UnitComboBox.addItem("cheiftain");
		UnitComboBox.addItem("gaul settler");
		
		JLabel lblNOfT = new JLabel("number of troops:");
		getterpanel.add(lblNOfT, "cell 10 2,alignx right,growy");
		
		troopField = new JTextField();
		getterpanel.add(troopField, "cell 13 2,growx,aligny top");
		troopField.setColumns(10);
		
		JButton btnGoEdit = new JButton("Start");
		btnGoEdit.setIcon(plus);
		getterpanel.add(btnGoEdit, "cell 14 2,growx,aligny top");
		
		btnGoEdit.setFont(new Font("Sitka Text", Font.PLAIN, 11));
		
		btnGoEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (FarmListEdit.running == false) {
						
						//Loading data from GUI to variables
						
						frameX = xField.getText();
						frameY = yField.getText();
						frameRange = rangeField.getText();
						frameNumTroops = troopField.getText();
						
						if (frameX.equals("") || frameY.equals("") || frameRange.equals("") || frameNumTroops.equals("")) {
							System.err.println("Missing parameters!");
						} else {
							frameCheckNatars = checknatars.isSelected();
							frameAntiNoob = antinoob.isSelected();
							frameMaxDiff = maxdiff.getText();
							
							/*Following htmlunit code is loading gettertools servers page, according to the specified server it looks for the inactive search URL
							 *for that particular server. Reason for that is that sometimes the inactive search url ends with ".com/cs/ts1.travian.com.5/42-Search-inactives".
							 *Notice the number in the server string, in some cases it is not there and I have not figured out why it shows up or what it means, so I get the correct URL
							 *straight from gettertools
							 */
						
							//Turns off some weird exception logging, will most likely throw an error if this line is not present.
							//As far as I know htmlunit reports errors when it reads problems in JS. 
							//Those errors cause no trouble but htmlunit doesnt like them. I had some problems before and I decided to put this line before any htmlunit webclient init
							java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF); 
							
							WebClient clientgetter = new WebClient(BrowserVersion.CHROME);
							clientgetter.getOptions().setThrowExceptionOnScriptError(false);
							clientgetter.getOptions().setThrowExceptionOnFailingStatusCode(false);
							
							HtmlPage getterurlpage = null;
							HtmlPage finalgetterurlpage = null;
							try {
								//Edited from https://www.gettertools.com/cs/
							    getterurlpage = clientgetter.getPage("https://www.gettertools.com");
							} catch (Exception ex) {
							    System.err.println("GetterURL page error: " + ex.getMessage());
							    ex.printStackTrace();
							}
							
							HtmlSpan theFinalServerSpan = null;
							List<HtmlSpan> serverSpans = getterurlpage.getByXPath("//*[@class='world ']//*[contains(text(), '" + BotFrame.frameServer + "')]");
							for (HtmlSpan span : serverSpans) {
								String server = span.getTextContent();
								if (server.substring(1, server.length() - 1).equals(BotFrame.frameServer)) {
									theFinalServerSpan = span;
									break;
								}
							}
							
							try {
								finalgetterurlpage = theFinalServerSpan.click();
							} catch (NullPointerException exp) {
								System.err.println("Could not find the provided server! Check if it is in the format.");
								exp.printStackTrace();
								clientgetter.close();
								return;
							}
							String finalurl = finalgetterurlpage.getUrl().toString() + "42-Search-inactives";
							
							System.out.println(finalgetterurlpage.getUrl());
							System.out.println("\n" + finalurl);
							
							clientgetter.close();
							
							frame_getterurl = finalurl;
							
							//specifes if a new FL is going to be created or not
							//ID of the new FL is going to be aquired later
							if(newFLcheckbox.isSelected() == true) {
								frame_idedit = null;
								frameNewFL = true;
							
							//if FarmAdder uses already existing FL, ID is aquired from the gui dropdown
							} else {
								frameNewFL = false;
								frame_idedit = Travian.farmlists_id2.get(IdComboBox.getSelectedIndex());
								String frame_idedit_temp = frame_idedit.replace("list","");
								frame_idedit = frame_idedit_temp;
							}
							
							System.out.println(frame_idedit);
							
							FARMLISTSENDER_SEND_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_SEND_DELAY_FIELD.getText());
							new Thread(new FarmListEdit(frameNewFL)).start();
						}
					} else {
						System.out.println("Only one instance of FarmAdder may run at the same time!");
					}
				} catch (FailingHttpStatusCodeException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		//explicitly sets getterpanel layer to be on top of everything else
		contentPane.setLayer(getterpanel, 2);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		contentPane.setLayer(bottomPanel, 1);
		bottomPanel.setBounds(10, 401, 713, 41);
		contentPane.add(bottomPanel);
		bottomPanel.setLayout(new MigLayout("", "[127px][][][][][grow]", "[25px]"));
		
		initbtn = new JButton("Refresh farmlists");
		bottomPanel.add(initbtn, "cell 0 0,alignx left,growy");
		initbtn.setEnabled(false);		
		mainFrameLang.put(initbtn, 5);
		
		JButton btnHowtouse = new JButton(langUtils.getPhraseFor("How to use?"));
		bottomPanel.add(btnHowtouse, "cell 2 0,growy");
		btnHowtouse.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnHowtouse.setForeground(new Color(0, 102, 255));
		btnHowtouse.setBorderPainted(false);
		btnHowtouse.setOpaque(false);
		btnHowtouse.setBackground(Color.WHITE);
		btnHowtouse.addActionListener(new OpenHowUrlListener());
		
		JButton btnTravbotxfcz = new JButton("<HTML><U>travbot.xf.cz</U></HTML>");
		bottomPanel.add(btnTravbotxfcz, "cell 3 0,growy");
		btnTravbotxfcz.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnTravbotxfcz.setForeground(new Color(0, 102, 255));
		btnTravbotxfcz.setBorderPainted(false);
		btnTravbotxfcz.setOpaque(false);
		btnTravbotxfcz.setBackground(Color.WHITE);
		btnTravbotxfcz.addActionListener(new OpenSiteUrlListener());
		JButton btndonate = new JButton(langUtils.getPhraseFor("Donate"));
		bottomPanel.add(btndonate, "cell 4 0,growy");
		btndonate.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btndonate.setForeground(new Color(255, 140, 0));
		btndonate.setBorderPainted(false);
		btndonate.setOpaque(false);
		btndonate.setBackground(Color.WHITE);
		btndonate.addActionListener(new OpenDonateUrlListener());
		
		JButton ver_btn = new JButton(ver);
		bottomPanel.add(ver_btn, "flowx,cell 5 0,alignx right");
		ver_btn.addActionListener(new OpenChangeUrlListener());
		ver_btn.setFont(new Font("Sitka Text", Font.PLAIN, 11));
		ver_btn.setBorderPainted(false);
		ver_btn.setOpaque(false);
		ver_btn.setBackground(Color.WHITE);
		
		JButton quit_btn = new JButton("Quit");
		bottomPanel.add(quit_btn, "cell 5 0,alignx right");
		mainFrameLang.put(quit_btn, 14);
		quit_btn.setFont(new Font("Dialog", Font.BOLD, 11));
		
		JPanel loginPanel = new JPanel();
		contentPane.setLayer(loginPanel, 1);
		loginPanel.setOpaque(false);
		loginPanel.setBounds(0, 0, 217, 117);
		contentPane.add(loginPanel);
		loginPanel.setLayout(new MigLayout("", "[][grow]", "[][][][grow][][grow]"));
		
		JLabel lblUsername = new JLabel("Username");
		loginPanel.add(lblUsername, "cell 0 0,alignx left,aligny center");
		mainFrameLang.put(lblUsername, 1);
		contentPane.moveToFront(lblUsername);
		lblUsername.setFont(new Font("Dialog", Font.PLAIN, 11));
		
		userField = new JTextField();
		loginPanel.add(userField, "cell 1 0,growx");
		userField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Password");
		loginPanel.add(lblNewLabel, "cell 0 1,alignx left,growy");
		mainFrameLang.put(lblNewLabel, 2);
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 11));
		passwordfield = new JPasswordField();
		loginPanel.add(passwordfield, "cell 1 1,growx");
		
		errorlbl = new JLabel("");
		errorlbl.setHorizontalAlignment(SwingConstants.CENTER);
		errorlbl.setForeground(new Color(255, 51, 0));
		loginPanel.add(errorlbl, "cell 1 2");
		
		JSeparator separator = new JSeparator();
		loginPanel.add(separator, "cell 0 4 2 1,growx,aligny center");
		
		loginbtn = new JButton("Login");
		loginPanel.add(loginbtn, "cell 0 5 2 1,growx");
		mainFrameLang.put(loginbtn, 3);
		
		loginbtn.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		loginbtn.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {				
				if(loggingin != true && Travian.LoginSuccessful == false) {		
					try {		
						Travian.LoginSuccessful = false;
						
						//Fetching data
						frameUsername = userField.getText();
						
						framePassword = passwordfield.getText();
						
						frameServer = Settings.serverfield.getText();
						
						frameUserAgent = Settings.useragentfield.getText();
						
						headless = Settings.headlessCheck.isSelected();
						
						remembercheck = Settings.logincheck.isSelected();
						
						useragentcheck = Settings.chckbxUseUseragent.isSelected();
						
						Travian.usehtmlunitrequest = !Settings.httpcheckbox.isSelected();
						
						timeout = Integer.valueOf(Settings.timeoutfield.getText());
						
						FARMADDER_INIT_DELAY = Integer.valueOf(Settings.FARMADDER_INIT_DELAY_FIELD.getText());
						FARMADDER_EXPAND_DELAY = Integer.valueOf(Settings.FARMADDER_EXPAND_DELAY_FIELD.getText());
						FARMADDER_ADD_DELAY = Integer.valueOf(Settings.FARMADDER_ADD_DELAY_FIELD.getText());
						
						FARMLISTSENDER_INIT_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_INIT_DELAY_FIELD .getText());
						FARMLISTSENDER_EXPAND_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_EXPAND_DELAY_FIELD.getText());
						FARMLISTSENDER_CHECK_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_CHECK_DELAY_FIELD.getText());
						FARMLISTSENDER_SEND_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_SEND_DELAY_FIELD.getText());			
						
						enablelogging = Settings.loggingcheckbox.isSelected();
						useproxy = Settings.proxycheckbox.isSelected();
						proxy = null;
						
						if(useproxy == true) {
							proxy = Settings.proxyfield.getText();
						}
						
						System.out.println(frameUsername);
					
					//Conditions that check if user inputted all req. fields
					errorlbl.setText("");
						
					if(frameUsername.equals("")) {
						System.err.println("Username not set!");
						errorlbl.setText(langUtils.getPhraseFor("Username not set!"));
					} else
					if(frameServer.equals("")) {
						System.err.println("Server not set!");
						errorlbl.setText("Server not set!");
					} else
					if(framePassword.equals("")) {
						System.err.println("Password not set!");
						errorlbl.setText("Password not set!");
					} else
					if(frameUserAgent.equals("") && useragentcheck == true) {
						System.err.println("UserAgent not set!");
						errorlbl.setText("UserAgent not set!");
					} else {
						loggingin = true;
						loginbtn.setIcon(loadingicon);
						new Travian().start();
					}
					} catch (FailingHttpStatusCodeException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		quit_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Travian.quitmethod();
				System.exit(0);
			}
		});	
		
				initbtn.addActionListener(new ActionListener() {  			
						public void actionPerformed(ActionEvent arg0) {				
							try {
								FarmListInit.farmlistinit();
								BotFrame.refreshVillageData();
							} catch (InterruptedException | IOException e) {
								e.printStackTrace();
							}
							
						}
					});
		
		//Console init
		try {
			dialog = new Console();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
			Point consolepoint = dialog.getLocation();
			setLocation(consolepoint.x+50, consolepoint.y+50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("**Waiting for login ...**");
	}
	
	//Method that returns all components in a container	
	public static List<Component> getAllComponents(final Container c) {
	    Component[] comps = c.getComponents();
	    List<Component> compList = new ArrayList<Component>();
	    for (Component comp : comps) {
	        compList.add(comp);
	        if (comp instanceof Container)
	            compList.addAll(getAllComponents((Container) comp));
	    }
	    return compList;
	}
	
	public BotFrame() {
		setBackground(SystemColor.window);
		
		//UI initialization
		try {
			initUI();
		} catch (URISyntaxException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//USER DETAILS READER		
		/*Getting path of the executable (to locate chromedriver)
		  pathp = BotFrame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		  pathp = pathp.replace("bin/", "");		
		*/
		
		//Creating file where temporary data is saved
		File f = new File(workingDirectoryPath + "/pass192.trav");
		
		//Get system IP
		getIP();
		
		//Loads data from pass.trav (if the file exists)
		try {
			readUserDetails(f);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException e) {
			e.printStackTrace();
			System.err.println("@@ User details reader ERROR @@");
		}		
	}
	
	//Generates a AES Cypher key for temporary data decryption
	public static Key generateKey() throws NoSuchAlgorithmException
	{
		Key key = new SecretKeySpec(Travian.keyValue, "AES");
        return key;
	}
	
	//Method called once FarmAdder "Start" button is clicked, I did this so I can simulate a click of this button from another class
	public static void getterbtnclick() {
		if (getter_btnstate == false) {
			BotFrame.btnGetter.setEnabled(false);
			refreshVillageData();
			//frame.btnRemove.setEnabled(true);	//Disabled feature
			farmAdderTabbedPane.setVisible(true);
			helpeditor.setVisible(false);
			getter_btnstate = true;
		} else {
			farmAdderTabbedPane.setVisible(false);
			helpeditor.setVisible(true);
			getter_btnstate = false;
		}
	}
	
	public static void refreshVillageData() {
		Travian.getVillageIds();
		
		BotFrame.IdComboBox.removeAllItems();
		BotFrame.villageComboBox.removeAllItems();
		Settings.villageComboBox.removeAllItems();
		AutoEvader.evadeEntries.clear();
		
		//Combobox item loading
		for(int i = 0; i < Travian.farmlists_names2.size(); i++) {	
			BotFrame.IdComboBox.addItem(Travian.farmlists_names2.get(i));
		}
		
		for(int i = 0; i < Travian.villagenames2.size(); i++) {	
			BotFrame.villageComboBox.addItem(Travian.villagenames2.get(i));
			Settings.villageComboBox.addItem(Travian.villagenames2.get(i));
		}		
		AutoEvader.refreshSettings();
		
		Settings.villageComboBox.setEnabled(true);
		btnCropchecker.setEnabled(true);
		btnAutoEvade.setEnabled(true);
		
		for(int i = 0; i < Travian.farmlists_names2.size(); i++) {	
			BotFrame.removerComboBox.addItem(Travian.farmlists_names2.get(i));
		}
		
		BotFrame.btnGetter.setEnabled(true);
	}
	
	//Getting system ip
	private void getIP() {
		try {
			URL whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(
			                whatismyip.openStream()));
			ip = in.readLine(); 
			
		} catch(Exception e)  {
			e.printStackTrace();
			System.out.println("@@@ IP check connection failed @@@");
		}
	}
	
	//Reading data from temporary file and loading the data into gui
	private void readUserDetails(File f) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		if (f.exists()) {
			BufferedReader b = new BufferedReader(new FileReader(f));
					try {
						
						
						//String datastrings[] = ddata.split("\\*");
						
						String datastrings[] = null;
						String decryptedPassword = null;
						Reader reader;
						try {
							reader = Files.newBufferedReader(Paths.get("metadata/metadata.csv"));
							CSVParser csvParser = CSVParser.parse(reader, CSVFormat.DEFAULT);
							java.util.List<CSVRecord> records = csvParser.getRecords();
							
							CSVRecord r = records.get(0);
							datastrings = new String[r.size()];
							
							Cipher desCipher;
							desCipher = Cipher.getInstance("AES");
							desCipher.init(Cipher.DECRYPT_MODE, generateKey());
							
							cryptodata = r.get(0);			
							byte[] decoded = Base64.decodeBase64(cryptodata);							
							byte[] decrypted = desCipher.doFinal(decoded);							
							decryptedPassword = new String(decrypted);
							
							for (int y = 1; y < r.size(); y++) {
								datastrings[y] = r.get(y);
							}	
						} catch (NoSuchFileException ex) {
							System.out.println("No metadata file found!");
						} catch (IOException e) {
							e.printStackTrace();
						}
						
						if (datastrings != null && decryptedPassword != null) {
							passwordfield.setText(decryptedPassword);
							userField.setText(datastrings[1]);
							Settings.serverfield.setText(datastrings[2]);
							Settings.useragentfield.setText(datastrings[3]);
							Settings.chckbxUseUseragent.setSelected(Boolean.valueOf(datastrings[4]));
							Settings.logincheck.setSelected(Boolean.valueOf(datastrings[5]));
							Settings.httpcheckbox.setSelected(Boolean.valueOf(datastrings[6]));
							Settings.FARMADDER_INIT_DELAY_FIELD.setText(datastrings[7]);
							Settings.FARMADDER_EXPAND_DELAY_FIELD.setText(datastrings[8]);
							Settings.FARMADDER_ADD_DELAY_FIELD.setText(datastrings[9]);
							Settings.FARMLISTSENDER_INIT_DELAY_FIELD.setText(datastrings[10]);
							Settings.FARMLISTSENDER_EXPAND_DELAY_FIELD.setText(datastrings[11]);
							Settings.FARMLISTSENDER_CHECK_DELAY_FIELD.setText(datastrings[12]);
							Settings.FARMLISTSENDER_SEND_DELAY_FIELD.setText(datastrings[13]);
							Settings.attack_lost_box.setSelected(Boolean.valueOf(datastrings[14]));
							Settings.attack_win_loss_box.setSelected(Boolean.valueOf(datastrings[15]));
							Settings.proxyfield.setText(datastrings[16]);
							Settings.proxycheckbox.setSelected(Boolean.valueOf(datastrings[17]));
							Settings.timeoutfield.setText(datastrings[18]);	
							checknatars.setSelected(Boolean.valueOf(datastrings[19]));
							antinoob.setSelected(Boolean.valueOf(datastrings[20]));
							maxdiff.setText(datastrings[21]);
							Settings.chckbxOverrideDir.setSelected(Boolean.valueOf(datastrings[22]));
							Settings.dirTextField.setText(datastrings[23]);
						}
					} catch (Exception e) {
						System.err.println("Reading user details error!");
						e.printStackTrace();
					}	
					
			b.close();
		}	
	}
	
	//Called on farmlist sender table event
	public void tableChanged(TableModelEvent e) {
	        int row = e.getFirstRow();
	        TableModel model = (TableModel)e.getSource();
	       
	        if (e.getType() == TableModelEvent.UPDATE)
	        {
	            int column = e.getColumn();
	            
	            //If the table change is a click of the checkbox -> get row data and call farmlistsender class
	            //When farmlists are initialized, the same amount of farmlistsender class instances is created,
	            //then, if the according checkbox is checked, a timer with a unique TimerTask is started (that runs on a seperate thread)
	            if (column == 0)
	            {
	            	if (tableignore == false) {		            	   	               
		            	//fllist is a ArrayList containing all the farmlistsender instances
		                if (Travian.fllist.get(row).running == false) {
		                	
		                	table.setEnabled(false);
		                	
		                	try {	                			      		            				
		        					String deviationmetaMIN = model.getValueAt(row, 3).toString();
		        					String deviationmetaMAX = model.getValueAt(row, 4).toString();
		                	
		        			//setting data for that particular farmlistsender instance, this is done only when the checkbox is checked
		        			//so you cant change TimerTask intervals / deviations when its running
			                Travian.fllist.get(row).setvalues(Travian.farmlists_id2.get(row).substring(4), Integer.valueOf(model.getValueAt(row, 2).toString()),
			                		Travian.farmlists_names2.get(row), row, deviationmetaMIN, deviationmetaMAX);		               		 
		                	
			                Travian.fllist.get(row).send();
			                Travian.fllist.get(row).running = true;
			                
			                table.setEnabled(true);		                
			                
		                	} catch (NullPointerException | NumberFormatException ex) {
		                		System.out.println("\nInterval/MIN/MAX not entered/valid!");
		                		tableignore = true;
		                		System.out.println("unchecking");
		                		model.setValueAt(false, row, column);	 
		                		table.setEnabled(true);
		                	}
			            	              			            
		                } else { 
		                	System.out.println("!!!!! - STOPPING flsender: " + Travian.fllist.get(row).name + " at row " + row + " - !!!!!");
		                	try {
								Travian.fllist.get(row).pause();
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
		                }
		                tableignore = false;
	            	}
	            } 
	            
	            table.repaint();
	        }          
		}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	//A rather crude way to listen for expander panel expansion(That whole expansion thing is a little wierd) (I plan to use is as extra space, but the main gui needs a rework anyway)
	@Override
	public void mousePressed(MouseEvent arg0) {
		if(frameexpanded == false) {
			frameexpanded = true;
			cropbtn.setVisible(true);
			setSize(this.getWidth() + 190 + 10, this.getHeight());
		} else {
			frameexpanded = false;
			cropbtn.setVisible(false);
			setSize(this.getWidth() - 190 - 10, this.getHeight());
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	public static void saveDelays() {
		FARMADDER_INIT_DELAY = Integer.valueOf(Settings.FARMADDER_INIT_DELAY_FIELD.getText());
		FARMADDER_EXPAND_DELAY = Integer.valueOf(Settings.FARMADDER_EXPAND_DELAY_FIELD.getText());
		FARMADDER_ADD_DELAY = Integer.valueOf(Settings.FARMADDER_ADD_DELAY_FIELD.getText());
		
		FARMLISTSENDER_INIT_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_INIT_DELAY_FIELD .getText());
		FARMLISTSENDER_EXPAND_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_EXPAND_DELAY_FIELD.getText());
		FARMLISTSENDER_CHECK_DELAY = Integer.valueOf(Settings.FARMLISTSENDER_CHECK_DELAY_FIELD.getText());
	}
	
	//Creates a small loading dialog when FarmAdder is parsing gettertools coordinates, its all adressed with global component declarations which is just not ideal (but hey it works)
	public static void showProgress(String title) {
		dlg = new JDialog();
	    dpb = new JProgressBar(0, 100);
	    dpb.setStringPainted(true);
	    dlabel = new JLabel("");
	    JPanel containerPanel = new JPanel();
	    containerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	    containerPanel.add(BorderLayout.NORTH, dpb);
	    containerPanel.add(BorderLayout.NORTH, dlabel);
	    
	    dlg.getContentPane().setLayout(new BorderLayout());
	    dlg.getContentPane().add(containerPanel, BorderLayout.CENTER);
	    
	    dlg.setTitle(title);
	    dlg.getContentPane().add(BorderLayout.NORTH, dpb);
	    dlg.getContentPane().add(BorderLayout.CENTER, dlabel);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.setSize(350, 80);
	    dlg.setLocationRelativeTo(BotFrame.table);
		dlg.setVisible(true);
		dlg.setAlwaysOnTop(true);
	}
	public static void hideProgress() {
		dlg.setVisible(false);
	}
}

//TODO: Side panel, maybe different solution, its kinda makeshift. Move stuff like Cropchecker, AttackChecker, maybe instance of neutral CoordinateViewer
//TODO: Create TroopTrainer
//TODO: Major class inheritance n'stuff cleanups (I mean, sometime)
/**
 *  © 2019 Dan Rakušan. All rights reserved.
 */

//4317 lines and counting 23/3/2018
//3383 lines and counting 26/2/2018

/**
 * 
<ver id="1.8w">
<feature>Farmlist loader algorithm now accounts for JS farmlist loading (when using high performance fl)</feature>
<feature>FarmAdder rework - Gettertools data is loaded into a seperated table</feature>
<feature>FarmAdder status reports and retry option</feature>
<feature>FarmAdder pause button</feature>
<feature>FarmAdder Gettertools table sorting</feature>
<feature>FarmAdder Population filter</feature>
<improvement>FarmlistSender sender threads now synchronised using a proper ThreadSafe object, they will now properly wait for each other.</improvement>
<improvement>No farmlists found / No gold club error message</improvement>
<improvement>Login error catched</improvement>
<improvement>Using highperformance fl checkbox removed - now detected automatically</improvement>
<bugfix>Settings menu closing</bugfix>
<bugfix>FarmAdder gettertools coordinates being offset by 1</bugfix>
<bugfix>FarmAdder crashed if gettertools found no farms</bugfix>
<bugfix>FarmAdder newly created FL will not get registered by following fl reinit</bugfix>
<bugfix>UserAgent tweaks</bugfix>
</ver>
*/
/**
<ver id="1.73w">
<feature>Attack result response - farm stopper</feature>
<feature>Settings menu</feature>
<feature>High performance farmlist support</feature>
<feature>Simple proxy support (requires proxy to have no authentication)</feature>
<feature>Both FLSender and FarmAdder custom delays added</feature>
<bugfix>Console tray minimalization tweaks</bugfix>
<improvement>Settings menu replaced some UI elements</improvement>
<improvement>UI tweaks</improvement>
</ver>
*/
/** 1.72w patch notes - 
 	FEATURE: Website links
 			 Standalone output console
 	IMP: Design elements
 		 Slight layout tweaks
 		 Server verification tweaks
 		 CoordinateViewer finished popup
	BUGFIX:  CoordinateViewer bugs
			 FarmAdder troop dropdown, wrong troop selected bug
			 FarmAdder index bugs
			 FarmAdder scraped the header of gettertools table
*/
/** 1.71w patch notes - 
 	IMP: 	 Login Tweaks
 	FEATURE: Useragent override	
 			 Security tweaks	
 			 Info Text area
 */
/** 1.7w patch notes - 														
	FEATURE:	-Multithreading- (Login/GetterAdder with dynamically updating GUI (EDT thread)
				GetterAdder progress bar
				CoordinateViewer GUI and Custom FLAdder window											
				FLAdder stop button																		
				Side frame expansion														
	BACKEND:	CustomMouseListener for side Jpanel														
	BUGFIX:		GetterAdder dropdown duplication
				FL JTable freezing on invalid parameters
	IMP:		FL sender cleanups / IO
				FL sender interval rounding
				FL sender interval under 1 min modified
				GetterAdder shows up on login
*/

/** 1.6w patch notes - 														
	FEATURE:	Multiple farmlist addition (100+) 						
	BUGFIX:		FL init timeout					
*/

/** 1.592w patch notes - 														
	IMP:		Raidlist init retry cycle added
				X and Y getter coord prompts		
				Add farms plus icon added			
				Output cleanups							
*/

/** 1.59w patch notes -
 	FEATURE:	GetterAdder ID now in form of a dropdown
 				GetterAdder URL no longer needed from user (URL derivates from frame_server)
 				New GetterAdder troop dropdown with icons 														
 	IMP:		New GetterAdder buttonIcon	
 	REM:		Tribe select removed																
*/

/** 1.58w patch notes -
 *
 *	FEATURE:	System tray minimization
 *	IMP: 		Columns no longer movable / resizable
 *	BUGFIX: 	Fixed duplicate FL inicialization
 *	
 */

//first ever version launched 18.10.2017 with 579 lines 