package dudss.travian;

import java.awt.EventQueue;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;


public class FarmListEdit extends Travian implements Runnable	{ 
	
	static int numberOfNewFl = 0;
	//For logging purposes
	static int farmsadded = 0;
	static boolean workerfinished = true;
	public static boolean running = false;
	
	static Boolean AddFailed = false;
	static Boolean AddError = false;
	static Boolean encounteredError = false;
	static int numberOfEncounteredErrors;
	
	private static final String CHAR_LIST =  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static final int RANDOM_STRING_LENGTH = 5;
	
	public static boolean stopAdder = false;
	public static boolean pauseAdder = false;
	
	static CoordinateViewer viewer;
	
	Boolean newFL;
	
	FarmListEdit(Boolean newFL) {
		this.newFL = newFL;
	}
	
	public void run() {
		try {
			running = true;
			numberOfNewFl = 0;
			farmsadded = 0;
			AddFailed = false;
			AddError = false;
			stopAdder = false;
			pauseAdder = false;
			workerfinished = true;
			numberOfEncounteredErrors = 0;
			encounteredError = false;
			
			editfarm(newFL);
		} catch (FailingHttpStatusCodeException | InterruptedException | IOException e) {
			System.err.println("Editfarm Thread run error!");
			e.printStackTrace();
		}
	}
	
	public static void editfarm(Boolean newFL) throws InterruptedException, FailingHttpStatusCodeException, MalformedURLException, IOException {
		
		BotFrame.showProgress("Loading coords ...");
		changeLabel("Initializing gettertools ...");
		setIndeter(true);
		
		//Gettertools initialization   		
		System.out.println("initializing gettertools");
		
		//Getter now return Boolean to determine if it found any farms or encountered an error
		Boolean getterSuccessful = Getter.get_gettercoords(BotFrame.frameX,BotFrame.frameY,BotFrame.frameRange, BotFrame.frameCheckNatars , BotFrame.frameAntiNoob, BotFrame.frameMaxDiff, BotFrame.frame_getterurl);		
		//If no farms found or an exception was thrown, FarmListEdit hides the getter progress bar and returns
		if (getterSuccessful == false) {
			changeLabel("No farms found ... !");
			Thread.sleep(800);
			running = false;
			BotFrame.hideProgress();
			return;
		}
		changeLabel("Done!");
		
		System.out.println("Starting CoordViewer ...");
	
		EventQueue.invokeLater(new Runnable() {
    	    @Override
    	    public void run() {
    	    	viewer = new CoordinateViewer();
    	    	viewer.setVisible(true);
    	    }
    	  });
		
		BotFrame.hideProgress();
		
		for(int i = 0; i < Getter.XYlist.size(); i += 2) {
			Object[] rowinfo = new Object[] { true , Getter.XYlist.get(i), Getter.XYlist.get(i+1), Getter.Villagelist.get(i/2), Getter.Namelist.get(i/2), Getter.Populationlist.get(i/2), Getter.Alliancelist.get(i/2)};
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					CoordinateViewer.getModel_loader().addRow(rowinfo);
				}
			});
		}
		
		while(!Travian.driverInUse.compareAndSet(false, true)) {
			System.out.println("editfarmclass waiting for driver access (1 sec) ...");
			Thread.sleep(1000);
		}
		
		Travian.driver.get(travian_url_farm);
		System.out.println(driver.getCurrentUrl());
		System.out.println("Create new FL: " + newFL);
		
		//Use existing FL or create a new one
		if(newFL == true) {
			CoordinateViewer.btnStartFarmadder.setEnabled(false);
			CoordinateViewer.btnStartFarmadder.setIcon(BotFrame.loadingicon);
			BotFrame.frame_idedit = createFLandGetID();
			System.out.println("final:" + BotFrame.frame_idedit);
			CoordinateViewer.btnStartFarmadder.setEnabled(true);
			CoordinateViewer.btnStartFarmadder.setIcon(new ImageIcon(CoordinateViewer.class.getResource("/res/plus3.png")));
		}		
	}	

	public static String resolveTroopId() {
		String t_id = null;
		
		switch (BotFrame.UnitComboBox.getSelectedItem().toString()) {
			case "clubswinger": t_id = "t1";
				break;
			case "spearman": t_id = "t2";
				break;
			case "axeman": t_id = "t3";
				break;
			case "paladin": t_id = "t5";
				break;
			case "teutonic knight": t_id = "t6";
				break;
			case "teutonic ram": t_id = "t7";
				break;
			case "catapult": t_id = "t8";
				break;
			case "chief": t_id = "t9";
				break;
			case "teutonic settler": t_id = "t10";
				break;			
				
			case "legionnaire": t_id = "t1";
				break;
			case "pretorian": t_id = "t2";
				break;
			case "imperian": t_id = "t3";
				break;
			case "EI": t_id = "t5";
				break;
			case "EC": t_id = "t6";
				break;
			case "roman ram": t_id = "t7";
				break;
			case "fire catapult": t_id = "t8";
				break;
			case "senator": t_id = "t9";
				break;
			case "roman settler": t_id = "t10";
				break;
				
			case "phalanx": t_id = "t1";
				break;
			case "swordsman": t_id = "t2";
				break;
			case "theutates thunder": t_id = "t4";
				break;
			case "druidrider": t_id = "t5";
				break;
			case "haeduan": t_id = "t6";
				break;
			case "gaul ram": t_id = "t7";
				break;
			case "trebuchet": t_id = "t8";
				break;
			case "cheiftain": t_id = "t9";
				break;
			case "gaul settler": t_id = "t10";
				break;
				
			default: t_id = "t1";
		}
		System.out.println("TID:" + t_id);	
		return t_id;
	}
	public static void GetterAdderFinished() throws InvocationTargetException, InterruptedException {
        if(workerfinished == true) {
        	SwingUtilities.invokeLater(new Runnable() {
        	    public void run() {
        	    
        	    if (encounteredError == false) {
        	        JOptionPane.showMessageDialog(BotFrame.MainGUI,
        	        	    "Farmlist Adder finished, Added/edited: " + (farmsadded) + " farmlists entries ");
        	    } else {
        	    	 JOptionPane.showMessageDialog(BotFrame.MainGUI,
         	        	    "Farmlist Adder finished, Added/edited: " + (farmsadded) + " farmlists entries\nFarmAdder encountered " + numberOfEncounteredErrors + " errors. Check the AdderTable to schedule a retry or console for more details");
        	    }
        	    }
        	});
        }

	}
	
	@SuppressWarnings("unchecked")
	public static String createFLandGetID() throws InterruptedException, IOException {
		
		String currentFLid = null;
		
		System.out.println("creating new FL ...");
		Travian.runscript("Travian.Game.RaidList.showCreateNewList()");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='name']")));
		WebElement name = driver.findElement(By.xpath("//*[@id='name']"));
		
	    String generatedString = generateRandomString();
	    System.out.println("FLName: " + generatedString);	    
	   
	    String tempname = "GetterAdderFL " + numberOfNewFl + " -- " + generatedString;
		name.sendKeys(tempname);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"did\"]")));
		Select villageselect = (Select) new Select(driver.findElement(By.xpath("//*[@id=\"did\"]")));		
		
		villageselect.selectByIndex(BotFrame.villageComboBox.getSelectedIndex());
		
		Travian.runscript("Travian.Game.RaidList.createNewList()");
		numberOfNewFl++;
		System.out.println("new FL created! + (" + tempname + ")");
		
		//WAITING FOR DOM READY STATE
		driver.get(travian_url_farm);
		
		ArrayList<String> tempidlist = new ArrayList<String>();
		tempidlist =  (ArrayList<String>) Travian.farmlists_id2.clone();
		
		System.out.println(Travian.farmlists_id2);
		System.out.println(tempidlist);
		
		FarmListInit.farmlistinit();
		
		System.out.println(Travian.farmlists_id2);
		System.out.println(tempidlist);
	
		outerloop:
		for(int i = 0; i < Travian.farmlists_id2.size(); i++) {
			
			try {
			
				System.out.println("fL: " + Travian.farmlists_id2.get(i));
				System.out.println("tp: " + tempidlist.get(i));
				
				if (Travian.farmlists_id2.get(i).equals(tempidlist.get(i))){
					System.out.println("no");
				} else {
					currentFLid = farmlists_id2.get(i);
					System.out.println(currentFLid);
					System.out.println("BREAK");
					break outerloop;
				}
			
		    } catch (IndexOutOfBoundsException e) {
				System.out.println("OutOfBounds");
				currentFLid = farmlists_id2.get(i);
			}
		}
		
		String frame_idedit_temp = currentFLid.replace("list","");
		currentFLid = frame_idedit_temp;
		
		System.out.println("NEW ID: " + currentFLid);
		
		return currentFLid;
	}
	
	static class TableSwingWorker extends SwingWorker<DefaultTableModel, Object[]> {	
		 
		    Actions actions;
		 	 
		    private final DefaultTableModel tableModel;
		    JTable table;
		    JProgressBar progbar;
		    JLabel numberlabel;
		    JLabel percentagelabel;
		    JLabel infolabel;
		    String t_id;
		   
		    //External duplicate of for loop "i" index (Index = X, Index + 1 = Y)
		    int index = 0;	
		    //Index / 2 (Number of farms)
		    int number = 0;
		    //Total number of farms inside XYList
		    int total;
		    
		    ArrayList<String> XYlist;
		    List<String> Villagelist;
		    List<String> Namelist;
		    List<String> Populationlist;
		    
		    String id_edit;
		    String noftroops;
		    
		    public TableSwingWorker(ArrayList<String> XYlist, List<String> Villagelist, List<String> Namelist, List<String> Populationlist, DefaultTableModel tableModel,JTable table,JProgressBar progbar, JLabel numberlabel, JLabel percentagelabel, JLabel infolabel, String t_id, String id_edit, String troops) {
		        this.tableModel = tableModel;
		        this.table = table;
		        this.progbar = progbar;		    
		        this.numberlabel = numberlabel;
		        this.percentagelabel = percentagelabel;
		        this.t_id = t_id;
		        this.noftroops = troops;
		        this.id_edit = id_edit;
		        this.infolabel = infolabel;
		        
		        this.XYlist = new ArrayList<String>(XYlist);	        
		        this.Villagelist = new ArrayList<String>(Villagelist);		        		        
		        this.Namelist = new ArrayList<String>(Namelist);		       	 
		        this.Populationlist = new ArrayList<String>(Populationlist);
		        
		        total = this.XYlist.size()/2;
		        
		        actions = new Actions(driver);
		        
		        progbar.setStringPainted(true);
		    }
		    
		    @Override
		    protected DefaultTableModel doInBackground() throws Exception {
		    	
		    	//driver.get(travian_url_farm);
		    	//driver.navigate().refresh();
		    	
		    	int i;
		    	int n = 1;
		    	numberOfEncounteredErrors = 0;
		    	encounteredError = false;
		    	
				for(i = 0; i < XYlist.size(); i = i + 2) {
					try {
						
						//System.out.println("StopAdder:" + stopAdder + " idedit: " + id_edit + " frame id edit: " + frame.frame_idedit );						
						index = i;
						
						if(stopAdder == true) {
							stopAdder = false;
							System.err.println("\nBreaking GetterAdder!");
							break;				
						}	
						while(pauseAdder == true) {
							Thread.sleep(1000);
						}
						Thread.sleep(BotFrame.FARMADDER_INIT_DELAY);
						System.out.println("\n");
						
						Boolean listcontentIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id_edit + "']/form/div[2]", 5, 500);
						if (listcontentIsPresent == false) {
							System.err.println("Could not find farmlist id: " + id_edit + " - could not find element");
							AddFailed = true;
							AddError = true;	
							continue;
						} 
						WebElement listcontent = driver.findElement(By.xpath("//*[@id='list" + id_edit + "']/form/div[2]"));
						
						if (listcontent.getAttribute("class").equals("listContent hide")) {
							System.out.println("Expanding fl ...");
							Boolean expanderIsPresent = FarmListEdit.webElementRetryCycleWait("//*[@id='list" + id_edit + "']/form/div[1]/div[1]/div/div", 5, 500, 1);
							if (expanderIsPresent == false) {
								System.err.println("Farmlist expansion failed - could not find element");	
								AddFailed = true;
								AddError = true;	
								continue;
							}					
							WebElement expand = driver.findElement(By.xpath("//*[@id='list" + id_edit + "']/form/div[1]/div[1]/div/div"));	
							Boolean loadingimgIsPresent = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id_edit + "']/form/div[1]/div[2]/img", 5, 500);
							if (loadingimgIsPresent == false) {
								System.err.println("Could not find loading img");
								AddFailed = true;
								AddError = true;	
								continue;
							} 
							WebElement loadingimg = driver.findElement(By.xpath("//*[@id='list" + id_edit + "']/form/div[1]/div[2]/img"));
							actions.moveToElement(expand).click().perform();
							Thread.sleep(BotFrame.FARMADDER_EXPAND_DELAY);
							if(loadingimg.getAttribute("class").equals("loading")) {
								while(loadingimg.getAttribute("class").equals("loading")) {
									System.out.println("FL loading, polling rate: 100");
									Thread.sleep(100);
								}
							}
							if(loadingimg.getAttribute("class").equals("loading hide")) {
								System.out.println("Loading finished");
							}
						}
						
						
						//	System.out.println("Waiting for addbutton visibility");		
						Boolean bool = FarmListInit.webElementRetryCycleWait("//*[@id='list" + id_edit +"']/form/div[2]/div[1]/div[2]/button", 5, 200);
						if (bool == false) {
							System.err.println("Farmlist error - could not find element - add");
		    		    	AddFailed = true;
		    		    	AddError = true;	
		    		    	continue;
						}
						//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='list" + id_edit +"']/form/div[2]/div[1]/div[2]/button")));		  //ERR	
						//WebElement add = driver.findElement(By.xpath("//*[@id='list" + id_edit +"']/form/div[2]/div[1]/div[2]/button"));		
						WebElement add = driver.findElement(By.xpath("//*[@id='list" + id_edit + "']/form/div[2]/div[1]/div[2]/button/div/div[2]"));		
						
						
						//need to wait here why dont know, TODO: fix 						
						Thread.sleep(BotFrame.FARMADDER_ADD_DELAY);
						
						try {			
			    			actions.moveToElement(add).click().perform();		    				
			   			//	System.out.println("Bringing up add window ...");
			    			} catch (NullPointerException | StaleElementReferenceException e) {
			    		    	System.err.println("Cannot load farmlist / Button missing - Maybe FarmAdder delays are too low? - you can retry adding this farm later with the retry button");
			    		    	AddFailed = true;
			    		    	AddError = true;
			    		    	continue;
			   		    	}	
						if(AddFailed != true) {		
								System.out.println("Adding farm ... n." + ((i/2)+1));					
								
								try {
			    				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("lid")));
			    				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("xCoordInput")));
			    				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("yCoordInput")));
			    				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(t_id)));
			    				wait.until(ExpectedConditions.elementToBeClickable(By.id("save")));
								} catch (NoSuchElementException | TimeoutException exp) {
									System.err.println("Farmlist add farm popup not loaded - could not find element - lid");
									AddFailed = true;
				    		    	AddError = true;
				    		    	continue;
								}
								
			    				Select select = (Select) new Select(driver.findElement(By.id("lid")));
			    				
			    				WebElement x = driver.findElement(By.id("xCoordInput"));
			    				
			    				WebElement y = driver.findElement(By.id("yCoordInput"));
			    				
			    				WebElement troops = driver.findElement(By.id(t_id));
			    				
			    				WebElement confirm = driver.findElement(By.id("save"));	
			    					
								select.selectByValue(id_edit);							
			  					System.out.println("Setting xCoordInput ... n." + i + " -- " + XYlist.get(i));
			  						String koordX = XYlist.get(i);
			 						x.clear();
			 					    x.sendKeys(koordX);
			  			
			    				System.out.println("Setting yCoordInput ... n." + (i+1) + " -- " + XYlist.get(i+1));
			    					String koordY = XYlist.get(i+1);
			    					y.clear();
			    					y.sendKeys(koordY);
			    				
			    				//System.out.println("Setting troops ...");
			    					troops.clear();
			    					troops.sendKeys(noftroops);
			    					
			    				//System.out.println("Confirming ...");
			    					System.out.println("Confirming ...");
			    					actions.moveToElement(confirm).click().perform();
			    					
			    				//driver.get(travian_url_farm);
				    		}

			    			if(n >= 100) {
			    				id_edit = createFLandGetID();
			    				n = 1;
			    				System.out.println("\n100/100 farms reached ... NOW adding to " + id_edit + "\n");
			    			}	    	
			    			
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("@@ FarmAdder SwingWorker thread error! @@ at index: " + i);
							AddError = true;
						} finally {
							if (AddError != true) {
								Object[] info = new Object[] {XYlist.get(i) + ":" + XYlist.get(i+1), Villagelist.get(i/2), Namelist.get(i/2), Populationlist.get(i/2), "OK", "--"};	    		
								publish(info);	
							} else {				
								Object[] info = new Object[] {XYlist.get(i) + ":" + XYlist.get(i+1), Villagelist.get(i/2), Namelist.get(i/2), Populationlist.get(i/2), "ERROR", "Retry"};	    		
								publish(info);							
								encounteredError = true;
								numberOfEncounteredErrors++;
							}
							
						number++;
		    			n++;		
						AddError = false;
						AddFailed = false;	
						}						
			}	
					workerfinished = true;
					Travian.driverInUse.set(false);
				    index = i;
				    FarmListEdit.farmsadded = index/2;
				    
				    if (encounteredError != true) {
						System.out.println("------- Farmlist adder - end -------");
						System.out.println("Added/edited: " + (i/2) + " farmlists entries" );
						GetterAdderFinished();
				    } else {
				    	System.err.println("FarmAdder Error");				
				    	System.out.println("------- Farmlist adder - end - errors -------");
						System.out.println("Added/edited: " + (i/2) + " farmlists entries with " + numberOfEncounteredErrors + " errors" );
						System.out.println("Farm adder did encounter errors!");
						GetterAdderFinished();
				    }
		    	
		        return tableModel;
		    }
		    
		    @Override
		    protected void process(List<Object[]> chunks) {
		        for (Object[] row : chunks) {
		            tableModel.addRow(row);
		        }
		        table.scrollRectToVisible(table.getCellRect(number, 1, false));
		        double percentage = ((double) number/ (double) total)*100;
		        progbar.setValue((int)percentage);		       
		        numberlabel.setText("(" + Integer.toString(number) + "/" + Integer.toString(total) + ")");
		        percentagelabel.setText("~" + Integer.toString((int)percentage) + "%");
		        try {
		        infolabel.setText("Added farm (" + XYlist.get(index) + "|" + XYlist.get(index + 1) + ") ...");
		        } catch (IndexOutOfBoundsException e ) {
		        	System.out.println("(infolbl err)");
		        }
		        FarmListEdit.farmsadded = index/2;
		    }	   
     }
	
	public static void executeWorker(String t_id, ArrayList<String> XYlist, List<String> Villagelist, List<String> Namelist, List<String> Populationlist) {
		 //AddingSwingWorkerStart
        TableSwingWorker worker1 = new TableSwingWorker(XYlist, Villagelist, Namelist, Populationlist,
        CoordinateViewer.model, 
			CoordinateViewer.table, 
				CoordinateViewer.progressBar, 
					CoordinateViewer.lblCurrentnumber,        								 
						CoordinateViewer.lblPercentage, 
							CoordinateViewer.lblInfo,
								t_id,
									BotFrame.frame_idedit,
										BotFrame.frameNumTroops);
        worker1.execute();
	}
	 
	private static int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
		
    public static String generateRandomString(){
         
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }
    
    private static void changeLabel(String text) {
    	  EventQueue.invokeLater(new Runnable() {
    	    @Override
    	    public void run() {
    	      BotFrame.dlabel.setText(text);
    	    }
    	  });
    	}
    
    public static void setIndeter(Boolean b) {
    	EventQueue.invokeLater(new Runnable() {
    	    @Override
    	    public void run() {
    	    	BotFrame.dpb.setIndeterminate(b);
    	    }
    	  });
	}
    public static boolean webElementRetryCycleWait(String xpath, int MAX_RETRY_COUNT, int delay, int factorial) throws InterruptedException {
		int retryCount = 0;
		
		while(true)
		{
		    try
		    {
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		    	WebElement isPresent = driver.findElement(By.xpath(xpath));
		    	return true;    
		    }
		    catch(NoSuchElementException | TimeoutException e)
		    {
		    if(retryCount > 1) {
		    	try {	
		    		
			    	Thread.sleep(delay * factorial);
			    	factorial += (factorial/(MAX_RETRY_COUNT/2));
		        
			        if(retryCount >= (MAX_RETRY_COUNT - 1)) {
			        	try {
			        	driver.get(travian_url_farm);
			        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			        	} catch (Exception exp) {} 
			        	try {
			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
				        } catch (Exception exp) {} 
			        }
			        
			        
			        if(retryCount > MAX_RETRY_COUNT)
			        {
			            throw new RuntimeException("element (xpath = \"" + xpath + "\") NOT found, retried " + retryCount + " times", e);
			        }
		    	} catch (RuntimeException ex) {
		    		System.out.println(e.getMessage());
		    		System.err.println("-- Element not found --");
		    		return false;
		    	}
		    }
		        System.out.println("element (xpath = \"" + xpath + "\", delay: " +  + delay*factorial + " ) not found retry number " + retryCount);
		        retryCount++;
		        continue;
		   }
		}
	}
}
/**
 *  � 2018 Dan Raku�an. All rights reserved.
 */
